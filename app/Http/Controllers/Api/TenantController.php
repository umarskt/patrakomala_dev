<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\TenantRequest;
use App\Models\SubDistrict;
use App\Models\Tenants\Tenant;
use App\Models\Tenants\TenantLegality;
use App\Models\Tenants\TenantService;
use App\Models\Tenants\TenantClient;
use App\Models\Tenants\TenantGallery;
use App\Models\Tenants\TenantAlbum;
use App\Models\Tenants\TenantPortofolio;
use App\Mail\notifregistertenant;
use Carbon\Carbon;
use JWTAuth;
use Validator;
use File;
use DB;

class TenantController extends Controller
{
	private $response = array(
		'status' => 'failed',
		'message' => 'not found data!',
		'errors'  => 0,
		'count'  => 0, 
		'data'   =>null,
		'meta'   =>null
	);

	function getAllSubDistrict(){
		try {
			$getData = SubDistrict::all();
			$datas = [];

			foreach ($getData as $key => $data) {
				$datas[] = $data->getDataApi();
			}

			if(count($datas) > 0 ){
				$this->response['status'] = 'success';
				$this->response['message'] = 'Found Data';
				$this->response['data'] = $datas;
			}

			return response()->json($this->response);
		} catch (Exception $e) {
			$this->response['message'] = $e->getMessage();
		}
	}

	function filteringTenant(Request $request){
		try {
			if( $request->has('filter') ){
				$tenants = Tenant::getBasedCategory($request->page, $request->filter);
			}else{
				$tenants = Tenant::getBasedCategory($request->page);
			}

			$datas = [];
			foreach ($tenants as $key => $tenant) {
				$datas[] = $tenant->ListDataApi();
			}

			if(count($tenants) == 0){
				$this->response['status'] = 'success';
				return response()->json($this->response);
			}else{
				$this->response['status'] = 'success';
				$this->response['message'] = 'found data!';
			}
			$paginate = $tenants->toArray();
			$this->response['data'] = $datas;
			$this->response['count'] = count($datas);
			$this->response['meta'] = [
					'current_page' => $paginate['current_page'],
					'from' => $paginate['from'],
					'last_page' => $paginate['last_page'],
					'path' => $paginate['path'],
					'per_page' => $paginate['per_page'],
					'prev_page_url' => $paginate['prev_page_url'],
					'next_page_url' => $paginate['next_page_url'],
					'to' => $paginate['to'],
					'total'=> $paginate['total']
				];
			return response()->json($this->response);
		} catch (Exception $e) {
			$this->response['message'] = $e->getMessage();
			return response()->json($this->response);
		}
	}

	function tenantDetail(Request $request){
		try {
			if(	$request->has('tenant_id')	){
				$detail = Tenant::find($request->tenant_id);
				if(!empty($detail)){
					$this->response['status'] = 'success';
					$this->response['message'] = "Found data";
					$this->response['data'] = $detail->getDataApiProfile();
				}else{
					$this->response['status'] = 'success';
					$this->response['message'] = "Not Found data";
				}

				return response()->json( $this->response );
			}else{
				$this->response['message'] = "request invalid";
				return response()->json($this->response, 422);
			}
		} catch (Exception $e) {
			$this->response['message'] = $e->getMessage();
			return response()->json($this->response);
		}
	}

	function editTenant( Request $request ){
	   	DB::beginTransaction();
		try {
			if( !$request->has('token') ){
				$this->response['message'] = 'invalidRequest';
				return response()->json( $this->response, 422 );
			}

			$user = JWTAuth::toUser( $request->token );
			if( $request->hasFile( 'tenant_logo' ) ){
			  $user->tenant->deleteImg();
			  $filename = time() . '.' . $request->file( 'tenant_logo' )->getClientOriginalExtension();
			  $request->file( 'tenant_logo' )->move( Tenant::getPath(), $filename);
			  $user->tenant->img_logo = $filename;
			}

			if( $request->has('tenant_color_background') ){
			   $user->tenant->tenant_color_background = $request->tenant_color_background;
			}

			//about
			if( $request->has('tenant_profile') ){
			  $user->tenant->about = $request->tenant_profile;
			}

			if( $request->has('tenant_intro') ){
			  $user->tenant->tenant_intro = $request->tenant_intro;
			}

			if( $request->has('tenant_mission') ){
			   $user->tenant->tenant_mission = $request->tenant_mission;
			}

			//update tenant
			if(	$request->has('social_site_type') ){
				DB::table('tenant_social_sites')->where('ekraf_tenant_id','=',$user->tenant->id)->delete();
				$data_social_sites = [];
				foreach ($request->social_site_type as $key => $social) {
					$data_social_sites[] = [
						'ekraf_tenant_id' => $user->tenant->id,
						'social_site_type' => $social,
						'social_site_url'  => ($request->has('social_site_url')) ? $request->social_site_url[$key]  : null,
					];
				}
				$user->tenant->social_sites()->createMany( $data_social_sites );	
			}

			if( $request->has('tenant_service') ){
				  $data_service = [];
				  TenantService::whereEkrafTenantId( $user->tenant->id )->delete();
				  foreach ($request->tenant_service as $key => $service) {
					$data_service[] = [
					  'ekraf_tenant_id' => $user->tenant->id,
					  'title'           => $service,
					  'description'     => (!empty($request->tenant_service_description[$key])) ? $request->tenant_service_description[$key] : null,
					];
				  }
				  $user->tenant->services()->insert( $data_service );
			}

		  	if( $request->has('latitude') || $request->has('longitude') ){
		  		$user->tenant->address->latitude  = (!empty($request->latitude)) ? $request->latitude : $user->tenant->address->latitude;
		  		$user->tenant->address->longitude = (!empty($request->longitude)) ? $request->longitude : $user->tenant->address->longitude;
		  		$user->tenant->address->save();
		  	}

		  	if( $request->has( 'portofolios' ) ){

		  		$validator = Validator::make( $request->portofolios, [
		  			'mimes:pdf|max:10000'
		  		]);

		  		if($validator->fails()){
		  			$this->response['message'] = $validator->messages();
		  			return response()->json( $this->response );
		  		}

		  		$portofolios = TenantPortofolio::whereEkrafTenantId( $user->tenant->id );
		  		if($portofolios->count() > 0){
		  			$portofolios->delete();
		  			foreach ($portofolios->get() as $key => $portofolio) {
		  				$portofolio->deletePortofolio();
		  			}
		  		}
		  			$path = TenantPortofolio::getPathPortofolio($user->tenant->id);
		  			if( !File::exists( $path ) ){
		  				 File::makeDirectory( $path, $mode = 0755, true, true );
		  			}
		  			$data_portofolios = [];
		  			$i = 0;
		  			foreach ($request->file('portofolios') as $key => $file) {
		  				$filename = time() .$i.'.' . $file->getClientOriginalExtension();
						$file->move($path, $filename);
						$data_portofolios[] = [
					  		'ekraf_tenant_id'   => $user->tenant->id,
					  		'name' 				=> $request->portofolio_names[$key],
					  		'img_portofolio'    => $filename
						];
						$i++;
		  			}
		  			$user->tenant->portofolios()->createMany( $data_portofolios );
		  	}

		  	if($request->has('client_img_logo')){
			  $clients = TenantClient::whereEkrafTenantId( $user->tenant->id );
			  if($clients->count() > 0){
				  $clients->delete();
				  foreach ($clients->get() as $key => $client) {
					  $client->deleteImg();
				  }
			  }
			  $path = TenantClient::getPathImgClient( $user->tenant->id );
				  if(!File::exists($path)){
					 File::makeDirectory( $path, $mode = 0755, true, true );
				  }
				  $i = 0;
				  $datas = [];
				  foreach ( $request->file('client_img_logo') as $key1 => $file ) {
					$filename = time() .$i.'.' . $file->getClientOriginalExtension();
					$file->move($path, $filename);
					$datas[] = [
					  'ekraf_tenant_id'   => $user->tenant->id,
					  'name'              => $request->client_name[$key1],
					  'img_icon'          => $filename
					];
					$i++;
				  }
				DB::table('tenant_clients')->insert( $datas );
		  }
		
			if( $user->tenant->save() ){
			  DB::commit();
			  $this->response['status']  = 'success';
			  $this->response['message'] = 'successUpdate';
			  return response()->json( $this->response );
			}

		} catch (JWTException $e) {
			$this->response['message'] = $e->getMessage();
			return response()->json( $this->response, 422 );
		}
	}

	function StoreGalleries( Request $request ){
		DB::beginTransaction();
		try {
			if( !$request->has('token') ){
				$this->response['message'] = 'invalidRequest';
				return response()->json( $this->response, 422 );
			}
			$user  	= JWTAuth::toUser( $request->token );
			$path 	= TenantGallery::getPathGallery( $user->tenant->id );
			if(!File::exists($path)){
                File::makeDirectory( $path, $mode = 0755, true, true );
            }

            if(	$request->has('album_title') || $request->has('album_description') ){
            	if( $request->has('id_album') ){
            		$album = TenantAlbum::findOrFail( $request->id_album );
            		$album->album_name = (isset( $request->album_title )) ? $request->album_title : $album->album_name;
            		$album->album_description = (isset( $request->album_description )) ? $request->album_description : $album->album_description;
            		$album->save();
            	}else{
            		$album = TenantAlbum::create([
            			'ekraf_tenant_id' => $user->tenant->id,
            			'album_name' => $request->album_title,
            			'album_description' => $request->album_description
            		]);
            	}
            }
           	if( $request->has('gallery_image') ){
           		if( $request->has('id_gallery') ){
           			$gallery = TenantGallery::findOrFail( $request->id_gallery );
           			$gallery->deleteImgGallery( $user->tenant->id );
           			$image = $request->file('gallery_image');
           			$filename = time().'.'.$image->getClientOriginalExtension();
           			$image->move( $path, $filename );
           			$gallery->img_gallery = $filename;
           			$gallery->save();
           		}else{
           			$i = 0;
		            $datas = [];
		            foreach ($request->file('gallery_image') as $key => $image) {
						$filename = time() .$i.'.'. $image->getClientOriginalExtension();
						$image->move( $path, $filename );
						$i++;
						 $datas[] = [
		                    'ekraf_tenant_id'=> $user->tenant->id,
		                    'img_gallery'    => $filename,
		                    'album_id' 		 => $album->id,
		                    'created_at'     => Carbon::now(),
		                    'updated_at' 	 => Carbon::now()
		                 ];
					}
					DB::table('tenant_galleries')->insert($datas);
           		}
           	}
			DB::commit();
			$this->response['status']  = 'success';
			$this->response['message'] = 'sucessSaveGallery';
			return response()->json( $this->response );
		} catch (JWTException $e) {

			$this->response['message'] = $e->getMessage();
			return response()->json( $this->response, 422 );
		}
	}

	function storeTenant( Request $request ){
		
		DB::beginTransaction();

		$validator = Validator::make($request->all(),[
			'tenant_name' => 'required|max:40',
			'tenant_founding_year' => 'required',
			'tenant_email' 		=> 'required|unique:ekraf_tenants',
			'tenant_phone' 		=>  'required'
		]);

		if( $validator->fails() ){
			$this->response['status']  = 'invalidInputs';
			$this->response['message'] = $validator->messages();
			$this->response['errors']  = count($validator->messages());
			return response()->json($this->response, 422);
		}

		try {

			if( $request->hasFile('tenant_logo') ){
				$filename = time(). '.' . $request->file('tenant_logo')->getClientOriginalExtension();
				$request->file('tenant_logo')->move( Tenant::getPath(), $filename );
			}
			//Save Tenant
			$tenant = Tenant::create( [
				'tenant_name' => $request->tenant_name,
				'founding_year' => Carbon::parse($request->tenant_founding_year)->format('Y-m-d'),
				'about' => $request->tenant_about,
				'img_logo' => (!empty($filename)) ? $filename : null,
				'tenant_email' => $request->tenant_email,
				'tenant_phone' => $request->tenant_phone,
				'company_type' => $request->company_type,
				'company_hki'  => $request->company_hki
			] );


			//Tenant Founder
		   $tenant->founders()->create([
				'ekraf_tenant_id' => $tenant->id,
				'founder_name'    => $request->founder_name,
				'founder_last_education' => $request->founder_last_education,
				'founder_last_education_department'   => $request->founder_last_education_department,
				'gender'     	  => $request->founder_gender,
				'founder_npwp' 	  => $request->founder_npwp
		   ]);

			//address
			if($request->has('address_street')){
				$tenant->address()->create([
					'ekraf_tenant_id' => $tenant->id,
					'street'          => $request->address_street,
					'subdistrict_id'  => $request->address_subdistrict_id,
					'postal_code'     => $request->address_postal_code,
					'latitude'        => $request->address_latitude,
					'longitude'       => $request->address_longitude,
					'rtandrw'  		  => $request->address_rtandrw 
				]);
			}

		   	if( $request->hasFile( 'legality_image' ) ){
				$path_legalty = TenantLegality::getPathLegality( $tenant->id );
				if(!File::exists($path_legalty)){
					File::makeDirectory( $path_legalty, $mode = 0755, true, true ); 
				}
				$i = 0;
				$datas = [];
				foreach ( $request->file('legality_image') as $key => $file ) {
					$filename = time() .$i.'.'. $file->getClientOriginalExtension();
					$file->move($path_legalty, $filename);
					$i++;
					$datas[] = [
						'ekraf_tenant_id'  		=> $tenant->id,
						'legality_doc_name' 	=> (!empty($request->legality_doc_name[$key])) ? $request->legality_doc_name[$key] : null,
						'legality_doc_slug'     => (!empty($request->legality_doc_name[$key])) ? str_slug($request->legality_doc_name[$key]) : null,
						'tenant_legality_img' => (!empty( $filename )) ? $filename : null
					];
				}
				$tenant->legality_docs()->createMany($datas);
			}

			if($request->has('tenant_subsectors')){
				$subSectors = [];
				foreach ($request->tenant_subsectors as $key => $subsecotor) {
					$subSectors[] = [
						'ekraf_tenant_id' => $tenant->id,
						'category_id' 	  => $subsecotor
					];
				}
				//SubSector
				DB::table('tenant_categories')->insert( $subSectors );
			}

			//product
			$product = $tenant->product()->create([
				'ekraf_tenant_id' => $tenant->id,
				'tenant_product_category' => $request->product, 
				'tenant_product_type' =>  $request->product_type,
				'tenant_product_character' 	=> $request->product_character,
				'tenant_product_quantity' 	=> $request->product_quantity,
				'tenant_product_unit' 		=> $request->product_unit
			]);

			$product->tools()->create([
				'tenant_product_id' => $product->id,
				'tool_name'=> $request->tool_name,
				'tool_brand_name'=> $request->tool_brand_name,
				'tool_quantity'  => $request->tool_quantity,
				'tool_capacity'  => $request->tool_capacity,
				'tool_price' 	 => $request->tool_price,
				'tool_is_copyright' => $request->is_copyright
			]);

			if( $request->has('marketing_is_online') ){
				$tenant->store()->create([
					'ekraf_tenant_id'     => $tenant->id,
					'marketing_is_online' => $request->marketing_is_online,
					'store_open_at'       => $request->open_at,
					'store_close_at' 	  => $request->close_at,
					'is_facility_parking' => $request->is_facility_parking,
					'is_facility_toilet'  => $request->is_facility_toilet,
					'is_facility_mushola' => $request->is_facility_mushola,
					'is_facility_showroom'=> $request->is_facility_showroom,
					'store_surface_area'  => $request->surface_area
				]);

				if($request->marketing_is_online){
					$data_social_sites = [];
					foreach ($request->social_site_type as $key => $social_site_type) {
						$data_social_sites[] = [
							'ekraf_tenant_id' => $tenant->id,
							'social_site_type' => $social_site_type,
							'social_site_url'  => ($request->has('social_site_url')) ? $request->social_site_url[$key]  : null,
						];
					}

					$tenant->social_sites()->createMany( $data_social_sites );
				}
			}

			if( $request->has('employee_male_total') ){
				$tenant->employee()->create([
					'ekraf_tenant_id' => $tenant->id,
					'employee_male_total' 	=> $request->employee_male_total,
					'employee_female_total'	=> $request->employee_female_total,
					'employee_general_fee'  => $request->employee_general_fee,
					'employee_elementary_school_total' => $request->employee_elementary_school_total,
					'employee_junior_high_total' => $request->employee_junior_high_total,
					'employee_high_total' => $request->employee_high_total,
					'employee_d1_total'   => $request->employee_d1_total,
					'employee_d3_total'   => $request->employee_d3_total,
					'employee_bacheloor_degree_total' => $request->employee_bacheloor_degree_total,
					'employee_post_graduate_total' => $request->employee_post_graduate_total,
					'employee_doctoral_degree_total' => $request->employee_doctoral_degree_total
				]);
			}


			if($request->has('asset')){
				$tenant->monetary()->create([
					'ekraf_tenant_id' => $tenant->id,
					'monetary_asset'  => $request->asset,
					'monetary_daily_omset' => $request->daily_omset,
					'monetary_monthly_omset' => $request->monthly_omset,
					'monetary_yearly_omset'  => $request->yearly_omset,
					'monetary_daily_profit'  => $request->daily_profit,
					'monetary_monthly_profit'=> $request->monthly_profit,
					'monetary_yearly_profit' => $request->yearly_profit,
					'monetary_access_financing' => $request->access_financing,
					'monetary_investment'    => implode(":", $request->investment)
				]);
			}

			if( $request->has('coaching') ){
				$data_coaches = [];
				foreach ($request->coaching as $key => $choaching) {
					$data_coaches[] = [
						'ekraf_tenant_id'	=> $tenant->id,
						'coaching' 			=> $choaching,
						'coaching_type' 	=> ($request->has('coaching_type')) ? $request->coaching_type[$key] : null,
						'coaching_year' 	=> ($request->has('coaching_year')) ? $request->coaching_year[$key] : null
					]; 
				}

				//save many
				$tenant->coaches()->createMany( $data_coaches );
			}

			if( $request->has('membership_name') ){
				$data_memberships = [];
				foreach ($request->membership_name as $key => $name) {
					$data_memberships[] = [
						'ekraf_tenant_id'	=> $tenant->id,
						'membership_name'	=> $name
					];
				}

				//save many
				$tenant->memberships()->createMany( $data_memberships );
			}

			if( $request->has('business_strength') ){
				$data_business_strengths = [];
				foreach ($request->business_strength as $key => $business_strength) {
					$data_business_strengths[] = [
						'ekraf_tenant_id'	=> $tenant->id,
						'bussiness_strength_title'	=> $business_strength
					];
				}

				//save many
				$tenant->business_strengths()->createMany( $data_business_strengths );
			}

			if( $request->has('business_opportunity') ){
				$data_business_opportunities = [];
				foreach ($request->business_opportunity as $key => $business_opportunity) {
					$data_business_opportunities[] = [
						'ekraf_tenant_id'	=> $tenant->id,
						'bussiness_opportunity_title'	=> $business_opportunity
					];
				}

				//save many
				$tenant->business_opportunities()->createMany( $business_opportunity );
			}

			//itineraries
			if( $request->has('itinerary_title') ){
				$data_itineraries = [];
				foreach ($request->itinerary_title as $key => $itinerary_title) {
					$data_itineraries[] = [
						'ekraf_tenant_id'	=> $tenant->id,
						'itineraries_title'	=> $itinerary_title,
						'itineraries_description' => ($request->has('itineraries_description')) ? $request->itineraries_description[$key] : null
					];
				}

				//save many
				$tenant->itineraries()->createMany( $data_itineraries );
			}

			if( $tenant->save() ){
				DB::commit();
				//hardcoded email
				Mail::to( 'ekraf.patrakomala@gmail.com' )->queue( new notifregistertenant($tenant) );
				$this->response['status'] = 'success';
				$this->response['message'] = 'createdTenant';
				$this->response['data']    =  $tenant->getDataApiProfile();
				return response()->json( $this->response, 201 );
			}
		} catch (Exception $e) {
			DB::rollback();
		}
	}

	function getVillages(Request $request){
		try {
			$subDistrict = SubDistrict::findOrFail( $request->subdistrict_id );
			if(empty($subDistrict)){
				$this->response['status'] 	= 'failed';
				$this->response['message']  = 'subDistrictNotFound';
				return response()->json( $this->response );
			}
				$this->response['status']  = 'success';
				$this->response['message'] = 'findData';
				$this->response['count']   = $subDistrict->villages->count();
				$this->response['data']    = $subDistrict->villages;
				return response()->json( $this->response );
		} catch (Exception $e) {
			$this->response['status'] = 'failed';
			$this->response['message'] = $e->getMessage();
			return response()->json( $this->response );
		}
	}
}
