<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Analytics\PageView;

class AnalyticController extends Controller
{
    function counterVisitPage(Request $request){
 		$log = PageView::create([
 			'ip_address' => $request->ip_address,
 			'os' 		 => $request->os,
 			'page_slug'  => $request->page_slug,
 			'device' 	 => $request->device,
 			'browser'    => $request->browser
 		]);

 		if($log->save()){
 			return response()->json([
 				'status' 	=> 201,
 				'message'   => 'successLogging',
 			], 201);
 		}
    }
}
