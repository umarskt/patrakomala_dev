<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TourPackages\TourismPackage;
use App\Models\TourPackages\TourismBelt;
use App\Models\Tenants\Tenant;
use Illuminate\Support\Facades\Input;

class TourismPackageController extends Controller
{
	 private $response = array(
		'status' => 'failed',
		'message' => 'not found data!',
		'data'   =>null,
		'meta'   =>null
	);

    public function getAllBelt(){
    	try {
    		$getDatas = TourismBelt::select('id','belt_title')->get();
    		$datas = [];

    		foreach ($getDatas as $key => $data) {
    			$datas[] = $data->getDataApi();
    		}

    		if(count($datas) > 0 ){
				$this->response['status'] = 'success';
				$this->response['message'] = 'Found Data';
				$this->response['data'] = $datas;
			}

			return response()->json($this->response);
    	} catch (Exception $e) {
    		$this->response['message'] = $e->getMessage();
    	}
    }

    public function getAllPackage(Request $request){
    	try {
    		
    		$getDatas = TourismPackage::join('belt_packages','tourism_package.id','=','belt_packages.tourism_package_id')
                        ->select('tourism_package.id','tourism_package.package_name','belt_packages.tourism_belt_id');
			
    		if( $request->has('belt_id') ){
    			$getDatas = $getDatas->where('belt_packages.tourism_belt_id','=', json_decode( $request->belt_id ))->get();
    		}else{
    			$getDatas = $getDatas->get();
    		}

    		$datas = [];
    		foreach ($getDatas as $key => $data) {
    			$datas[] = $data->getDataApi();
    		}

    		if(count($datas) > 0 ){
				$this->response['status'] = 'success';
				$this->response['message'] = 'Found Data';
				$this->response['data'] = $datas;
			}

			return response()->json($this->response);
    	} catch (Exception $e) {
    		$this->response['message'] = $e->getMessage();
    	}
    }

    public function filterPackage(Request $request){
    	try {
    		$tenants = Tenant::filterTourismPackage($request->filter);
    		$datas = [];
    		foreach ($tenants as $key => $tenant) {
                $datas[] = [
    				'id' => $tenant->id,
    				'latitude'=>$tenant->latitude,
    				'longitude'=>$tenant->longitude,
                    'tenant_logo' => $tenant->getImg(),
                    'tenant_name' => $tenant->tenant_name,
    			];
    		}
    		
    		if(count($datas) > 0 ){
    			$this->response['status'] = 'success';
    			$this->response['message'] = 'Found Data';
    			$this->response['data'] = $datas;
    		}
			
			return response()->json($this->response);
    		
    	} catch (Exception $e) {
    		$this->response['message'] = $e->getMessage();
    	}
    }

    public function listTenant(){
    	try {
    		$tenants = Tenant::select('id')->get();
    		$datas =[];
    		
    		foreach ($tenants as $key => $tenant) {
    			$datas[] = $tenant->address_map->toArray();
    		}
    		
    		if(count($datas) > 0 ){
				$this->response['status'] = 'success';
				$this->response['message'] = 'Found Data';
				$this->response['data'] = $datas;
			}

			return response()->json($this->response);

    	} catch (Exception $e) {
    		$this->response['message'] = $e->getMessage();
    	}
    }

    public function detailTenant(Request $request){
    	try {

    		if( $request->has('tenant_id') ){
    			$tenant = Tenant::find( $request->tenant_id );
    		}else{
    			$this->response['message'] = "request invalid";
    			return response()->json($this->response, 422);
    		}
    		
            if(!empty($tenant)){
                $this->response['status'] = 'success';
                $this->response['message'] = "Found data";
                $this->response['data'] = $tenant->getDataApi();
            }else{
                $this->response['status'] = 'success';
                $this->response['message'] = "Not Found data";
            }

    		return response()->json( $this->response );

    	} catch (Exception $e) {
    		$this->response['message'] = $e->getMessage();	
    	}
    }
}
