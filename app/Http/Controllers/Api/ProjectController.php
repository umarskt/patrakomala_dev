<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\notifProjectClient;
 use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Notification;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Models\AppLogin;
use App\Models\SubDistrict;
use App\Models\Projects\Project;
use App\Models\Projects\ProjectCategory;
use App\Models\Tenants\Category;
use App\Models\Tenants\Tenant;
use Carbon\Carbon;
use JWTAuth;
use DB;

class ProjectController extends Controller
{
    use Notifiable;
	private $response = array(
		'status' => 'failed',
		'message' => 'not found data!',
		'errors'  => 0,
		'count'  => 0, 
		'data'   =>[],
	);

    function postProject( Request $request ){
    	DB::beginTransaction();
    	try {
    		$user = JWTAuth::toUser( $request->token );
    		$project  = Project::create([
    			'project_title'=> $request->title,
    			'project_status' => 'open',
    			'project_type'  => strtolower($request->type), //just service or product
    			'project_description' => $request->description,
    			'project_estimated_cost'=>$request->estimated_cost,
    			'app_login_id'=>$user->id
    		]);
    		
    		if( $request->has('sub_sectors') ){
    			$categories = Category::select('id')->whereIn('id', $request->sub_sectors)->pluck('id');
	    		$data_categories = [];
	    		foreach ($request->sub_sectors as $key => $id_sub_sector) {
	    			$data_categories[] = [
	    				'category_id' => $id_sub_sector,
	    				'project_id'  => $project->id,
	    				'created_at'  => Carbon::now(),
	    				'updated_at'  => Carbon::now()
	    			];
	    		}
	    		DB::table('project_categories')->insert($data_categories);
    		}

    		//$project->project_categories()->create( $data_categories );
    		$users = AppLogin::getSubSectorTenants( $request->sub_sectors );
            $users = AppLogin::whereIn('app_email',$users)->get();
            Notification::send($users, new notifProjectClient);
            

    		if( $project->save() ){
    			DB::commit();
    			$this->response['status'] 	= 'success';
    			$this->response['message'] 	= 'succesPostJob';
    			return response()->json( $this->response );
    		}


    	} catch (JWTException $e) {
    		DB::rollback();
    		$this->response['message'] = $e->getMessage();
    		return response()->json($this->response);
    	}
    }

    function listProject( Request $request ){
    	try {
    		$user  = JWTAuth::toUser( $request->token );
    		
    		$data = [];
    		if(!empty($user)){
    			if($user->projects->count() == 0){
    				return response()->json( $this->response );
    			}
    			foreach ($user->projects as $key => $project) {
    				$data[] = [
    					'id' 	=>$project->id,
    					'title'	=>$project->project_title,
    					'description'=>$project->project_description,
    					'status' => $project->project_status,
    					'estimated_cost' => $project->project_estimated_cost,
    					'type' => $project->project_type
    				];
    			}
    		}

    		$this->response['status'] 	= 'succes';
    		$this->response['message']	= 'foundData';
    		$this->response['data']     = $data;
    		return response()->json( $this->response );
    	} catch (JWTException $e) {
    		$this->response['message'] = $e->getMessage();
    		return response()->json( $this->response );
    	}
    }
}
