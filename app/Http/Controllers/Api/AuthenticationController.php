<?php

namespace App\Http\Controllers\Api;

use Tymon\JWTAuth\Exceptions\JWTException;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Notifications\verificationClient;
use App\Notifications\forgotPasswordNotification;
use App\Models\User;
use App\Models\AppLogin;
use App\Models\Activation;
use App\Models\SosmedAccount;
use Sentinel;
use JWTAuth;
use DB;

class AuthenticationController extends Controller
{
	protected $response = [
        'errors'    => 0,
        'data'      => array(),
        'message'   => null
    ];

    protected $token;

    public function login(Request $request){
    	try {
            if( !$this->token = JWTAuth::attempt( ['app_username'  => $request->username, 'password'=> $request->password ] ) ){
                 $this->token = JWTAuth::attempt( ['app_email'  => $request->username, 'password'=> $request->password ] );
            }
            if($request->has('social')){
                $user = $this->LoginSosmed( $request );
                $this->response['data']['user'] = $user;
            }//checking user applogin is exist or not
            else if( !$this->token ){
                $this->response['errors']  = 1;
                $this->response['message'] = 'InvalidCredential';
                return response()->json($this->response, 401);
    		}else if( $user = AppLogin::checkApiLogin( $this->token ) ){
                //checking user is active or not
                if( !$user['is_active'] ){
                    $this->response['message'] = 'userNotActive';
                    return response()->json($this->response);
                }
                $this->response['data']['user'] = $user;
    		}
    	} catch (Exception $e) {
            $this->response['errors']   += 1;
    		$this->response['message']  = $e->getMessage();
    	}

    	$this->response['message'] = 'success';
    	$this->response['data']['token'] = $this->token;
        return response()->json($this->response);
    }

    private function LoginSosmed($request=''){
        $sosmed = SosmedAccount::whereSocial( $request->social )->whereSocialId( $request->social_id )->first();
        if(!$sosmed){
            $username = ($request->has('email')) ? $request->email : $request->username;
            $credential = [
                'app_username' => explode("@", $username)[0],
                'app_email'    => $username,
                'app_password' => str_random(8),
             ];

            if($request->user_type == 'client'){
                 $name = explode(' ', $request->name);
                $client = [
                    'client_first_name' => $name[0],
                    'client_last_name'  => $name[1],
                    'company_name' => null,
                    'user_type'  => 'client'
                ];
                //merging with credential
                $credential = array_merge($credential, $client);
            }
            DB::beginTransaction();
            try {
                $app_login = new AppLogin;
                $user = $app_login->registerAndActive($credential); //getUser
                SosmedAccount::create([
                    'app_login_id' => $user->id,
                    'social'       => $request->social,
                    'social_id'    => $request->social_id
                ]);
                DB::commit();
                $this->token = JWTAuth::fromUser($user);                
            } catch (Exception $e) {
                DB::rollback();
            }
        }else{
            $this->token = JWTAuth::fromUser($sosmed->appLogin);
        }
        $data = AppLogin::checkApiLogin( $this->token );
        return $data;
    }

    public function verification(Request $request){
        try {
            if(!$request->has('code')){
                $this->response['errors']  += 1;
                $this->response['message'] = 'InvalidRequest';
                return response()->json($this->response, 422);
            }

            $code = Activation::whereCode( $request->code )->first();
            if(empty($code)){
                $this->response['message'] = 'InvalidCode';
                return response()->json($this->response);
            }else if($code->updated_at->diffInHours() > 24){
                $this->response['errors']  += 1;
                $this->response['message'] = 'ExpiredCode';
                return response()->json($this->response);
            }
            $code->completed = true;
            $code->completed_at = Carbon::now();
            $code->appLogin->is_active = true;
            
            if($code->save() && $code->appLogin->save()){
                $this->response['message'] = 'verified';
                return response()->json($this->response);
            }
        } catch (Exception $e) {
            $this->response['message'] = $e->getMessage();
            return response()->json($this->response);
        }
    }

    public function reSendVerification(Request $request){
        try {
            if(! $request->has('email') ){
                $this->response['errors']   = 1;
                $this->response['message']  = 'InvalidRequest';
                return response()->json($this->response, 422);
            }

            $user = AppLogin::whereAppEmail( $request->email )->first();
            if(!$user){
                $this->response['errors']   = 1;
                $this->response['message']  = 'EmailNotFound';
                return response()->json($this->response);
            }
            if(!$user->activation){
                $this->response['message'] = 'userNotActive';
                return response()->json($this->response);
            }

            $user->activation->code = str_random(30).time(0);
            if( $user->activation->save() ){
                $user->notify( new verificationClient($user->activation->code) );
                $this->response['message']      = 'success';
                $this->response['data']['code'] = $user->activation->code;
                return response()->json($this->response);
            }
        } catch (Exception $e) {
            $this->response['errors'] = count($e->getMessage());
            $this->response['message'] = $e->getMessage();
            return response()->json( $this->response );
        }
    }

    public function resetPassword( Request $request ){
        DB::beginTransaction();
        try {
            if(! $request->has('email') ){
                $this->response['errors']   = 1;
                $this->response['message']  = 'InvalidRequest';
                return response()->json($this->response, 422);
            }

            $user = AppLogin::whereAppEmail( $request->email )->first();
            if(!$user){
                $this->response['errors']   = 1;
                $this->response['message']  = 'EmailNotFound';
                return response()->json($this->response);
            }

            if( !$user->is_active ){
                $this->response['message'] = 'userNotActive';
                return response()->json($this->response);
            }
            $generate_pin   = str_random(8);
            $token          = str_random(30);
            $user->generate_pin = $generate_pin;
            $user->app_password = Hash::make($generate_pin);
            $user->token        = $token;
            if( $user->save() ){
                DB::commit();
                $user->notify( new forgotPasswordNotification([
                    'generate_pin'  => $generate_pin,
                    'token'         => $token
                ]) );
                $this->response['message'] = 'successResetPassword';
                return response()->json( $this->response );
            }

        } catch (Exception $e) {
            DB::rollback();
            $this->response['errors'] = count($e->getMessage());
            $this->response['message'] = $e->getMessage();
            return response()->json( $this->response );
        }
    }

    public function changePassword(Request $request){
        try {
            if( !($request->has('pin') && $request->has('password') && $request->has('token') ) ){
                $this->response['message'] = 'InvalidRequest';
                return response( $this->response, 422 );
            }

            $user = AppLogin::whereGeneratePin( $request->pin )->whereToken( $request->token )->first();
            if(!$user){
                $this->response['message'] = 'invalidCode';
                return response( $this->response, 422 );
            }
            $user->app_password = Hash::make( $request->password );
            if( $user->save() ){
                $this->response['message'] = 'successChangePassword';
                return response()->json( $this->response );
            }

        } catch (Exception $e) {
            $this->response['errors'] = count($e->getMessage());
            $this->response['message'] = $e->getMessage();
            return response()->json( $this->response );
        }
    }
}
