<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Tenants\Category;

class SubSectorController extends Controller
{
	private $response = array(
		'status' => 'failed',
		'message' => 'not found data!',
		'data'   =>null
	);

	//Get All Sub Sector
    function getAllSubSector(){
    	
    	try {
    		$subs = Category::all();
    	    $data = [];
    		foreach ($subs as $key => $sub) {
    			$data[] = $sub->getDataApi();
    		}

    		if(count($data) > 0){
                $this->response['data'] = $data;
                $this->response['status'] = 'success';
                $this->response['message'] = 'found data';
            }

    		return response()->json($this->response);
    	} catch (Exception $e) {
    		$this->response['status'] 	= 'failed';
    		$this->response['message']  = $e->getMessage();
    		return response()->json($this->response);		
    	}
    }
}
