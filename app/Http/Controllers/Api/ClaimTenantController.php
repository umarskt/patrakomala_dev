<?php

namespace App\Http\Controllers\Api;

use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Controllers\Controller;
use App\Mail\NotifClaimTenant;
use Illuminate\Http\Request;
use App\Models\Tenants\TenantClaim;
use App\Models\Tenants\Tenant;
use JWTAuth;
use DB;
use Mail;

class ClaimTenantController extends Controller
{
    function claimTenant(Request $request){
		DB::beginTransaction();
		try {
			$tenant = Tenant::findOrFail( $request->tenant_id );
			if( !$request->has('token') ){
				$this->response['message'] = 'invalidRequest';
				return response()->json( $this->response, 422 );
			}

			$user = JWTAuth::toUser( $request->token ); //user app_login
			if(empty($tenant)){
				$this->response['status'] 	= 'failed';
				$this->response['message']  = 'tenantNotFound';
				return response()->json( $this->response );
			}

			if( isset($tenant->claimed) && $tenant->claimed->status == 'claimed' ){
				$this->response['status'] 	= 'failed';
				$this->response['message']  = 'tenantHasBeenClaim';
				return response()->json( $this->response, 422 );
			}

			$claim = TenantClaim::insert([
				'status' => 'claimed',
				'ekraf_tenant_id' => $tenant->id,
				'app_login_id' => $user->id
			]);

			
			if( $claim ){
				Mail::to( 'ekraf.patrakomala@gmail.com' )->queue( new NotifClaimTenant([
					'id' 		  => $tenant->id,
					'tenant_name' => $tenant->tenant_name,
					'tenant_email' => $tenant->tenant_email,
					'tenant_phone' => $tenant->tenant_phone
				]) );
				DB::commit();
				$this->response['status'] 	= 'success';
				$this->response['message'] 	= 'ClaimedTenant';
				return response()->json( $this->response, 200 );
			}
		} catch (JWTException $e) {
			DB::rollback();
			$this->response['status'] = 'failed';
			$this->response['message'] = $e->getMessage();
			return response()->json( $this->response );
		}
	}
}
