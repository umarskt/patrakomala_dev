<?php

namespace App\Http\Controllers\Api\Publics;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\verificationClient;
use App\Mail\NotifActivationUser;
use App\Models\AppLogin;
use Mail;
use Validator;
use DB;
use Sentinel;

class RegisterClientPublicController extends Controller
{
    public $response  = [
		'status' 	=> 200,
		'message' 	=> 'NoResult',
		'errors'    => [],
		'data' 		=> []
	];

	function register(Request $request){
		DB::beginTransaction();
    	try {
            $parameters = [
                'client_first_name' => $request->client_first_name,
                'client_last_name'  => $request->client_last_name,
                'app_email'         => $request->client_email,
                'client_password'   => $request->client_password,
                'company_name'      => $request->company_name,
                'client_password_confirmation' => $request->client_password_confirmation,
                'app_username' => explode("@", $request->client_email)[0]
            ];

    		$validator = Validator::make( $parameters, [
    			'client_first_name' => 'required|max:40',
    			'client_last_name'  => 'required|max:40',
    			'app_email' 	 => 'required|unique:app_logins|max:50',
                'app_username'   => 'unique:app_logins',
    			'client_password'   => 'required|min:7',
    			'client_password_confirmation'  => 'required|same:client_password'
    		] );

    		if( $validator->fails() ){
    			$this->response['message']  = 'InvalidRequest';
    			$this->response['errors'] 	= $validator->errors()->all();
    			$this->response['status']   = 422;
                return response()->json($this->response, 422);
    		}

            $register = new AppLogin;
            $register = $register->registerAndActive([
                    'app_username' => explode("@", $request->client_email)[0],
                    'app_email'    => $request->client_email,
                    'app_password' => $request->client_password,
                    'client_first_name' => $request->client_first_name,
                    'client_last_name'  => $request->client_last_name,
                    'company_name'      => $request->company_name,
                    'user_type'         => 'client' 
                ], false);

            if( $register->save() ){
                DB::commit();
                Mail::to( $request->client_email )->queue( new NotifActivationUser( $parameters ) );
                $this->response['status'] = 201;
                $this->response['message'] = 'Created';
                $this->response['data']    = $register;
                return response()->json($this->response, 201);
            }

    	} catch (Exception $e) {
            DB::rollback();
    		$this->response['message'] = $e->getMessage();
            return response()->json($this->response);
    	}
	}
}
