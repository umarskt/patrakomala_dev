<?php

namespace App\Http\Controllers\Api\Publics;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SubDistrict;

class PublicListSubDistrict extends Controller
{
	private $response = array();
    function getAllSubDistrict(){
		try {
			$getData = SubDistrict::all();
			$datas = [];

			foreach ($getData as $key => $data) {
				$datas[] = $data->getDataApi();
			}

			if(count($datas) > 0 ){
				$this->response['status'] = 200;
				$this->response['message'] = 'FoundData';
				$this->response['data'] = $datas;
			}

			return response()->json($this->response);

		} catch (Exception $e) {
			$this->response['message'] = $e->getMessage();
		}
	}

}
