<?php

namespace App\Http\Controllers\Api\Publics;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tenants\Tenant;
use App\Http\Requests\Publics\PublicTenantRequest;

class PublicTenantController extends Controller
{
	 private $response = array(
		'status'  => 200,
		'message' => 'NoResult',
		'errors'  => 0
	);

    public function listTenant(Request $request){
        try {
            if( $request->has('filter') ){
                $tenants = Tenant::getBasedCategory($request->page, $request->filter);
            }else{
                $tenants = Tenant::getBasedCategory($request->page);
            }

            $datas = [];
            foreach ($tenants as $key => $tenant) {
                $datas[] = $tenant->ListDataApi();
            }

            if(count($tenants) == 0){
                $this->response['status'] = 200;
                $this->response['message'] = 'NoResult';
                return response()->json($this->response);
            }else{
                $this->response['status'] = 'success';
                $this->response['message'] = 'FoundData';
            }
            $paginate = $tenants->toArray();
            $this->response['data'] = $datas;
            $this->response['count'] = count($datas);
            $this->response['meta'] = [
                    'current_page' => $paginate['current_page'],
                    'from' => $paginate['from'],
                    'last_page' => $paginate['last_page'],
                    'path' => $paginate['path'],
                    'per_page' => $paginate['per_page'],
                    'prev_page_url' => $paginate['prev_page_url'],
                    'next_page_url' => $paginate['next_page_url'],
                    'to' => $paginate['to'],
                    'total'=> $paginate['total']
                ];
            return response()->json($this->response);
        } catch (Exception $e) {
            $this->response['message'] = $e->getMessage();
            return response()->json($this->response);
        }
    }

    public function tenantDetail(PublicTenantRequest $request){
        try {
            if( $request->has('tenant_id')  ){
                $detail = Tenant::find($request->tenant_id);
                if(!empty($detail)){
                    $this->response['status'] = 200;
                    $this->response['message'] = "FoundData";
                    $this->response['data'] = $detail->getDataApiProfile();
                }else{
                    $this->response['status'] = 200;
                    $this->response['message'] = "NoResult";
                }

                return response()->json( $this->response );
            }else{
                $this->response['message'] = "InvalidRequest";
                return response()->json($this->response, 422);
            }
        } catch (Exception $e) {
            $this->response['message'] = $e->getMessage();
            return response()->json($this->response);
        }
    }
}
