<?php

namespace App\Http\Controllers\Api\Publics;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Publics\PublicEventRequest;
use App\Models\Events\Event;
use Validator;

class PublicEventController extends Controller
{
	public $response  = [
		'status' 	=> 200,
		'message' 	=> 'NoResult',
		'errors'    => [],
		'data' 		=> []
	];

    function listEvent(PublicEventRequest $request){

    	$contents = Event::filterAllContent($request->page,['event'=>$request->event]);
    	$datas = [];
    	foreach ($contents as $key => $content) {
    		$datas[] = $content->getListDataApi();
    	}

    	if( empty( $datas ) ){
    		return response()->json( $this->response );
    	}

    	$paginate = $contents->toArray();
		
		$this->response['message'] = 'FoundData';
		$this->response['count']  = count($datas);
		$this->response['data'] = $datas;
		$this->response['meta'] = [
			'current_page' => $paginate['current_page'],
			'from' => $paginate['from'],
			'last_page' => $paginate['last_page'],
			'path' => $paginate['path'],
			'per_page' => $paginate['per_page'],
			'prev_page_url' => $paginate['prev_page_url'],
			'to' => $paginate['to'],
			'total'=> $paginate['total']
		];

		return response()->json( $this->response );

    }

    function detailContent(PublicEventRequest $request){
    	$content = Event::find( $request->content_id );
    	
    	if( empty($content) ){
    		return response()->json( $this->response );
    	}

    	if($content->event_content_type == "event"){
            $content = $content->getDataApiEvent();
        }else{
            $content = $content->getDataApiNews();
        }

        $this->response['message'] = 'FoundData';
        $this->response['data']    = $content;
        return response()->json( $this->response );
    }
}
