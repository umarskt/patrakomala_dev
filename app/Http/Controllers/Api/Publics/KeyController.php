<?php

namespace App\Http\Controllers\Api\Publics;

use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JWTAuth;

class KeyController extends Controller
{
    private $response = array(
		'status'  => 200,
		'message' => 'not found data!',
		'errors'  => 0
	);

	public function generateAccessToken( Request $request ){
        try {
            if( !$request->has('token') ){
                $this->response['message'] = 'invalidRequest';
                return response()->json( $this->response, 422 );
            }
            $user = JWTAuth::toUser( $request->token );
            $rand = str_random(30);
            $user->client->api_key = $rand;

            if( $user->client->save() ){
                $this->response['status']  = 200;
                $this->response['message'] = 'generated';
                $this->response['data']['key']  = $rand;
                return response()->json( $this->response, 200 );
            }
        } catch (JWTException $e) {
            $this->response['status']   = 422;
            $this->response['message']  = $e->getMessage();
            return response()->json( $this->response, 422 );
        }
    }

}
