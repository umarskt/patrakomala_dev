<?php

namespace App\Http\Controllers\Api\Publics;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tenants\Category;

class PublicListSubsector extends Controller
{
    //Get All Sub Sector
    function getAllSubSector(){
    	
    	try {
    		$subs = Category::all();
    	    $data = [];
    		foreach ($subs as $key => $sub) {
    			$data[] = $sub->getDataApi();
    		}

    		if(count($data) > 0){
                $this->response['data'] = $data;
                $this->response['status'] = 200;
                $this->response['message'] = 'FoundData';
            }

    		return response()->json($this->response);
    	} catch (Exception $e) {
    		$this->response['status'] 	= 'failed';
    		$this->response['message']  = $e->getMessage();
    		return response()->json($this->response);		
    	}
    }
}
