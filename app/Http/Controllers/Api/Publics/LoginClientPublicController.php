<?php

namespace App\Http\Controllers\Api\Publics;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Models\User;
use App\Models\AppLogin;
use App\Models\Activation;
use App\Models\SosmedAccount;
use Sentinel;
use JWTAuth;
use DB;

class LoginClientPublicController extends Controller
{
    public $response  = [
		'status' 	=> 200,
		'message' 	=> 'NoResult',
		'errors'    => [],
		'data' 		=> []
	];

	 public function login(LoginRequest $request){
    	try {
            if( !$this->token = JWTAuth::attempt( ['app_username'  => $request->username, 'password'=> $request->password ] ) ){
                 $this->token = JWTAuth::attempt( ['app_email'  => $request->username, 'password'=> $request->password ] );
            }
            if($request->has('social')){
                $user = $this->LoginSosmed( $request );
                $this->response['data']['user'] = $user;
            }//checking user applogin is exist or not
            else if( !$this->token ){
            	$this->response['status']  = 401;
                $this->response['message'] = 'InvalidCredential';
                return response()->json($this->response, 401);
    		}else if( $user = AppLogin::checkApiLogin( $this->token ) ){
                //checking user is active or not
                if( !$user['is_active'] ){
                    $this->response['message'] = 'userNotActive';
                    return response()->json($this->response);
                }
                $this->response['data']['user'] = $user;
    		}
    	} catch (Exception $e) {
            $this->response['errors']   += 1;
    		$this->response['message']  = $e->getMessage();
    	}

    	$this->response['message'] = 'success';
    	$this->response['data']['token'] = $this->token;
        return response()->json($this->response);
    }
}
