<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Api\EventApiRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Models\Events\Event;
use App\Models\Events\EventImage;
use App\Models\Events\EventUrl;
use App\Models\Events\TagEvent;
use App\Mail\NotifEventAdmin;
use Carbon\Carbon;
use Mail;
use DB;
use JWTAuth;
use Validator;
use Illuminate\Validation\ValidationException;

class EventController extends Controller
{
    function store(EventApiRequest $request){
        try {
            $user = JWTAuth::toUser( $request->token );
        } catch (JWTException $e) {
            return response()->json([
                'status' => 422,
                'message'=>'InvalidToken'
            ]);
        }

        DB::beginTransaction();
        try {
            
            $start_date = Carbon::createFromTimestamp($request->start_date);
    		$end_date   = Carbon::createFromTimestamp($request->end_date);
            
            $event = Event::create( [
    			'title'=> $request->title,
    			'description' => $request->content,
    			'take_place'  => $request->place,
                'start_date'  => $start_date->format('Y-m-d'),
                'end_date'    => $end_date->format('Y-m-d'),
    			'start_time'  => $start_date->format('H:i:s'),
                'end_time'    => $end_date->format('H:i:s'),
                'event_content_type' => $request->event_content_type,
                'status' => 'draft',
                'is_active' => false,
                'user_app_login_id' => $user->id
    		] );

            if( $request->has('tags') ){
                $event->saveTagBySlug( $event->id, $request->tags );
            }

            if( $request->has('urls') ){
                $data_urls = [];
                foreach ($request->urls as $key_url => $url) {
                    $data_urls[] = [
                        'event_id' => $event->id,
                        'url'      => $url,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ];
                }

                EventUrl::insert( $data_urls );
            }

    		DB::commit();
            $data_event = [
                'event_id'=>$event->id,
                'event_title' => $event->title,
                'start_date'  => $start_date->format('Y-m-d'),
                'end_date'    => $end_date->format('Y-m-d')
            ];

            Mail::to( config('ekraf_config.mail_admin') )->send( new  NotifEventAdmin( $data_event ));

            $response = [
                'status'    => 201,
                'message'   => 'Created',
                'data'      => $data_event
            ];

            return response()->json($response, 201);
    	} catch (Exception $e) {
    		DB::rollback();	
    	}
    }

    public function uploadEventImage(Request $request){
        $validator = Validator::make($request->all(),[
            'id' => 'required',
            'token' => 'required',
            'images.*' => 'required|image|max:7000'
        ]);

        if( $validator->fails() ){
            return response()->json([
                'status' => 422,
                'message' => 'InvalidRequest',
                'errors' => $validator->errors()->all()
            ]);
        }
        try {
            $user  = JWTAuth::toUser( $request->token );
        } catch (JWTException $e) {
            return response()->json([
                'status' => 422,
                'message'=>'InvalidToken'
            ]);
        }

        $datas = [];
        foreach ($request->file('images') as $key => $file) {
            $filename = time().'.'.$file->getClientOriginalExtension();
            $file->move( EventImage::getPath(), $filename );
            $datas[] = [
                'event_id' => $request->id,
                'img_event'=>$filename,
                'img_name' =>$file->getClientOriginalName(),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ];

            EventImage::insert($datas);
        }

        $response = [
            'status'    => 201,
            'message'   => 'Created'
        ];

        return response()->json( $response, 201);
    }

    public function editContent(EventApiRequest $request, $id){
        try {
            $user = JWTAuth::toUser( $request->token );
        } catch (JWTException $e) {
            return response()->json([
                'status' => 422,
                'message'=>'InvalidToken'
            ]);
        }
        DB::beginTransaction();
        try {
            $event = Event::whereId($request->id)->whereStatus('draft')->first();
            $start_date = Carbon::createFromTimestamp($request->start_date);
            $end_date   = Carbon::createFromTimestamp($request->end_date);

            $event->title = $request->title;
            $event->description = $request->content;
            $event->take_place = $request->place;
            $event->start_date = $start_date->format('Y-m-d');
            $event->end_date = $end_date->format('Y-m-d');
            $event->start_time   = $start_date->format('H:i:s');
            $event->end_time     = $end_date->format('H:i:s');

            if( $request->has('tags') ){
                TagEvent::whereEventId( $event->id )->delete();
                $event->saveTagBySlug( $event->id, $request->tags );
            }

            if( $request->has('urls') ){
                EventUrl::whereEventId( $event->id )->delete();
                foreach ($request->urls as $key_url => $url) {
                    $data_urls[] = [
                        'event_id' => $event->id,
                        'url'      => $url,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ];
                }

                EventUrl::insert( $data_urls );
            }

            if( $event->update() ){
                DB::commit();  
            }

            return response()->json([
                'status' => 202,
                'message' => 'Updated'
            ]);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json([
                'status'  => 500,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function listEventUser(Request $request){
        try {
            $user = JWTAuth::toUser( $request->token );
        } catch (JWTException $e) {
            return response()->json([
                'status' => 422,
                'message'=>'InvalidToken'
            ]);
        }

        $events = Event::whereUserAppLoginId($user->id)->get();
        $datas = [];
        foreach ($events as $key => $event) {
            $datas[] = $event->getDataApiEvent();
        }

        $response['status'] = 200;
        $response['message'] = (empty($datas)) ? 'NotFound' : 'FoundData';
        $response['data'] = $datas;

        return response()->json( $response );
    }

    public function detailEventUser(Request $request){

        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'event_id' => 'required'
        ]);

        if( $validator->fails() ){
            throw new ValidationException( $validator );
        }

        try {
            $user = JWTAuth::toUser( $request->token );
        }catch (Exception $e) {
            return response()->json([
                'status' => 422,
                'message'=>'InvalidToken'
            ]);
        }

        $event = Event::whereId( $request->event_id )->whereUserAppLoginId( $user->id )->first();

        if(is_null($event)){
            return response()->json([
                'status'    => 200,
                'message'   => 'EmptyData'
            ]);
        }

        return response()->json([
            'status' => 200,
            'message' => 'foundData',
            'data' => $event->getDataApiEvent()
        ]);

    }
}
