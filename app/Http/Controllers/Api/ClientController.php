<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Notifications\verificationClient;
use Illuminate\Http\Request;
use App\Models\AppLogin;
use Validator;
use DB;
use Sentinel;

class ClientController extends Controller
{

    private $response = array(
        'status' => 'failed',
        'message' => 'not found data!',
        'data'   =>[],
    );

    function register(Request $request){
        DB::beginTransaction();
    	try {
            $filtering = [
                'client_first_name' => $request->client_first_name,
                'client_last_name'  => $request->client_last_name,
                'app_email'         => $request->client_email,
                'client_password'   => $request->client_password,
                'company_name'      => $request->company_name,
                'client_password_confirmation' => $request->client_password_confirmation,
                'app_username' => explode("@", $request->client_email)[0]
            ];

    		$validator = Validator::make( $filtering, [
    			'client_first_name' => 'required|max:40',
    			'client_last_name'  => 'required|max:40',
    			'app_email' 	 => 'required|unique:app_logins',
                'app_username'   => 'unique:app_logins',
    			'client_password'   => 'required|min:7',
    			'client_password_confirmation'  => 'required|same:client_password'
    		] );

    		if( $validator->fails() ){
                $this->response['status'] = 422;
                $this->response['errors'] = $validator->errors()->all();
                $this->response['message'] = 'InvalidRequest';
                return response()->json($this->response, 422);
    		}
            
            $register = new AppLogin;
            $register = $register->register([
                    'app_username' => explode("@", $request->client_email)[0],
                    'app_email'    => $request->client_email,
                    'app_password' => $request->client_password,
                    'client_first_name' => $request->client_first_name,
                    'client_last_name'  => $request->client_last_name,
                    'company_name'      => $request->company_name,
                    'user_type'         => 'client' 
                ], false);

            if( $register->save() ){
                DB::commit();
                $register->notify( new verificationClient($register->activation->code) );
                $this->response['status'] = 'success';
                $this->response['message'] = 'success create user';
                $this->response['data']    = $register;
                return response()->json($this->response);
            }

    	} catch (Exception $e) {
            DB::rollback();
    		$this->response['message'] = $e->getMessage();
            return response()->json($this->response);
    	}
    }
}
