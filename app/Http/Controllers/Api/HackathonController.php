<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterHackathonRequest;
use App\Models\Hackathon\TeamHackathon;
use App\Models\Hackathon\MemberTeamHackathon;
use App\Mail\notifHackathon;
use Validator;
use Mail;
use DB;

class HackathonController extends Controller
{

	private $response = array(
		'status'  => 200,
		'message' => 'not found data!',
		'errors'  => 0
	);

    function RegisHackathon(RegisterHackathonRequest $request){
        if( TeamHackathon::whereContactEmail( $request->data['contact_email'] )->first() ){
            $this->response['status']   = 422;
            $this->response['message']  = 'EmailWasExists';
            $this->response['errors']   = 1;
            return response()->json( $this->response, 422);
        }
        DB::connection('hackathon')->beginTransaction();
        try {

            $key = str_random(30);
            $hackathon = TeamHackathon::create([
                'team_name'     => $request->data['team_name'],
                'contact_email' => $request->data['contact_email'],
                'contact_phone' => $request->data['contact_phone'],
                'access_key'    => $key
            ]);

            foreach ($request->data['members'] as $key => $member) {
                $datas[] = [
                    'mbr_full_name' => $member['name'],
                    'mbr_email' => $member['email'],
                    'mbr_phone' => $member['phone'],
                    'mbr_phone_emergency' => $member['emergency_phone'],
                    'mbr_address_emergency' => $member['emergency_address'],
                    'mbr_name_contact_emergency' => $member['emergency_name'],
                    'mbr_ill' => isset($member['ill']) ? $member['ill'] : null,
                    'team_id' => $hackathon->id
                ];
            }
            
            $hackathon->members()->insert( $datas );
            DB::connection('hackathon')->commit();
           /* Mail::to( $request->data['contact_email'] )->queue( new notifHackathon([
                'team_name' => $request->data['team_name'],
                'contact_phone' => $request->data['contact_phone'],
                'access_key'    => $key,
                'members'       => $datas
            ]) );*/
            $this->response['status']   = 201;
            $this->response['message']  = 'created';
            return response()->json( $this->response, 201 );
        } catch (Exception $e) {
            DB::connection('hackathon')->rollback();
        }
    }

    function generate(Request $request){
        $team = TeamHackathon::findOrFail( $request->id );
        $generate  = generateChar();
        $team->access_key = $generate;
        if( $team->save() ){
            Mail::to( $team->contact_email )->queue( new notifHackathon([
                'team_name' => $team->team_name,
                'contact_phone' => $team->contact_phone,
                'contact_email' => $team->contact_email,
                'access_key'    => $generate
            ]) );

            return response()->json([
                'status' => 'success',
                'access_key' => $generate
            ]);
        }
    }
}
