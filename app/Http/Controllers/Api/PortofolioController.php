<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tenants\TenantPortofolio;

class PortofolioController extends Controller
{
	private $response = array(
		'status' => 'failed',
		'message' => 'not found data!',
		'errors'  => 0,
		'count'  => 0
	);

    function removePortofolio(Request $request){
    	try {
    		if(!$request->has('portofolio_id')){
    			$this->response['message'] = 'invalidRequest';
    			$this->response['errors']  = 1;
    			return response()->json( $this->response, 422 );
    		}

    		$portofolio = TenantPortofolio::find( $request->portofolio_id );
            if( empty($portofolio) ){
                $this->response['message'] = 'PortofolioNotFound';
                $this->response['errors']  = 1;
                return response()->json( $this->response );
            }

            $portofolio->deletePortofolio();
            $portofolio->delete();

            $this->response['message'] = 'success';
            return response()->json( $this->response );
    	} catch (Exception $e) {
    		$this->response['message'] = 'PortofolioNotFound';
            $this->response['errors']  = 1;
            return response()->json( $this->response );
    	}
    }
}
