<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Events\Event;

class ContentController extends Controller
{
	private $response = array(
		'status' => 'failed',
		'message' => 'not found data!',
		'count'  => 0, 
		'data'   =>null,
		'meta'   =>null
	);

    public function getAllContent(Request $request){
    	try {
    		/*dd($request->filter['news']);*/
    		$contents = Event::filterAllContent($request->page,$request->filter, $request->keyword);
    		$datas = [];
    		foreach ($contents as $key => $content) {
    			$datas[] = $content->getListDataApi();
    		}

    		if(count($datas) > 0 ){
    			$paginate = $contents->toArray();
				$this->response['status'] = 'success';
				$this->response['message'] = 'Found Data';
				$this->response['count']  = count($datas);
				$this->response['data'] = $datas;
				$this->response['meta'] = [
                    'current_page' => $paginate['current_page'],
                    'from' => $paginate['from'],
                    'last_page' => $paginate['last_page'],
                    'path' => $paginate['path'],
                    'per_page' => $paginate['per_page'],
                    'prev_page_url' => $paginate['prev_page_url'],
                    'to' => $paginate['to'],
                    'total'=> $paginate['total']
                ];
			}else{
				$this->response['status'] = 'success';
				$this->response['message'] = 'Not Found Data';
				$this->response['count']  = count($datas);
			}

			return response()->json($this->response);
    	} catch (Exception $e) {
			$this->response['message'] = $e->getMessage();
    	}
    }

    function detailContent(Request $request){
    	try {

    		if( $request->has('content_id') ){
    			$content = Event::find( $request->content_id );
    		}else{
    			$this->response['message'] = "request invalid";
    			return response()->json($this->response, 422);
    		}

            if( empty($content) ){
                $this->response['message'] = "wrong id event";
                return response()->json($this->response);
            }
    		
            if($content->event_content_type == "event"){
                $content = $content->getDataApiEvent();
            }else{
                $content = $content->getDataApiNews();
            }

            if(!empty($content)){
                $this->response['status'] = 'success';
                $this->response['message'] = "Found data";
                $this->response['data'] = $content;
            }else{
                $this->response['status'] = 'success';
                $this->response['message'] = "Not Found data";
            }

    		return response()->json( $this->response );

    	} catch (Exception $e) {
    		$this->response['message'] = $e->getMessage();	
            return response()->json($this->response);
    	}
    }
}
