<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tag;

use Validator;

class TagEventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.tagEvent.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tagEvent.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $validation = Validator::make( $request->all(), [
                'name' => 'required|max:30|min:5',
                'slug' => 'required|max:30|min:5|unique:tags'
            ] );

            if( $validation->fails() ){
                $request->session()->flash('failed','not valid input');
                return redirect()->back()->withInput()->withErrors( $validation->messages() );
            }else {
                try {
                    Tag::create( $request->all() );
                    $request->session()->flash('success','Success Input Tag');
                    return redirect()->back();
                } catch (Exception $e) {
                    $request->session()->flash('failed',$e->getMessage());
                }
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tag = Tag::findOrFail( $id );
        return view('admin.tagEvent.edit', compact('tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
           $validation = Validator::make( $request->all(), [
                'name' => 'required|max:30|min:5',
                'slug' => 'required|max:30|min:5|unique:tags'
            ] );

           if( $validation->fails() ){
                $request->session()->flash('failed','not valid input');
                return redirect()->back()->withInput()->withErrors( $validation->messages() );
           }else{
                try {
                    $tag = Tag::findOrFail( $id );
                    if($tag->update( $request->all() )){
                        $request->session()->flash('success','Success Update Tag event'); 
                    }
                    return redirect()->back();
               } catch (Exception $e) {
                    $request->session()->flash('failed',$e->getMessage());
               }
           }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        try {
            $tag = Tag::findOrFail( $id );
            $tag->delete();
            $request->session()->flash('success','Success delete Tag Event');
            return redirect()->back();
        } catch (Exception $e) {
            $request->session()->flash('failed',$e->getMessage()); 
        }
    }

    public function get_data(){
        $tags = Tag::all();
        return datatables($tags)
        ->addColumn('action', function($tag) {
            $url = route('admin.event.tag.edit', $tag->id);
            $delete_url = route('admin.event.tag.delete', $tag->id);
            return view('admin.forms.action-buttons',[
                'edit_url' => $url
            ]).
            view('admin.forms.action-buttons', [
                'delete_url' => $delete_url
            ]);
        })->make(true);
    }
}
