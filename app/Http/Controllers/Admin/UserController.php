<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Yajra\Datatables\Datatables as Datatables;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use DB;
use Sentinel;
use Session;

class UserController extends Controller
{
    public function index(){
    	return view('admin.user.index');
    }

    public function edit( $id ){
        $user = User::find($id);
        return view('admin.user.edit',compact('user'));
    }

    public function create(){
        return view('admin.user.create');
    }

    public function store(UserRequest $request){
        DB::beginTransaction();
        try {
            Sentinel::registerAndActivate( $request->all() );
            $user = Sentinel::findByCredentials( [ 'login' => $request->email ] );
            Sentinel::findRoleBySlug( 'system-administrator' )->users()->attach( $user );
            DB::commit();
            return redirect()->route('admin.user.index');
        } catch (Exception $e) {
            DB::rollback();
        }
    }

    public function update( $id, UserRequest $request){
        
        DB::beginTransaction();
        try {
            $user = User::findOrFail($id);
            Sentinel::update( $user ,$request->all() );
             DB::commit();
            $request->session()->flash('success');
            return redirect()->route('admin.user.index');
        } catch (Exception $e) {
            DB::rollback();
            flash()->error($e->getMessage());
        }
    }

    public function get_data(){
        $users = User::all();
        return datatables($users)
        ->addColumn('action', function($user) {
            $url = route('admin.user.edit',$user->id);
            $delete_url = route('admin.user.delete',$user->id);
            return view('admin.forms.action-buttons',[
                'edit_url' => $url
            ]).
            view('admin.forms.action-buttons', [
                'delete_url' => $delete_url
            ]);
        })->make(true);
    }


    public function destroy(Request $request,$id)
    {
        try {
            $user = User::findOrFail( $id );
            $user->delete();
            $request->session()->flash('success', 'Success delte user');
            return redirect()->route('admin.user.index');
        } catch (Exception $e) {
            
        }
    }
}
