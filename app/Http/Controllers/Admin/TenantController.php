<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\TenantRequest;
use Illuminate\Support\Facades\Storage;

use App\Models\Tenants\Tenant;
use App\Models\Tenants\Category;
use App\Models\SubDistrict;
use App\Models\AppLogin;
use App\Models\Tenants\TenantGallery;
use App\Models\Tenants\TenantClient;
use App\Models\Tenants\TenantAlbum;
use App\Mail\tenantNotification;
use Illuminate\Support\Facades\Mail;

use Sentinel;
use DB;
use Carbon\Carbon;
use Session;
use Html;
use Mapper;
use File;
use View;


class TenantController extends Controller
{
    public function index(){
    	return view('admin.tenant.index');
    }

    public function create(){
        $role = Sentinel::findRoleBySlug('tenant');
        $users = $role->users()->with('roles')->pluck('full_name','id');
        $listSubDistrict = SubDistrict::select('id','sub_district_name')->pluck('sub_district_name','id');
        $SubSectors = Category::select('id','category_name')->get()->pluck('category_name','id');
        Mapper::map('-6.914744', '107.609810',['draggable' => true,'zoom'=>12, 'animation' => 'DROP', 
          'eventDrag' => "
            $('[name=latitude]').val(event.latLng.lat());
            $('[name=longitude]').val(event.latLng.lng());

          ",
        ]);
        return view('admin.tenant.create',compact('users','listSubDistrict','SubSectors'));
    }

    public function edit($id){
        try {
            $tenant = Tenant::find( $id );
            $listSubDistrict = SubDistrict::select('id','sub_district_name')->pluck('sub_district_name','id');
            $SubSectors = Category::select('id','category_name')->get()->pluck('category_name','id');

            $datas = $tenant->getDataEdit();

            Mapper::map('-6.914744', '107.609810',['draggable' => true,'zoom'=>12, 'animation' => 'DROP', 
              'eventDrag' => "
                $('[name=latitude]').val(event.latLng.lat());
                $('[name=longitude]').val(event.latLng.lng());
              ",
            ]);

            return view('admin.tenant.edit',compact('tenant','listSubDistrict','SubSectors', 'datas'));
        } catch (Exception $e) {
            abort(404);
        }
    }

    //Method for approval tenant
    public function approvalTenant(Request $request){
      DB::beginTransaction();

      try {
        $tenant = Tenant::find($request->id);
        $register = new AppLogin;
        $check = AppLogin::whereAppEmail( $tenant->tenant_email )->first();
        if(!empty($check)){
          $request->session()->flash('failed','Email was exists!');
          $response = [
            'status' => false,
            'view'   => View::make('admin.main.flash-message')->render()
          ];

          return response()->json($response);
        }
        $app_password = str_random(8);
        $app_login = [
          'app_username' => explode("@", $tenant->tenant_email)[0],
          'app_email'    => $tenant->tenant_email,
          'app_password' => $app_password,
          'tenant_id'    => $tenant->id,
          'user_type'    => 'tenant'
        ];
        $register->registerAndActive($app_login);

        DB::commit();
        Mail::to( $tenant->tenant_email )->queue( new tenantNotification($app_login) );
        $request->session()->flash('success','Success Approve tenant');
        $response = [
            'status' => true,
            'view'   => View::make('admin.main.flash-message')->render()
        ];

        return response()->json($response);
      } catch (Exception $e) {
        DB::rollback();
      }
    }

    public function show( $id ){
        $tenant = Tenant::findOrFail($id);
        $tenant = $tenant->getDataDetail();

        Mapper::map('-6.914744', '107.609810',['zoom'=>12, 'animation' => 'DROP']);
        return view('admin.tenant.show', compact('tenant'));
    }

    public function get_data(){
    	$tenants = Tenant::all();
        return datatables($tenants)
        ->editColumn('img_logo', function($tenants){
            return Html::image( $tenants->getImg(), $tenants->name, ['width'=>'30'] );
        })
        ->addColumn('action', function($tenant) {
            $url = route('admin.tenant.edit', $tenant->id);
            $delete_url = route('admin.tenant.delete', $tenant->id);
            $show_url = route('admin.tenant.show', $tenant->id);
            return view('admin.forms.action-buttons',[
                'edit_url' => $url
            ]).
            view('admin.forms.action-buttons', [
                'delete_url' => $delete_url
            ]).view('admin.forms.action-buttons',[
                'show_url' => $show_url
            ]);
        })->make(true);
    }

    public function get_data_user(){
      $user_tenants = AppLogin::get_user();
      return datatables($user_tenants)
        ->addColumn('action', function($user_tenants) {
            $url = '';
            $delete_url = '';
            $show_url = '';
            return view('admin.forms.action-buttons',[
                'edit_url' => $url
            ]).
            view('admin.forms.action-buttons', [
                'delete_url' => $delete_url
            ]).view('admin.forms.action-buttons',[
                'show_url' => $show_url
            ]);
        })->make(true);    
    }

    public function get_data_user_client(){
      $user_tenants = AppLogin::get_user();
      return datatables($user_tenants)
        ->addColumn('action', function($user_tenants) {
            $url = '';
            $delete_url = '';
            $show_url = '';
            return view('admin.forms.action-buttons',[
                'edit_url' => $url
            ]).
            view('admin.forms.action-buttons', [
                'delete_url' => $delete_url
            ]).view('admin.forms.action-buttons',[
                'show_url' => $show_url
            ]);
        })->make(true);    
    }

    public function store(TenantRequest $request){
        
        DB::beginTransaction();
        try {
            if( $request->hasFile('img_logo') ){
                
                $filename = time() . '.' . $request->file( 'img_logo' )->getClientOriginalExtension();
                $request->file('img_logo')->move( Tenant::getPath(), $filename );
            }

           //Save Tenant
           $tenant = Tenant::create( [
                'tenant_name' => $request->name,
                'founding_year' => Carbon::parse($request->founding_year)->format('Y-m-d'),
                'about' => $request->about,
                'img_logo' => (!empty($filename)) ? $filename : null,
                'tenant_email' => $request->tenant_email,
                'tenant_phone' => $request->tenant_phone,
                'status'       => 'waiting' //hardcoded for status tenant.
           ] );

           $tenant->address()->create([
              'ekraf_tenant_id' => $tenant->id,
              'street'          => $request->street,
              'subdistrict_id'  => $request->subdistrict,
              'postal_code'     => $request->postal_code,
              'latitude'        => $request->latitude,
              'longitude'       => $request->longitude 
           ]);

           //Tenant Founder
           $tenant->founders()->create([
                'ekraf_tenant_id' => $tenant->id,
                'founder_name'    => $request->founder_name,
                'founder_last_education' => $request->founder_last_education,
                'founder_last_education_department'  => $request->founder_last_education_department,
                'gender'     => $request->gender
           ]);

           //Tenant Legality
           $tenant->legality_docs()->create([
              'ekraf_tenant_id' => $tenant->id,
              'legality_doc_name' => $request->legality_doc_name,
              'tenant_legality_img' => null
           ]);

           //Album
           if(  !empty($request->album_title) || !empty($request->album_description) ){
              if( $request->has('id_album') ){
                $album = TenantAlbum::findOrFail( $request->id_album );
                $album->album_name = (isset( $request->album_title )) ? $request->album_title : $album->album_name;
                $album->album_description = (isset( $request->album_description )) ? $request->album_description : $album->album_description;
                $album->save();
              }else{
                $album = TenantAlbum::create([
                  'ekraf_tenant_id' => $tenant->id,
                  'album_name' => $request->album_title,
                  'album_description' => $request->album_description
                ]);
              }
            }

           //galleries
           if($request->hasFile('files_gallery')){
               $path = TenantGallery::getPathGallery( $tenant->id );
               if(!File::exists($path)){
                  File::makeDirectory( $path, $mode = 0755, true, true );
               }         
               $i=0;
               $datas = [];
               foreach ($request->file('files_gallery') as $key => $file) {
                  $filename = time() .$i.'.' . $file->getClientOriginalExtension();
                  $file->move( $path, $filename );
                  $i++;
                  $datas[] = [
                    'ekraf_tenant_id'=> $tenant->id,
                    'img_gallery'  => $filename,
                    'album_id' => $album->id
                  ];
               }
               DB::table('tenant_galleries')->insert( $datas );
           }

           //image client
            if($request->hasFile('img_logo_client')){
              $clients = TenantClient::whereEkrafTenantId( $tenant->id );
              if($clients->count() > 0){
                $clients->delete();
                foreach ($clients->get() as $key => $client) {
                  $client->deleteImg();
                }
              }
              $path = TenantClient::getPathImgClient( $tenant->id );
              if(!File::exists($path)){
                File::makeDirectory( $path, $mode = 0755, true, true );
              }
              $i = 0;
              $datas = [];
              foreach ( $request->file('img_logo_client') as $key1 => $file ) {
                $filename = time() .$i.'.' . $file->getClientOriginalExtension();
                $file->move($path, $filename);
                $datas[] = [
                'ekraf_tenant_id'   => $tenant->id,
                'name'              => $request->client_name[$key1],
                'img_icon'          => $filename
                ];
                $i++;
              }
              DB::table('tenant_clients')->insert( $datas );
            }

           //subsectors
           if($request->has('selected_tenants')){
            $subSectors = [];
             foreach ($request->selected_tenants as $key => $value) {
                $subSectors[] = [
                  'ekraf_tenant_id' => $tenant->id,
                  'category_id'  => $value
                ];
             }

             //SubSector
             DB::table('tenant_categories')->insert( $subSectors );
           }

           //Product
           if($request->has('tenant_product_category') || $request->has('tenant_product_type') || $request->has('tenant_product_character')){
              $product = $tenant->product()->create([
                'ekraf_tenant_id' => $tenant->id,
                'tenant_product_category' => $request->tenant_product_category, 
                'tenant_product_type' =>  $request->tenant_product_type,
                'tenant_product_character'  => $request->tenant_product_character,
                'tenant_product_quantity'   => $request->tenant_product_quantity,
                'tenant_product_unit'     => $request->tenant_product_unit
              ]);
           }

           //Tools
           $product->tools()->create([
            'tenant_product_id' => $product->id,
            'tool_name'=> $request->tool_name,
            'tool_brand_name'=> $request->tool_brand_name,
            'tool_quantity'  => $request->tool_quantity,
            'tool_capacity'  => $request->tool_capacity,
            'tool_price'   => $request->tool_price,
            'tool_is_copyright' => $request->is_copyright
          ]);



           //update store
           $tenant->store()->create([
              'ekraf_tenant_id' =>$tenant->id,
              'marketing_by_online'=>$request->marketing_type,
              'store_open_at'=>$request->store_open_at,
              'store_close_at'=>$request->store_close_at,
              'is_facility_mushola'=>$request->is_facility_mushola,
              'is_facility_toilet'=>$request->is_facility_toilet,
              'is_facility_parking'=>$request->is_facility_parking,
              'is_facility_showroom'=>$request->is_facility_showroom,
              'store_surface_area' =>$request->store_surface_area
           ]);

          

           if( $request->has('marketing_type') ){
               if($request->marketing_is_online){
                        $data_social_sites = [];
                        foreach ($request->social_site_type as $key => $social_site_type) {
                            $data_social_sites[] = [
                                'ekraf_tenant_id' => $tenant->id,
                                'social_site_type' => $social_site_type,
                                'social_site_url'  => ($request->has('social_site_url')) ? $request->social_site_url[$key]  : null,
                            ];
                        }

                    $tenant->social_sites()->createMany( $data_social_sites );
                }
            }
                
            if($request->has('employee_male_total') || $request->has('employee_female_total')){
                $tenant->employee()->create([
                    'ekraf_tenant_id' => $tenant->id,
                    'employee_male_total'   => $request->employee_male_total,
                    'employee_female_total' => $request->employee_female_total,
                    'employee_general_fee'  => $request->employee_general_fee,
                    'employee_elementary_school_total' => $request->employee_elementary_school_total,
                    'employee_junior_high_total' => $request->employee_junior_high_total,
                    'employee_high_total' => $request->employee_high_total,
                    'employee_d1_total'   => $request->employee_d1_total,
                    'employee_d3_total'   => $request->employee_d3_total,
                    'employee_bacheloor_degree_total' => $request->employee_bacheloor_degree_total,
                    'employee_post_graduate_total' => $request->employee_post_graduate_total,
                    'employee_doctoral_degree_total' => $request->employee_doctoral_degree_total
                ]);
            }

            //financing (monetary)
            if($request->has('asset')){
                $tenant->monetary()->create([
                    'ekraf_tenant_id' => $tenant->id,
                    'monetary_asset'  => $request->monetary_asset,
                    'monetary_daily_omset' => $request->monetary_daily_omset,
                    'monetary_monthly_omset' => $request->monetary_monthly_omset,
                    'monetary_yearly_omset'  => $request->monetary_yearly_omset,
                    'monetary_daily_profit'  => $request->monetary_daily_profit,
                    'monetary_monthly_profit'=> $request->monetary_monthly_profit,
                    'monetary_yearly_profit' => $request->yearly_profit,
                    'monetary_access_financing' => $request->monetary_access_financing,
                ]);
            }

            /*if( !empty($request->coaching_type) ){
                $data_coaches = [];
                foreach ($request->coaching as $key => $choaching) {
                    $data_coaches[] = [
                        'ekraf_tenant_id'   => $tenant->id,
                        'coaching'          => $choaching,
                        'coaching_type'     => ($request->has('coaching_type')) ? $request->coaching_type[$key] : null,
                        'coaching_year'     => ($request->has('coaching_year')) ? $request->coaching_year[$key] : null
                    ]; 
                }

                //save many
                $tenant->coaches()->createMany( $data_coaches );
            }

            if( !empty($request->membership_name) ){
                $data_memberships = [];
                foreach ($request->membership_name as $key => $name) {
                    $data_memberships[] = [
                        'ekraf_tenant_id'   => $tenant->id,
                        'membership_name'   => $name
                    ];
                }

                //save many
                $tenant->memberships()->createMany( $data_memberships );
            }

            if( !empty($request->business_strength) ){
                $data_business_strengths = [];
                foreach ($request->business_strength as $key => $business_strength) {
                    $data_business_strengths[] = [
                        'ekraf_tenant_id'   => $tenant->id,
                        'bussiness_strength_title'  => $business_strength
                    ];
                }

                //save many
                $tenant->business_strengths()->createMany( $data_business_strengths );
            }
            dd($request->business_opportunities);
            if( !empty($request->business_opportunities) ){
                $data_business_opportunities = [];
                foreach ($request->business_opportunities as $key => $business_opportunity) {
                    $data_business_opportunities[] = [
                        'ekraf_tenant_id'   => $tenant->id,
                        'bussiness_opportunity_title'   => $business_opportunity
                    ];
                }

                //save many
                $tenant->business_opportunities()->createMany( $business_opportunity );
            }

            if( !empty($request->itinerary_title) ){
                $data_itineraries = [];
                foreach ($request->itinerary_title as $key => $itinerary_title) {
                    $data_itineraries[] = [
                        'ekraf_tenant_id'   => $tenant->id,
                        'itineraries_title' => $itinerary_title,
                        'itineraries_description' => ($request->has('itineraries_description')) ? $request->itineraries_description[$key] : null
                    ];
                }
                
                //save many
                $tenant->itineraries()->createMany( $data_itineraries );
            }*/
            
           if( $tenant->save() ){
               $request->session()->flash('success','Success create Tenant');
               DB::commit();
               return redirect()->back();
           } 
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->withInput()->withErrors( $e->getMessage() );
        }
    }

    public function update( $id ,TenantRequest $request){
        DB::beginTransaction();
        try {
            
            $tenant = Tenant::findOrFail( $id );

            if( $request->hasFile( 'img_logo' ) ){
                $tenant->deleteImg();
                $filename = time() . '.' . $request->file( 'img_logo' )->getClientOriginalExtension();
                $request->file( 'img_logo' )->move( Tenant::getPath(), $filename);
            }else{
              $filename = $tenant->img_logo;
            }
            $tenant->update( [
                'tenant_name' => $request->name,
                'founding_year' => Carbon::parse($request->founding_year)->format('Y-m-d'),
                'about' => $request->about,
                'info'  => $request->info,
                'img_logo' => $filename,
                'tenant_intro' => $request->tenant_intro,
                'tenant_mission' => $request->tenant_mission
            ] );

           $tenant->address()->update([
              'ekraf_tenant_id' => $tenant->id,
              'street'          => $request->street,
              'subdistrict_id'  => $request->subdistrict,
              'postal_code'     => $request->postal_code,
              'latitude'        => $request->latitude,
              'longitude'       => $request->longitude 
           ]);

           //Tenant Founder
           $tenant->founders()->update([
                'ekraf_tenant_id' => $tenant->id,
                'founder_name'    => $request->founder_name,
                'founder_last_education' => $request->founder_last_education,
                'founder_last_education_department'   => $request->founder_last_education_department,
                'gender'     => $request->gender
           ]);

           //Tenant Legality
           $tenant->legality_docs()->update([
              'ekraf_tenant_id' => $tenant->id,
              'legality_doc_name' => $request->legality_doc_name,
              'tenant_legality_img' => null
           ]);

           $store = [
              'ekraf_tenant_id' =>$tenant->id,
              'marketing_is_online'=>$request->marketing_type,
              'store_open_at'=>$request->store_open_at,
              'store_close_at'=>$request->store_close_at,
              'is_facility_mushola'=>$request->is_facility_mushola,
              'is_facility_toilet'=>$request->is_facility_toilet,
              'is_facility_parking'=>$request->is_facility_parking,
              'is_facility_showroom'=>$request->is_facility_showroom,
              'store_surface_area' =>$request->store_surface_area
           ];

           //Tenant Store
           if($tenant->store()->count() != 0 ) {
              $tenant->store()->update($store);
           }else{
              $tenant->store()->insert($store);
           }

           $itinerary = [
            'ekraf_tenant_id' => $tenant->id,
            'itineraries_title' => $request->itineraries_title,
            'itineraries_description' => $request->itineraries_description
           ];

          $social_sites = [
            'ekraf_tenant_id'=>$tenant->id,
             'social_site_type'=>$request->social_site_type,
             'social_site_url' => $request->social_site_url
           ];

           if($tenant->social_sites){
              $tenant->social_sites()->update($social_sites);
           }else{
              $tenant->social_sites()->insert($social_sites);
           }

           //galleries
           if($request->hasFile('files_gallery')){
              $path = TenantGallery::getPathGallery( $tenant->id );
               if(!File::exists($path)){
                  File::makeDirectory( $path, $mode = 0755, true, true );
               }         
               $i=0;
               $datas = [];
               foreach ($request->file('files_gallery') as $key => $file) {
                  $filename = time() .$i.'.' . $file->getClientOriginalExtension();
                  $file->move( $path, $filename );
                  $i++;
                  $datas[] = [
                    'ekraf_tenant_id'=> $tenant->id,
                    'title' => 'gallery',
                    'img_gallery'    => $filename
                  ];
               }
               DB::table('tenant_galleries')->insert( $datas );
           }

           //image client
           if($request->hasFile('img_logo_client')){
            $path = TenantClient::getPathImgClient( $tenant->id );
             if(!File::exists($path)){
               File::makeDirectory( $path, $mode = 0755, true, true );
             }
                $i = 0;
                $datas = [];
                foreach ( $request->file('img_logo_client') as $key1 => $file ) {
                  $filename = time() .$i.'.' . $file->getClientOriginalExtension();
                  $file->move($path, $filename);
                  $datas[] = [
                    'ekraf_tenant_id' => $tenant->id,
                    'name'     => $request->name_client[$key1],
                    'img_icon' => $filename
                  ];
                  $i++;
                }
              DB::table('tenant_clients')->insert( $datas );
           }

           //subsectors
           if($request->has('selected_tenants')){
            DB::table('tenant_categories')->where('ekraf_tenant_id','=',$tenant->id)->delete();
            $subSectors = [];
             foreach ($request->selected_tenants as $key => $value) {
                $subSectors[] = [
                  'ekraf_tenant_id' => $tenant->id,
                  'category_id'  => $value
                ];
             }
             //SubSector
             DB::table('tenant_categories')->insert( $subSectors );
           }

           //itineraries
           if( $request->has('itinerary_title') ){
            $data_itineraries = [];
            foreach ($request->itinerary_title as $key => $itinerary_title) {
              $data_itineraries[] = [
                'ekraf_tenant_id' => $tenant->id,
                'itineraries_title' => $itinerary_title,
                'itineraries_description' => ($request->has('itineraries_description')) ? $request->itineraries_description[$key] : null
              ];
            }

            //save many
            $tenant->itineraries()->createMany( $data_itineraries );
          }

            $request->session()->flash('success','Success Update Tenant');
            DB::commit();
            return redirect()->back();
        } catch (Exception $e) {
            DB::rollback();
            flash()->error('failed',$e->getMessage());
            return back()->withInput();
        }
    }

    public function destroy( $id ){
        try {
            $tenant  = Tenant::findOrFail( $id );
            $tenant->delete();
            Session::flash('success','Deleted Tenant'. $tenant->name);
            //$request->session()->flash('success','Success delete tenant');
            return redirect()->back();
        } catch (Exception $e) {
            //$request->session()->flash('failed',$e->getMessage());
        }
    }

    //data tables for galleries tenant
    public function get_data_images_gallery($id=''){
      $images = TenantGallery::where('ekraf_tenant_id','=', $id)->get();
        return datatables($images)
        ->editColumn('img_gallery', function($images) use ($id){
            return Html::image( $images->getImgGallery($id), $images->name, ['width'=>'30'] );
        })
        ->addColumn('action', function($image) use ($id) {
            $delete_url = route('admin.gallery.delete', $image->id);
            return 
            view('admin.forms.action-buttons', [
                'delete_url' => $delete_url
            ]);
        })->make(true);
    }

    //data tables
    public function get_data_clients($id=''){
      $clients = TenantClient::where('ekraf_tenant_id','=', $id)->get();
        return datatables($clients)
        ->editColumn('img_icon', function($clients) use ($id){
            return Html::image( $clients->getImgClient($id), $clients->name, ['width'=>'30'] );
        })
        ->addColumn('action', function($client) use ($id) {
            $delete_url = route('admin.gallery.delete', $client->id);
            return 
            view('admin.forms.action-buttons', [
                'delete_url' => $delete_url
            ]);
        })->make(true); 
    }

    //This method for delete image gallery
    public function destroyImageGallery( $id ){
      try {
        $image = TenantGallery::findOrFail($id);
        $image->delete();
        $image->deleteImgGallery( $image->ekraf_tenant_id );
        return redirect()->back();
      } catch (Exception $e) {
        Session::flash('failed', $e->getMessage());
      }
    }
}
