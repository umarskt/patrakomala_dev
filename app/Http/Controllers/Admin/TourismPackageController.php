<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TourPackages\TourismPackage;
use App\Models\TourPackages\TourismBelt;
use App\Models\Tenants\Tenant;
use App\Http\Requests\TourPackageRequest;
use DB;
use Session;

class TourismPackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.packages.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tenants = Tenant::select('id','tenant_name')->pluck('tenant_name','id');
        $belts = TourismBelt::select('id','belt_title')->get()->pluck('belt_title','id');
        return view('admin.packages.create', compact('belts','tenants'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TourPackageRequest $request)
    {
        DB::beginTransaction();
        try {
            $package = TourismPackage::create([
                'package_name'=>$request->package_name,
                'package_description'=>$request->package_description,
                'estimated_cost'=>$request->estimated_cost,
                'provider'=>$request->provider
            ]);

            if( $request->has('selected_belt') ){
                DB::table('belt_packages')->insert(['tourism_package_id'=>$package->id,'tourism_belt_id'=>$request->selected_belt]);
            }

            if($request->has('selected_tenants')){
                $datas = [];
                $i= 1;
                foreach ($request->selected_tenants as $key => $tenant) {
                    $datas[] = [
                        'ekraf_tenant_id' => $tenant,
                        'tourism_package_id'=> $package->id,
                        'package_thread' => $i
                    ];
                    $i++;
                }
                
                $tenant = DB::table('tenant_tourism_package')->insert($datas);
            }

            if( $package->save() ){
                $request->session()->flash('success','Success Create Package');
                DB::commit();
                return redirect()->back();
            }
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->withInput()->withErrors( $e->getMessage() );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id )
    {
        DB::beginTransaction();
        try {
            $package = TourismPackage::findOrFail( $id );

            if( $package->delete() ){
               Session::flash('success','Success deleted Tourism Package');
               DB::commit();
               return redirect()->back();
            }
        } catch (Exception $e) {
            DB::rollback();
        }
    }

    public function get_data(){
        $packages = TourismPackage::all();
        return datatables($packages)
        ->addColumn('action', function($package) {
            $url = '';
            $delete_url = route('admin.packages.delete', $package->id);
            return view('admin.forms.action-buttons',[
                'edit_url' => $url
            ]).
            view('admin.forms.action-buttons', [
                'delete_url' => $delete_url
            ]);
        })->make(true);
    }
}
