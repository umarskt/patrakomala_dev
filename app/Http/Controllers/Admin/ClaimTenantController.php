<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tenants\TenantClaim;
use Html;

class ClaimTenantController extends Controller
{
	//list claimed tenant
    public function getDataList(){
    	$claims = TenantClaim::getClaimTenant();

    	return datatables($claims)
        ->addColumn('action', function($claim) {
            $show_url = route('admin.tenant.show', $claim->id_tenant);
            return view('admin.forms.action-buttons',[
                'show_url' => $show_url
            ]);
        })->make(true);
    }
}
