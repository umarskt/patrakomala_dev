<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;

use App\Models\User;
use Validator;
use Sentinel;

class AuthencticationController extends Controller
{
    public function index(){
        return view('admin.auth.index');
    }
    public function store(Request $request){
        $validation = Validator::make( $request->all(), [
            'inp_email' => 'required|email',
            'password' => 'required'
        ] );

        if( $validation->fails() ){
            $request->session()->flash('failed_login','Failed');
            return redirect()->back()->withInput()->withErrors( $validation->messages() );
        }else{
            try {
                if(!User::login($request)){
                    $request->session()->flash('failed_login','Wrong email or password');
                    return redirect()->back()->withErrors( $validation->messages() );
                }
                $request->session()->flash('success');
                //redirect to dashboard
                return redirect()->route('admin.dahsboard');
            } catch (Exception $e) {
                return redirect()->back()->withInput()->withErrors( $e->getMessage() );
            }
        }
    }

    function destroy(){
        Sentinel::logout();
        return redirect()->route('admin.login');
    }
}
