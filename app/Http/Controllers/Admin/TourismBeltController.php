<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TourPackages\TourismBelt;
use App\Models\Tenants\Tenant;

use Validator;
use Session;
use DB;


class TourismBeltController extends Controller
{
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.belts.index');
    }

    public function store(Request $request){
        try {
            $validator = Validator::make( $request->all(),[
                'belt_title' => 'required',
                'belt_description' => 'required'
            ] );

            if( $validator->fails() ){
                return redirect()->back()->withErrors( $validator->messages() );
            }

            $TourismBelt = TourismBelt::create( $request->all() );
            
            if($request->has('selected_tenants')){
                $datas = [];

                foreach ($request->selected_tenants as $key => $tenant) {
                    $datas[] = [
                        'ekraf_tenant_id' => $tenant,
                        'tourism_belt_id'=> $TourismBelt->id
                    ];
                }
                
                $tenant = DB::table('tenant_belts')->insert($datas);
            }

            if( $TourismBelt->save() ){
                $request->session()->flash('success','Success Create Belt');
                return redirect()->back();
            }

        } catch (Exception $e) {
            return redirect()->back()->withInput()->withErrors( $e->getMessage() );
        }
    }

    public function update($id, Request $request){
        DB::beginTransaction();
        try {
             $validator = Validator::make( $request->all(),[
                'belt_title' => 'required',
                'belt_description' => 'required'
            ] );

            if( $validator->fails() ){
                return redirect()->back()->withErrors( $validator->messages() );
            }
            if($request->has('selected_tenants')){
                 $datas = [];

                foreach ($request->selected_tenants as $key => $tenant) {
                    $datas[] = [
                        'ekraf_tenant_id' => $tenant,
                        'tourism_belt_id'=> $id
                    ];
                }
                //problem 2 query, solve next time
                DB::table('tenant_belts')->where('tourism_belt_id','=',$id)->delete();
                $tenant = DB::table('tenant_belts')->insert($datas);
            }
            $TourismBelt = TourismBelt::findOrFail( $id );
            if( $TourismBelt->update( $request->all() ) ){
                $request->session()->flash('success','Success Update Tenant');
            }
            DB::commit();
            return redirect()->back();
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->withInput()->withErrors( $e->getMessage() );
        }
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tenants = Tenant::select('id','tenant_name')->pluck('tenant_name','id');
        return view('admin.belts.create', compact('tenants'));
    }

    public function edit( $id ){
        $belt  = TourismBelt::findOrFail($id);
        $tenants = Tenant::select('id','tenant_name')->pluck('tenant_name','id');
        $data_tenant = DB::table('tenant_belts')->select('ekraf_tenant_id')->where('tourism_belt_id','=',$belt->id)->pluck('ekraf_tenant_id');
        return view('admin.belts.edit',compact('belt','tenants','data_tenant'));
    }

    public function get_data(){
        $belts = TourismBelt::all();
        return datatables($belts)
        ->addColumn('action', function($belt) {
            $url = route('admin.belts.edit', $belt->id);
            $delete_url = route('admin.belts.delete', $belt->id);
            
            return view('admin.forms.action-buttons',[
                'edit_url' => $url
            ]).
            view('admin.forms.action-buttons', [
                'delete_url' => $delete_url
            ]);
        })->make(true);
    }

    public function destroy( $id ){
        DB::beginTransaction();
        try {
            $package = TourismBelt::findOrFail( $id );

            if( $package->delete() ){
               Session::flash('success','Success deleted Belt');
               DB::commit();
               return redirect()->back();
            }
        } catch (Exception $e) {
            DB::rollback();
        }
    }
    
}
