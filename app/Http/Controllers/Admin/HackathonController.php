<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Hackathon\TeamHackathon;
use App\Http\Controllers\Controller;

class HackathonController extends Controller
{
    function index(){
		return view('admin.hackathons.index');
    }

    function get_data(){
    	$hackathons = TeamHackathon::select('id','team_name','contact_email','contact_phone','created_at')->get();
    	return datatables( $hackathons )
    	->addColumn('action', function( $hackathon ){
    		$show_url = route('admin.hackathons.detail', $hackathon->id);
    		 return view('admin.forms.action-buttons',[
                'show_url' => $show_url
            ]);
    	})->make( true );
    }

    function detail($id){
    	$hackathon = TeamHackathon::find( $id )->getData();
    	return view('admin.hackathons.detail', compact('hackathon'));
    }
}
