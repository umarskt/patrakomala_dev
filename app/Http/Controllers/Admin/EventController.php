<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Events\Event;
use App\Models\Tag;
use App\Models\Events\TagEvent;
use App\Models\Events\EventImage;
use App\Models\Events\EventUrl;

use App\Http\Requests\EventRequest;
use DB;
use Carbon\Carbon;
use Session;
use Html;

class EventController extends Controller
{
    public function index(){
    	return view('admin.events.index');
    }

    public function create(){
    	$tags = Tag::select('id','name')->pluck('name','id');
    	return view('admin.events.create', compact('tags'));
    }

    public function store(EventRequest $request){
        DB::beginTransaction();
    	try {
            $event = Event::create( [
    			'title'=> $request->title,
    			'description' => $request->description,
    			'take_place'  => $request->take_place,
                'start_date'  => Carbon::parse($request->start_date)->format('Y-m-d'),
                'end_date'    => Carbon::parse($request->end_date)->format('Y-m-d'),
    			'start_time'  => Carbon::parse($request->start_time)->format('H:i:s'),
                'end_time'    => Carbon::parse($request->end_time)->format('H:i:s'),
                'event_content_type' => $request->event_content_type,
                'status' => 'published'
    		] );

    		if($request->has('tags')){
                $event->save_tags($request->tags);
            }

            if( $request->has('files') ){
                $datas = [];
                
                foreach ($request->file('files') as $key => $file) {
                    $filename = time().'.'.$file->getClientOriginalExtension();
                    $file->move( EventImage::getPath(), $filename );
                    $datas[] = [
                        'event_id' => $event->id,
                        'img_event'=>$filename,
                        'img_name' =>$file->getClientOriginalName()
                    ];
                }
                EventImage::insert( $datas );
            }

            if( $request->has('urls') ){
                $datas = [];
                foreach ($request->urls as $key => $url) {
                    $datas[] = [
                        'event_id' => $event->id,
                        'url'      => $url
                    ];
                }

                EventUrl::insert( $datas );

            }

    		if( $event->save() ){
    		   $request->session()->flash('success','Success create Event');
               DB::commit();
               return redirect()->back();
    		}

    	} catch (Exception $e) {
            DB::rollback();
    	}
    }

    function edit($id){
        $tags = Tag::select('id','name')->pluck('name','id');
        $data = Event::find($id)->getDataEdit();
        return view('admin.events.edit', compact('data','tags'));
    }

    function update(EventRequest $request){
        DB::beginTransaction();
        try {
            $event = Event::findOrFail( $request->id );
            $event->title = $request->title;
            $event->description = $request->description;
            $event->take_place  = $request->take_place;
            $event->start_date  = Carbon::parse($request->start_date)->format('Y-m-d');
            $event->end_date    = Carbon::parse($request->end_date)->format('Y-m-d');
            $event->start_time  = Carbon::parse($request->start_time)->format('H:i:s');
            $event->end_time  = Carbon::parse($request->end_time)->format('H:i:s');
            $event->event_content_type = $request->event_content_type;

            if($request->has('tags')){
                TagEvent::whereEventId( $request->id )->delete();
                $event->save_tags( $request->tags );
            }

            if( $request->has('files') ){
                $datas = [];
                foreach ($request->file('files') as $key => $file) {
                    $filename = time().'.'.$file->getClientOriginalExtension();
                    $file->move( EventImage::getPath(), $filename );
                    $datas[] = [
                        'event_id' => $event->id,
                        'img_event'=>$filename,
                        'img_name' =>$file->getClientOriginalName()
                    ];
                }
                EventImage::insert( $datas );
            }

             if( $request->has('urls') && !empty($request->urls) ){
                $datas = [];
                foreach ($request->urls as $key => $url) {
                    $datas[] = [
                        'event_id' => $event->id,
                        'url'      => $url
                    ];
                }

                EventUrl::insert( $datas );

            }

            if( $event->save() ){
                DB::commit();
                $request->session()->flash('success','Success update Event');
                return redirect()->back();
            }
        } catch (Exception $e) {
            DB::rollback();
        }
    }

    function destroy( $id ){
    	DB::beginTransaction();
    	try {
    		$event = Event::findOrFail( $id );
    		if( $event->delete() ){
    		   Session::flash('success','Success deleted Event');
               DB::commit();
               return redirect()->back();
    		}
    	} catch (Exception $e) {
    		DB::rollback();
    	}
    }

    function get_data(){
    	$events = Event::all();
        return datatables($events)
        ->addColumn('action', function($event) {
            $url = route('admin.event.edit', $event->id);
            $delete_url = route('admin.event.delete',$event->id);
            $show = route('admin.event.show', $event->id);
            return view('admin.forms.action-buttons',[
                'edit_url' => $url
            ]).
            view('admin.forms.action-buttons', [
                'delete_url' => $delete_url
            ]).
            view('admin.forms.action-buttons', [
                'show_url' => $show
            ]);
        })->make(true);
    }

    function getDataImages( $id ){
        $images = Event::find( $id )->images;
        return datatables($images)->editColumn('images',function($images){
            return Html::image( $images->getImg(), null, ['width'=>'30'] );
        })->addColumn('action', function($image) {
            $delete_url = route('admin.event.images.delete', $image->id);
            return 
            view('admin.forms.action-buttons', [
                'delete_url' => $delete_url
            ]);
        })->make(true);
    }

    function getDataUrls( $id ){
        $urls = Event::find( $id )->urls;
        return datatables($urls)->editColumn('url',function($url){
            return $url->url;
        })->addColumn('action', function($url) {
            $delete_url = route('admin.event.url.delete', $url->id);
            return view('admin.forms.action-buttons', [
                'delete_url' => $delete_url
            ]);
        })->make(true);
    }

    function show( $id ){
       $event =  Event::findOrFail($id);
       $images = [];
       foreach ($event->images as $key => $img) {
           $images[] = [
                'path'=>$img->getImg()
           ];
       }

       $list = [
            'date' => Carbon::parse($event->date)->format('D, d M Y'),
            'event_name' => $event->title,
            'place' => $event->take_place,
            'desc'  => $event->description,
            'tags'  => $event->getTags()->toArray(),
            'images'=> $images
       ];
       return view('admin.events.show',compact('list'));
    }

    function deleteImg($id){
        $img = EventImage::findOrFail( $id );
        $img->delete();
        $img->deleteImage();
        Session::flash('success','Success deleted Image');
        return redirect()->back();
    }

    function deleteUrl($id){
        $url = EventUrl::find($id);
        $url->delete();
        Session::flash('success','Success deleted Url');
        return redirect()->back();
    }
}
