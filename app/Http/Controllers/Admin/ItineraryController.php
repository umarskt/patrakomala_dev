<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TourPackages\Tourism;
use App\Models\Tenants\TenantItinerary;
use Validator;

class ItineraryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.itineraries.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.itineraries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validation =  Validator::make( $request->all(), [
                'itineraries_title' => 'required',
                'itineraries_description' => 'required'
            ] );

            if( $validation->fails() ){
                return redirect()->back()->withInput()->withErrors( $validation->messages() );
            }

            TenantItinerary::create( $request->all() );
            $request->session()->flash('success','Success Create Itinerary');
        } catch (Exception $e) {
            $request->session()->flash('failed', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            
        } catch (Exception $e) {
            
        }
    }

    public function get_data(){
        $itineraries = TenantItinerary::all();
        return datatables($itineraries)
        ->addColumn('action', function($itinerary) {
            $url = '';
            $delete_url = '';
            $show_url = route('admin.tenant.show', $itinerary->id);
            return view('admin.forms.action-buttons',[
                'edit_url' => $url
            ]).
            view('admin.forms.action-buttons', [
                'delete_url' => $delete_url
            ]).view('admin.forms.action-buttons',[
                'show_url' => $show_url
            ]);
        })->make(true);
    }
}
