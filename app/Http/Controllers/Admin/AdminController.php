<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tenants\Tenant;
use App\Models\Analytics\PageView;
use Html;

class AdminController extends Controller
{
    public function index(){
        $analytic = PageView::getAnalytic();
        return view('admin.main.index', compact('analytic'));
    }

    public function getData(){
    	$tenants = Tenant::select('id','app_login_id','tenant_name','img_logo','tenant_email','tenant_phone')
    				->whereNull('app_login_id')
    				->get();
    	return datatables($tenants)
        ->editColumn('img_logo', function($tenants){
            return Html::image( $tenants->getImg(), $tenants->name, ['width'=>'30'] );
        })
        ->addColumn('action', function($tenant) {
            $url = route('admin.tenant.edit', $tenant->id);
            $delete_url = route('admin.tenant.delete', $tenant->id);
            $show_url = route('admin.tenant.show', $tenant->id);
            return view('admin.forms.action-buttons',[
                'show_url' => $show_url
            ]);
        })->make(true);
    }

    public function analyzeChart(Request $request){
        if( strtolower($request->type) == 'tenant-chart' ){
            return response()->json(['status'=>true]);
        }
    }
}
