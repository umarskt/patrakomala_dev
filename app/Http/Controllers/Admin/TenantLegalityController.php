<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tenants\TenantLegality;
use Html;

class TenantLegalityController extends Controller
{
    function get_data_legality($id=''){
    	$docs = TenantLegality::where('ekraf_tenant_id','=', $id)->get();
        return datatables($docs)
        ->editColumn('tenant_legality_img', function($docs) use ($id){
            return Html::image( $docs->getLegality($id), $docs->legality_doc_name, ['width'=>'30'] );
        })
        ->addColumn('action', function($docs) use ($id) {
            $delete_url = route('admin.gallery.delete', $docs->id);
            return 
            view('admin.forms.action-buttons', [
                'delete_url' => $delete_url
            ]);
        })->make(true); 
    }
}
