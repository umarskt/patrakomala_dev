<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;

use App\Models\User;

class AuthController extends Controller
{
    public function store(LoginRequest $request){
        try {
            if(User::login($request)){
                $request->session()->flash('failed');
                return redirect()->back()->withErrors( $validation->messages() );
            }
            $request->session()->flash('success');
            //redirect to dashboard
        } catch (Exception $e) {
            return redirect()->back()->withInput()->withErrors( $e->getMessage() );
        }
    }
}
