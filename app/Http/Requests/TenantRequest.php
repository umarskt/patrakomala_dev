<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TenantRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( strtolower($this->method()) ) {
            case 'post':
                return[
                    'name' => 'required|max:50',
                    'founding_year' => 'required',
                    'tenant_email' =>'required|unique:ekraf_tenants',
                    'tenant_phone' => 'required|numeric',
                    /*'tenant_npwp'  => 'required',
                    'founding_year' => 'required',
                    'tenant_corporation' => 'required',*/

                    //address
                    'street'=>'required',
                    'subdistrict' => 'required',
                    'postal_code' => 'required',

                    //Tenant founder
                    /*'founder_name'=>'required|max:33',
                    'founder_last_education'=>'required',
                    'founder_last_education_department'=>'required',
                    'gender'=>'required',*/

                    //Legality document
                    /*'legality_doc_name'=>'required',
                    'tenant_legality_img'=>'required',*/

                    //Product
                    /*'tenant_production_type'=>'required',
                    'tenant_product_name' => 'required',
                    'tenant_product_character'=>'required',*/

                    //material


                    //product tool


                    //marketing
                    /*'social_site_type' => 'required',
                    'social_site_url' => 'required',*/

                    //operational

                ];
                break;
            case 'put':
                return[
                    'name' => 'required|max:50',
                    'founding_year' => 'required',
                    'postal_code' => 'required'
                ];
                break;
            default:
                # code...
                break;
        }
    }

    public function formatErrors(Validator $validator){
        flash()->error( "error" );
        $check =  $validator->messages()->toArray();
    }
}
