<?php

namespace App\Http\Requests\Publics;

use Illuminate\Foundation\Http\FormRequest;

class PublicTenantRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( strtolower( $this->method() ) ) {
            case 'get':
                return [
                    'tenant_id'=>'numeric'
                ];
                break;
            
            default:
                # code...
                break;
        }
    }
}
