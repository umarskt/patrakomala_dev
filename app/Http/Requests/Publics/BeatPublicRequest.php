<?php

namespace App\Http\Requests\Publics;

use Illuminate\Foundation\Http\FormRequest;

class BeatPublicRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( strtolower( $this->method() ) ) {
            case 'post':
                return [
                    'belt' => 'array',
                    'belt.*' => 'numeric',
                    'package' => 'array',
                    'package.*' => 'numeric',
                    'subsector' => 'array',
                    'subsector.*' => 'numeric',
                    'keyword' => 'max:40' 
                ];
                break;
            
            case 'get':
                # code...
                break;
        }
    }
}
