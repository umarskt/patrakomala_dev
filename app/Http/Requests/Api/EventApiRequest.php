<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class EventApiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( strtolower( $this->method() ) ) {
            case 'post':
                return [
                   'title'  => 'required',
                   'place'  => 'required',
                   'start_date' => 'required',
                   'end_date' => 'required',
                   'tags.*' => 'required|max:20|min:1',
                   'token'  => 'required'
                ];
                break;

            case 'put':
                return [
                   'title'  => 'required',
                   'place'  => 'required',
                   'start_date' => 'required',
                   'end_date' => 'required',
                   'token'  => 'required'
                ];
                break;
            
            default:
                return [
                    
                ];  
                break;
        }
    }

    public function messages(){
        return [
            'tags.*' => 'Value parameters tags may not be greater than 5 characters.'
        ];
    }
}
