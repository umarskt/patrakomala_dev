<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TourPackageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'package_name'=>'required|max:40',
            'package_description'=>'required',
            'estimated_cost'=>'required|numeric',
            'provider'=>'required',
            'selected_tenants'=>'required'
        ];
    }

    public function formatErrors(Validator $validator){
        flash()->error( "error" );
        $check =  $validator->messages()->toArray();
    }
}
