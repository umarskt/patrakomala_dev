<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( strtolower($this->method()) ) {
            case 'post':
                return[
                    'full_name'=>'required|min:5|max:33',
                    'email' => 'required|email|unique:users|min:5|max:40',
                    'username' => 'required',
                    'password' => 'required|min:8',
                    'confirm_password' => 'required|same:password'
                ];
                break;
            case 'put':
                return[
                  'full_name' => 'required',
                  'email'     => 'required|email',
                  'username'  => 'required',
                  'password'  => 'min:8'  
                ];
                break;
            default:
                # code...
                break;
        }
    }

    public function formatErrors(Validator $validator){
        flash()->error( "error" );
        $check =  $validator->messages()->toArray();
    }
}
