<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\Request;

class RegisterHackathonRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */


    public function authorize()
    {
        return true;
    }

    //re mapping input
    public function sanitize(){
        
        $input['data'] = json_decode($this->data, true);
        $input['proposal'] = $this->file;
        $this->replace( $input );
        return $this->all();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->sanitize();
        return [
           /* 'data'                => 'required',
            'data.team_name'      => 'required',
            'data.description'    => 'required',
            'data.contact_email'  => 'required',
            'data.contact_phone'  => 'required',
            'data.members'        => 'required'*/
        ];
    }
}
