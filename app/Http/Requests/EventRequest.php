<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( strtolower($this->method()) ) {
            case 'post':
                return[
                    'title'=>'required|max:30',
                    'description'=>'required',
                    'files'=>'required|max:2048'
                ];
                break;

            case 'put':
                return[
                    'title'=>'required|max:30',
                    'description' => 'required'
                ];
                break;    

            default:
                # code...
                break;
        }
    }

    public function formatErrors(Validator $validator){
        flash()->error( "error" );
        $check =  $validator->messages()->toArray();
    }
}
