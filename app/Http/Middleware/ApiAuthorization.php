<?php

namespace App\Http\Middleware;

use Closure;

class ApiAuthorization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $apiKey = $request->header('api-key');
        if(!check_api_key($apiKey)){
            return response()->json([
                'status'    => false,
                'message'   => 'Unauthorized access'
            ], 401);
        }

        return $next($request);
    }
}
