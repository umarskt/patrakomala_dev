<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Client;
use App\Models\Hackathon\TeamHackathon;

class AccessAPIHackathon
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $key = $request->header('access-key');
        $check = TeamHackathon::CheckAuthKey($key);
        if( !$check ){
            return response()->json([
                'status'    => 401,
                'message'   => 'UnAuthorizedAccess'
            ], 401);
        }
        return $next($request);
    }
}
