<?php

namespace App\Http\Middleware;

use Closure;
use Log;

class APIMiddleware
{
    protected $start;
    protected $end;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->start = microtime(true);
        return $next($request);
    }

    public function terminate($request, $response)
    {
        $this->end = microtime(true);

        $this->log($request);
    }

    protected function log($request)
    {
        $duration = $this->end - $this->start;
        $url = $request->fullUrl();
        $method = $request->getMethod();
        $ip = $request->getClientIp();
        $input = $request->all();


        $log = "{$ip}: {$method}@{$url} - {$duration}ms";

        Log::info(array('log'=>$log,'input'=>$request->all()));
    }
}
