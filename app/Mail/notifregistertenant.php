<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class notifregistertenant extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $data_tenant;
    public function __construct($data_tenant='')
    {
        $this->data_tenant = $data_tenant;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Request approval Tenant '.$this->data_tenant->tenant_name)->markdown('emails.tenants.notifregistertenant')->with(['data_tenant'=>$this->data_tenant]);
    }
}
