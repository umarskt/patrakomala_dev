<?php

if(! function_exists('hasError') ){
    function hasError($errors,$name=''){
        if($errors->has($name)){
            return "has-error";
        }
    }
}

if (! function_exists('user_info')) {
    /**
     * Get logged user info.
     *
     * @param  string $column
     * @return mixed
     */
    function user_info($column = null)
    {
        if ($user = Sentinel::check()) {
            if (is_null($column)) {
                return $user;
            }

            if ('full_name' == $column) {
                return user_info('full_name');
            }

            if ('role' == $column) {
                return user_info()->roles[0];
            }

            if ('wholesaler' == $column) {
                return $user->wholesaler()->first()->id;
            }

            if ('wholesalerTypeId' == $column) {
                return $user->wholesaler()->first()->wholesaler_type_id;
            }

            return $user->{$column};
        }

        return null;
    }
}

if (! function_exists('listCountry')) {

    function listCountry()
    {   
        $data_list = ["Afghanistan", "Aland Islands", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Barbuda", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Trty.", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Caicos Islands", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "French Southern Territories", "Futuna Islands", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard", "Herzegovina", "Holy See", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Jan Mayen Islands", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea", "Korea (Democratic)", "Kuwait", "Kyrgyzstan", "Lao", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macao", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "McDonald Islands", "Mexico", "Micronesia", "Miquelon", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "Nevis", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Palestinian Territory, Occupied", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Principe", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Barthelemy", "Saint Helena", "Saint Kitts", "Saint Lucia", "Saint Martin (French part)", "Saint Pierre", "Saint Vincent", "Samoa", "San Marino", "Sao Tome", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia", "South Sandwich Islands", "Spain", "Sri Lanka", "Sudan", "Suriname", "Svalbard", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "The Grenadines", "Timor-Leste", "Tobago", "Togo", "Tokelau", "Tonga", "Trinidad", "Tunisia", "Turkey", "Turkmenistan", "Turks Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "Uruguay", "US Minor Outlying Islands", "Uzbekistan", "Vanuatu", "Vatican City State", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (US)", "Wallis", "Western Sahara", "Yemen", "Zambia", "Zimbabwe"];

        $data = [];
        foreach ($data_list as $row) {
            $data[$row] = $row;
        }

        return $data;
    }
}

if(! function_exists('tenant_list_npwp')){
    function tenant_list_npwp(){
        $list = [
            null,"Not have","Exists","Individual","Institution"
        ];

        $data = [];
        foreach ($list as $key => $row) {
            $data[$row] = $row;
        }

        return $data;
    }
}

if(! function_exists('tenant_institution')){
    function tenant_institiution(){
        $list = [
            null,
            "PT",
            "CV",
            "PD"
        ];

        $data = [];
        foreach ($list as $key => $row) {
            $data[$row] = $row;
        }

        return $data;
    }
}

if(! function_exists('tenant_founder_education')){
    function tenant_founder_education(){
        $list = [
            "SD","SMP","SMA","S1","S2"
        ];

        $data = [];
        foreach ($list as $key => $row) {
            $data[$row] = $row;
        }

        return $data;
    }
}

if(!function_exists( 'paging' )){
    function paging($skip='',$take=''){
        return ($skip - 1) * $take;
    }
}

//This function for checking view is empty or not
if(! function_exists('check_view')){
    function check_view($str=''){
        if( empty($str) ){
            return '-'; 
        }else{
            return $str;
        }
    }
}

if(! function_exists( 'gender' ) ){
    function gender($str=''){
        if( $str == 'm' ){
            return "Pria";
        }else if( $str == 'f' ){
            return "Wanita";
        }
    }
}

if(! function_exists( 'check_api_key' )){
    function check_api_key($input_key=''){
        $key = config('api_key.key');
        $salt = config('api_key.salt');
        $hash = crypt($key, $salt);

        if (crypt($input_key, $hash) == $hash) {
            return true;
        }
        else {
            return false;
        }
    }
}

if(! function_exists( 'generateChar' ) ){
    function generateChar($length = 30){
        $rand = substr( sha1( microtime() ), 1, $length);
        return $rand;
    }
}