<?php

namespace App\Models;

use Cartalyst\Sentinel\Users\EloquentUser as Model;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Cartalyst\Sentinel\Users\EloquentUser as SentinelModel;

use Sentinel;
use JWTAuth;

class User extends SentinelModel implements Authenticatable
{
    use AuthenticableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','name', 'email', 'username', 'password', 'permission','last_login', 'full_name','created_at', 'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function login( $request ){
        return Sentinel::authenticate( [
            'email' => $request->inp_email,
            'password' => $request->password
        ] );
    }

    public static function checkApiLogin( $token ){
        $user = JWTAuth::toUser( $token );
        $user = Sentinel::findById( $user->id );
        if( $user->inRole('tenant') || $user->inRole(' user-job ') ){
            return true;
        }else{
            return false;
        }
    }
}
