<?php

namespace App\Models\Tenants;

use Illuminate\Database\Eloquent\Model;
use App\Models\Tenants\TenantGallery;

class TenantAlbum extends Model
{
    protected $table = 'albums';
    protected $fillable = [
    	'id','ekraf_tenant_id','album_name', 'album_description','created_at','updated_at'
    ];

    protected static function boot(){
    	parent::boot();

    	Self::deleting(function($model){
    		TenantGallery::whereAlbumId($model->id)->delete();
    	});
    }

    function photos(){
    	return $this->hasMany( TenantGallery::class, 'album_id', 'id' );
    }

    function get_galleries(){
        $datas = [];
        foreach ($this->photos as $key => $gallery) {
            $datas[] = $gallery->getImageGalleries();
        }
        return $datas;
    }
}
