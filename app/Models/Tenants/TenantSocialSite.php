<?php

namespace App\Models\Tenants;

use Illuminate\Database\Eloquent\Model;

class TenantSocialSite extends Model
{
    protected $table = 'tenant_social_sites';
    protected $fillable = [
    	'id','ekraf_tenant_id','social_site_type','social_site_url','created_at','updated_at'
    ];
}
