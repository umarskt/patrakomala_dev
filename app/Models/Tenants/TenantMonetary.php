<?php

namespace App\Models\Tenants;

use Illuminate\Database\Eloquent\Model;

class TenantMonetary extends Model
{
    protected $table = 'tenant_monetaries';
    protected $fillable = [
    	'id','ekraf_tenant_id','monetary_asset','monetary_daily_omset','monetary_monthly_omset','monetary_yearly_omset','monetary_daily_profit','monetary_monthly_profit','monetary_yearly_profit','monetary_access_financing','monetary_investment','created_at','updated_at'
    ];

    function getDataArray(){
    	return[
    		'monetary_asset'=> $this->monetary_asset,
    		'monetary_daily_omset' => $this->monetary_daily_omset,
    		'monetary_monthly_omset' => $this->monetary_monthly_omset,
    		'monetary_yearly_omset' => $this->monetary_yearly_omset,
    		'monetary_daily_profit' => $this->monetary_daily_profit,
    		'monetary_monthly_profit' => $this->monetary_monthly_profit,
    		'monetary_yearly_profit' => $this->monetary_yearly_profit,
    		'monetary_access_financing' => $this->monetary_access_financing,
    		'monetary_investment' => $this->monetary_investment
    	];
    }
}
