<?php

namespace App\Models\Tenants;

use Illuminate\Database\Eloquent\Model;

class TenantMembership extends Model
{
    protected $table = 'tenant_community_memberships';
    protected $fillable = [
    	'id','ekraf_tenant_id','membership_name','created_at','updated_at'
    ];
}
