<?php

namespace App\Models\Tenants;

use Illuminate\Database\Eloquent\Model;

class TenantService extends Model
{
    protected $table = 'tenant_services';
    protected $fillable = [
    	'id','ekraf_tenant_id','title','description','created_at','updated_at'
    ];
}
