<?php

namespace App\Models\Tenants;

use Illuminate\Database\Eloquent\Model;

class TenantEmployee extends Model
{
    protected $table = 'tenant_employees';
    protected $fillable = ['id', 'ekraf_tenant_id','employee_male_total','employee_female_total','employee_general_fee','employee_elementary_school_total',
    						'employee_junior_high_total','employee_high_total','employee_d1_total','employee_d2_total','employee_d3_total',
    						'employee_bacheloor_degree_total','employee_post_graduate_total','employee_doctoral_degree_total','created_at',
    						'updated_at'
						  ];

	function getDataArray(){
		return[
			'employee_male_total' => $this->employee_male_total,
			'employee_female_total' => $this->employee_female_total,
			'employee_general_fee' => $this->employee_general_fee,
			'employee_elementary_school_total' => $this->employee_elementary_school_total,
			'employee_junior_high_total' => $this->employee_junior_high_total,
			'employee_high_total' => $this->employee_high_total,
			'employee_d1_total' => $this->employee_d1_total,
			'employee_d2_total' => $this->employee_d2_total,
			'employee_d3_total' => $this->employee_d3_total,
			'employee_bacheloor_degree_total' => $this->employee_bacheloor_degree_total,
			'employee_post_graduate_total' => $this->employee_post_graduate_total,
			'employee_doctoral_degree_total' => $this->employee_doctoral_degree_total
		];
	}
}
