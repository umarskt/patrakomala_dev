<?php

namespace App\Models\Tenants;

use Illuminate\Database\Eloquent\Model;

class TenantCatalog extends Model
{
    protected $table = 'tenant_portofolio_product_catalog';
    
    protected $fillable = [
    	'id','ekraf_tenant_id','img_product','created_at','updated_at'
    ];
}
