<?php

namespace App\Models\Tenants;

use Illuminate\Database\Eloquent\Model;

class TenantBusinessStrength extends Model
{
    protected $table = 'tenant_business_strengths';
    protected $fillable = [
    	'id','ekraf_tenant_id','bussiness_strength_title','created_at','updated_at'
    ];
}
