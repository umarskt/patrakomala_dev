<?php

namespace App\Models\Tenants;

use Illuminate\Database\Eloquent\Model;

class TenantCategory extends Model
{
    protected $table = 'tenant_categories';
    protected $fillable = [
    	'id','ekraf_tenant_id','category_id'
    ];

    function category(){
    	return $this->hasOne(Category::class,'id','category_id');
    }
}
