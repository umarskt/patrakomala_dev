<?php

namespace App\Models\Tenants;

use Illuminate\Database\Eloquent\Model;
use File;
use DB;
use App\Models\Tenants\TenantGallery;

class Tenant extends Model
{
    protected $table = 'ekraf_tenants';

    protected $fillable = [
    	'id','tenant_name','app_login_id','founding_year','tenant_email', 'tenant_corporation','tenant_phone','about','additional_info','about','is_active', 'img_logo','created_at','updated_at','tenant_logo_background','company_type', 'company_hki','created_at','updated_at','status','app_login_claim'
    ];

    protected static function boot(){
        parent::boot();

        //deleted child table
        self::deleting(function($model){
            $tenant = (new Tenant)->find($model->id);
            DB::table('tenant_address')->where('ekraf_tenant_id','=',$model->id)->delete();
            DB::table('tenant_itineraries')->where('ekraf_tenant_id','=',$model->id)->delete();
            DB::table('tenant_social_sites')->where('ekraf_tenant_id','=',$model->id)->delete();
            DB::table('tenant_founders')->where('ekraf_tenant_id','=',$model->id)->delete();
            DB::table('tenant_categories')->where('ekraf_tenant_id','=',$model->id)->delete();
            DB::table('tenant_legality_docs')->where('ekraf_tenant_id','=',$model->id)->delete();
            DB::table('tenant_monetaries')->where('ekraf_tenant_id','=',$model->id)->delete();
            DB::table('app_logins')->where('id','=', $model->app_login_id)->delete();

            $gallery = TenantGallery::where('ekraf_tenant_id','=', $model->id)->first();
            if($gallery){
                 $gallery->delete();
                 $gallery->deleteImgGallery($model->id);
            }
            DB::table('tenant_stores')->where('ekraf_tenant_id','=',$model->id)->delete();
        });
    }

    function address(){
        return $this->hasOne(TenantAddress::class, 'ekraf_tenant_id','id');
    }

    function address_map(){
        return $this->hasOne(TenantAddress::class,'ekraf_tenant_id','id')->select('latitude','longitude');
    }

    function itinerary(){
        return $this->hasOne( TenantItinerary::class,'ekraf_tenant_id','id' );
    }

    function itineraries(){
        return $this->hasMany( TenantItinerary::class,'ekraf_tenant_id','id' );
    }

    function social_sites(){
        return $this->hasMany( TenantSocialSite::class,'ekraf_tenant_id','id' )->select(['social_site_type','social_site_url']);
    }

    function founders(){
        return $this->hasMany( TenantFounder::class, 'ekraf_tenant_id', 'id');
    }

    //single founder
    function founder(){
        return $this->hasOne( TenantFounder::class, 'ekraf_tenant_id', 'id' );
    }

    function sub_sectors(){
        return $this->hasMany( TenantCategory::class, 'ekraf_tenant_id', 'id' );
    }

    function legality_docs(){
        return $this->hasMany( TenantLegality::class, 'ekraf_tenant_id', 'id' );
    }

    function product(){
        return $this->hasOne( TenantProduct::class, 'ekraf_tenant_id', 'id' );
    }

    function store(){
        return $this->hasOne( TenantStore::class, 'ekraf_tenant_id', 'id' );
    }

    function galleries(){
        return $this->hasMany( TenantGallery::class, 'ekraf_tenant_id', 'id' );
    }

    function clients(){
        return $this->hasMany( TenantClient::class, 'ekraf_tenant_id','id');
    }

    function employee(){
        return $this->hasOne( TenantEmployee::class, 'ekraf_tenant_id', 'id' );
    }

    function monetary(){
        return $this->hasOne( TenantMonetary::class, 'ekraf_tenant_id', 'id' );
    }

    function claimed(){
        return $this->hasOne( TenantClaim::class, 'ekraf_tenant_id', 'id' );
    }

    function coaches(){
        return $this->hasMany( TenantCoaching::class, 'ekraf_tenant_id', 'id' );
    }

    function memberships(){
        return $this->hasMany( TenantMembership::class, 'ekraf_tenant_id', 'id' );
    }

    function business_strengths(){
        return $this->hasMany( TenantBusinessStrength::class, 'ekraf_tenant_id', 'id' );
    }

    function business_opportunities(){
        return $this->hasMany( TenantBusinessOpportunity::class, 'ekraf_tenant_id', 'id' );
    }

    function services(){
        return $this->hasMany( TenantService::class, 'ekraf_tenant_id', 'id');
    }

    function albums(){
        return $this->hasMany( TenantAlbum::class, 'ekraf_tenant_id', 'id' );
    }

    function portofolios(){
        return $this->hasMany( TenantPortofolio::class, 'ekraf_tenant_id', 'id' );
    }

    //has One
    function package(){
      return  DB::table('ekraf_tenants')
       ->join('tenant_tourism_package', function( $join ){
            $join->on('ekraf_tenants.id','=','tenant_tourism_package.ekraf_tenant_id');
       })
       ->join('tourism_package', function( $join ){
            $join->on('tenant_tourism_package.tourism_package_id','=','tourism_package.id');
       })->where('ekraf_tenants.id','=',$this->id)
         ->select('tourism_package.id','tourism_package.package_name')
         ->first();
    }

    public static function filterTourismPackage($params=''){
     
        $collect = Self::join('tenant_categories', function( $join ) use ($params){
            $filter = $join->on('ekraf_tenants.id','=','tenant_categories.ekraf_tenant_id');
             if( !empty( $params['subsector'] ) ){
                $filter->whereIn('tenant_categories.category_id',$params['subsector']);
            }
        })
        ->join('tenant_address', function( $join ) {
            $join->on('ekraf_tenants.id','=','tenant_address.ekraf_tenant_id');
        });
        
        if( !empty($params['belt']) ){
            
            $collect->join('tenant_belts', function( $join ) use ( $params ){
                $filter = $join->on('ekraf_tenants.id','=','tenant_belts.ekraf_tenant_id');
                if(!empty($params['belt'])){
                    $filter->whereIn('tenant_belts.tourism_belt_id', $params['belt']);
                }
            })
            ->join('tourism_belt', function( $join ) use ( $params ) {
                $join->on('tenant_belts.tourism_belt_id','=','tourism_belt.id');
            });
        }

        if(!empty($params['package'])){
            $collect->join('tenant_tourism_package', function( $join ) use ( $params ){
                $filter = $join->on('ekraf_tenants.id','=','tenant_tourism_package.ekraf_tenant_id');
                if(!empty($params['package'])){
                    $filter->whereIn('tenant_tourism_package.tourism_package_id', $params['package']);
                }
            })
            ->join('tourism_package', function( $join ) use ( $params ) {
                $join->on('tenant_tourism_package.tourism_package_id','=','tourism_package.id');
            });
        }

        $collect->select('ekraf_tenants.id', 'ekraf_tenants.tenant_name','ekraf_tenants.img_logo','tenant_address.latitude','tenant_address.longitude');
        
        if( !empty($params['keyword']) ){
            $collect = $collect->where('ekraf_tenants.tenant_name','like','%'.$params['keyword'].'%');
        }
        return $collect->get();
    }
    
    static function getPath($file=null){
        return public_path('uploads/tenant/logo/'.$file);
    }


    function deleteImg(){
    	if( !empty( $this->img_logo ) ){
    		$path = $this->getPath();
            if( File::exists( $path . $this->img_logo ) ){
                File::delete( $path .'/'. $this->img_logo);
    		}
    	}
    }

    static function getBasedCategory($page='', $params=''){
        
       $collect = Self::join('tenant_categories', function( $join ){
            $join->on('ekraf_tenants.id','=', 'tenant_categories.ekraf_tenant_id');

        })
        ->join('categories', function( $join ) use ( $params ){
            $filter = $join->on('tenant_categories.category_id','=','categories.id');
            if( !empty( $params['subsector'] ) ){
                $filter->whereIn('categories.id',$params['subsector']);
            }
        })
        ->join('tenant_address', function( $join ){
            $join->on('ekraf_tenants.id','=','tenant_address.ekraf_tenant_id');
        })
        ->join('sub_districts', function( $join ) use ( $params ){
            $filter = $join->on('tenant_address.subdistrict_id','=','sub_districts.id');
            if( !empty($params['subdistrict']) ){
                $filter->whereIn('sub_districts.id',$params['subdistrict']);
            }
        })
        ->distinct()
        ->select('ekraf_tenants.id','ekraf_tenants.tenant_name','ekraf_tenants.img_logo','categories.category_name as tenant_subSector','tenant_address.latitude','tenant_address.longitude');

        if( !empty($params['keyword']) ){
            $collect = $collect->where('ekraf_tenants.tenant_name','like','%'.$params['keyword'].'%');
        }

        return $collect->paginate(9);
        
    }

    function getImg(){
       $img = url('admin/dist/img/no_image_available.png');
        
        if(!empty( $this->img_logo )){
            if( File::exists( Self::getPath( $this->img_logo ) ) ){
                $img = Self::getUrl( $this->img_logo );
            }
        }
        return $img;
    }

    static function getUrl($file=null){
        return url('uploads/tenant/logo/'.$file);
    }

    function ListDataApi(){
        return[
            "id"=>$this->id,
            "tenant_name" => $this->tenant_name,
            "sub_sector_name" => $this->tenant_subSector,
            "tenant_logo" => $this->getImg(),
            "latitude" => $this->latitude,
            "longitude" => $this->longitude
        ];
    }

    function getDataApi(){
        return[
            "id"            => $this->id,
            "tenant_name"   => $this->tenant_name,
            "address"       => $this->address->street,
            "operasional"   => ($this->store) ? $this->store->getDataOperasional() : null,
            "access"        => !empty($this->itinerary->itineraries_description) ? $this->itinerary->itineraries_description : null ,
            "capacity"      => ($this->store->store_surface_area) ? (String)$this->store->store_surface_area." m2" : null,
            "facilities"    => ($this->store) ? $this->store->getDataFacilities() : null,
            "contact"       =>  $this->tenant_phone,
            "galleries"     =>  (isset($this->galleries)) ? $this->getGallery() : null,
            "package_name"  => !(empty($this->package()->package_name)) ? $this->package()->package_name : null
        ];
    }

    //For data to front end
    function getDataApiProfile(){
        return[
            "id"    => $this->id,
            "tenant_name" => $this->tenant_name,
            "tenant_logo" => $this->getImg(),
            "tenant_profile" => $this->about,
            "tenant_services"=> $this->getService(),
            "tenant_color_background" => $this->tenant_color_background,
            "tenant_email" => $this->tenant_email,
            "tenant_phone" => $this->tenant_phone,
            "status"  => (isset($this->claimed)) ? $this->claimed->status : null,
            "tenant_address" => $this->address->street,
            "latitude" => $this->address->latitude,
            "longitude"  => $this->address->longitude,
            "sub_sectors" => $this->getDetailSubSector(),
            "clients" => $this->get_clients(),
            "albums" => $this->getAlbums(),
            "social_sites" => $this->social_sites,
            "portofolios"  => $this->getPortofolios()
        ];
    }

    function getDataDetail(){
        return[
            "id"    => $this->id,
            "app_login_id" => $this->app_login_id,
            "tenant_name" => check_view( $this->tenant_name ),
            "tenant_logo" => $this->getImg(),
            "founding_year" => check_view($this->founding_year),
            "about"         => check_view($this->about),
            "tenant_services"=> $this->getService(),
            "tenant_color_background" => $this->tenant_color_background,
            "address"       => $this->address->street,
            "latitude" => $this->address->latitude,
            "longitude"  => $this->address->longitude,
            "postal_code" => check_view( $this->address->postal_code ),
            "subdistrict" => check_view( $this->address->subdistrict ),
            "sub_sectors" => $this->getDetailSubSector(),
            "founder_name" => check_view( $this->founder->founder_name ),
            "founder_email" => check_view( $this->founder->founder_email ),
            "founder_phone" => check_view( $this->founder->founder_phone ),
            "founder_last_education" => check_view( $this->founder->founder_last_education ),
            "founder_last_education_department" => check_view( $this->founder->founder_last_education_department ),
            "founder_gender" => gender( $this->founder->gender ),
            "founder_npwp" => check_view( $this->founder->founder_npwp ),
            "legalities"   => $this->getLegalities(),
            "company_type" => check_view( $this->company_type ),
            "company_hki"  => check_view( $this->company_hki ),
            "clients" => $this->get_clients(),
            "albums" => $this->getAlbums(),
            "social_sites" => $this->social_sites,
            "portofolios"  => $this->getPortofolios()
        ];
    }

    function getDetailSubSector(){
        $datas = [];
        foreach ($this->sub_sectors as $key => $sub_sector) {
            $datas[] = $sub_sector->category->getDataApi();
        }
        return $datas;
    }

    function getAlbums(){
        $datas = [];
        foreach ($this->albums as $key => $album) {
            $datas[] = [
                'id' => $album->id,
                'album_name' => $album->album_name,
                'album_description' => $album->album_description,
                'galleries' => $album->get_galleries()
            ];
        }
        return $datas;
    }

    function get_clients(){
        $datas = [];
        foreach (   $this->clients as $key => $client   ) {
            $datas[] = $client->getDataApi();
        }
        return $datas;
    }

    function getPortofolios(){
        $datas = [];
        foreach ($this->portofolios as $key => $portofolio) {
            $datas[] = [
                'name'    => $portofolio->name,
                'catalog' => $portofolio->getPortofolio()
            ];
        }
        return $datas;
    }

    function getService(){
        $datas = [];
        foreach ($this->services as $key => $service) {
            $datas[] = [
                'id' => $service->id,
                'title' => $service->title,
                'description' => $service->description
            ];
        }

        return $datas;
    }

    function getLegalities(){
        $datas = [];

        foreach ($this->legality_docs as $key => $legality) {
            $datas[] = [
                'legality_doc_name' => $legality->legality_doc_name,
                'tenant_legality_img' => $legality->getLegality($legality->id)
            ];
        }

        return $datas;
    }

    function getDataEdit(){
      return [
            'stores'  => ($this->store) ? $this->store->getDataArray() : null,
            'founders'=> ($this->founder) ? $this->founder->getDataArray() : null,
            'sub_sectors' => ($this->sub_sectors) ? $this->sub_sectors->pluck('category_id')->toArray() : null,
            'products' => ($this->product) ? $this->product->getDataArray() : null,
            'tools'    => ($this->product) ? $this->product->tool->getDataArray() : null,
            'stores'   => ($this->store)  ? $this->store->getDataArray() : null,
            'employees' => ($this->employee) ? $this->employee->getDataArray() : null,
            'monetaries' => ($this->monetary) ? $this->monetary->getDataArray() : null
      ];
    }

    function getGallery(){
        $datas = [];
        foreach ($this->galleries as $key => $gallery) {
            $datas[] = $gallery->first()->getImageGalleries(); 
        }
        return $datas;
    }



}
