<?php

namespace App\Models\Tenants;

use Illuminate\Database\Eloquent\Model;

class TenantItinerary extends Model
{
    protected $table = 'tenant_itineraries';
    protected $fillable = [
    	'id','ekraf_tenant_id','itineraries_title','itineraries_description'
    ];
}
