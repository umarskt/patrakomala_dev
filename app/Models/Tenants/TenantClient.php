<?php

namespace App\Models\Tenants;

use Illuminate\Database\Eloquent\Model;
use File;

class TenantClient extends Model
{
    protected $table = 'tenant_clients';
    protected $fillable = [
    	'id','ekraf_tenant_id','name','img_icon','created_at','updated_at'
    ];

    static function getPathImgClient( $id=null, $file=null ){
    	return public_path('storage/app/public/tenant/clients/logo/'.$id.'/'.$file);
    }

    static function getUrlClient( $id=null ,$file=null){
    	return url('storage/app/public/tenant/clients/logo/'.$id.'/'.$file);
    }

    function getImgClient($id=null){
    	$img = url('admin/dist/img/no_image_available.png');
        
        if(!empty( $this->img_icon )){
            if( File::exists( Self::getPathImgClient( $this->ekraf_tenant_id ,$this->img_icon ) ) ){
                $img = Self::getUrlClient( $this->ekraf_tenant_id ,$this->img_icon );
            }
        }
        return $img;
    }

    function deleteImg(){
        if( !empty( $this->img_icon ) ){
            $path = $this->getPathImgClient($this->ekraf_tenant_id, $this->img_icon);
            if( File::exists( $path ) ){
                File::delete( $path );
            }
        }
    }

    function getDataApi(){
        return[
            "id"=>$this->id,
            "name_client"=>$this->name,
            "img_icon" => $this->getUrlClient($this->ekraf_tenant_id, $this->img_icon)
        ];
    }
}
