<?php

namespace App\Models\Tenants;

use Illuminate\Database\Eloquent\Model;
use File;

class TenantGallery extends Model
{
    protected $table = 'tenant_galleries';
    protected $fillable = [
    	'id','ekraf_tenant_id','img_gallery', 'album_id','created_at','updated_at'
    ];


    static function getPathGallery($id=null, $file=null){
    	return public_path('storage/app/public/tenant/galleries/'.$id.'/'.$file);
    }

    
    function deleteImgGallery( $id=null ){
    	if( !empty( $this->img_gallery ) ){
    		$path = Self::getPathGallery($id, $this->img_gallery);
            if( File::exists( $path ) ){
                File::delete( $path );
    		} 
    	}
    }

    static function getUrlGallery( $id=null ,$file=null){
    	return url('storage/app/public/tenant/galleries/'.$id.'/'.$file);
    }

    function getImgGallery($id=null){
    	$img = url('admin/dist/img/no_image_available.png');
        if(!empty( $this->img_gallery )){
            if( File::exists( Self::getPathGallery( $this->ekraf_tenant_id ,$this->img_gallery ) ) ){
                $img = Self::getUrlGallery( $this->ekraf_tenant_id ,$this->img_gallery );
            }
        }
        return $img;
    }

    function getImageGalleries(){
        return[
            'img_gallery' => $this->getImgGallery()
        ];
    }
}
