<?php

namespace App\Models\Tenants;

use Illuminate\Database\Eloquent\Model;
use App\Models\Tenants;

class TenantClaim extends Model
{
    protected $table = 'tenant_claims';
    protected $fillable = [
    	'id','ekraf_tenant_id','app_login_id','status','created_at','updated_at'
    ];

    //function for get tenant claimed
    static function getClaimTenant(){
        return Self::join('ekraf_tenants', function( $join ){
            $join->on('tenant_claims.ekraf_tenant_id','=','ekraf_tenants.id');
        })
        ->join('app_logins', function( $join ){
        	$join->on('tenant_claims.app_login_id','=','app_logins.id');
        })
        ->where('tenant_claims.status','=','claimed')
        ->select('ekraf_tenants.id as id_tenant', 'ekraf_tenants.tenant_name', 'ekraf_tenants.img_logo','app_logins.id as id_user','app_logins.app_email','app_logins.app_username')
        ->get();
    }

    
}
