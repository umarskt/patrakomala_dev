<?php

namespace App\Models\Tenants;

use Illuminate\Database\Eloquent\Model;

class TenantCoaching extends Model
{
    protected $table = 'tenant_coaching_events';
    protected $fillable = [
    	'id','ekraf_tenant_id','coaching','coaching_type','coaching_year','created_at','updated_at'
    ];
}
