<?php

namespace App\Models\Tenants;

use Illuminate\Database\Eloquent\Model;

class TenantTourismPackage extends Model
{
    protected $table = 'tenant_tourism_package';
    protected $fillable = [
    	'id','ekraf_tenant_id','tourism_package_id'
    ];

    public function tenant(){
    	return $this->hasOne(Tenant::class,'id','ekraf_tenant_id');
    }
}
