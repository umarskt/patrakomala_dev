<?php

namespace App\Models\Tenants;

use Illuminate\Database\Eloquent\Model;
use File;

class TenantLegality extends Model
{
    protected $table = 'tenant_legality_docs';
    protected $fillable = [
    	'id','ekraf_tenant_id','legality_doc_name','legality_doc_slug', 'tenant_legality_img','created_at','updated_at'
    ];

    static function getPathLegality($id=null, $file=null){
    	return public_path('storage/app/public/tenant/legalities/'.$id.'/'.$file);
    }

    static function getUrlLegality( $id=null ,$file=null){
    	return url('storage/app/public/tenant/legalities/'.$id.'/'.$file);
    }

    function getLegality($id=null){
    	$img = url('admin/dist/img/no_image_available.png');
        
        if(!empty( $this->tenant_legality_img )){
            if( File::exists( Self::getPathLegality( $id ,$this->tenant_legality_img ) ) ){
                $img = Self::getUrlLegality( $id ,$this->tenant_legality_img );
            }
        }
        return $img;
    }
}
