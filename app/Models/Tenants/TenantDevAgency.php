<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TenantDevAgency extends Model
{
    protected $table = 'tenant_development_agencies';
    protected $fillable = [
    	'id','ekraf_tenant_id','development_name','development_format','development_year','created_at','updated_at'
    ];
}
