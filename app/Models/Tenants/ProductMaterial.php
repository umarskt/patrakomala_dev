<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductMaterial extends Model
{
    protected $table = 'tenant_product_materials';
    protected $fillable = [
    	'id','material_name','material_yearly_quantity','material_yearly_expenses','material_provider','created_at','updated_at'
    ];
}
