<?php

namespace App\Models\Tenants;

use Illuminate\Database\Eloquent\Model;

class ProductTool extends Model
{
    protected $table = 'tenant_product_tools';
    protected $fillable = [
    	'id','tenant_product_id', 'tool_name','tool_price','tool_brand_name','tool_is_copyright','tool_capacity','tool_quantity','created_at','updated_at'
    ];

    function getDataArray(){
    	return[
    		'tool_name' => $this->tool_name,
    		'tool_price' => $this->tool_price,
    		'tool_brand_name' => $this->tool_brand_name,
    		'tool_is_copyright' => $this->tool_is_copyright,
    		'tool_capacity' => $this->tool_capacity,
    		'tool_quantity' => $this->tool_quantity
    	];
    }
}
