<?php

namespace App\Models\Tenants;

use Illuminate\Database\Eloquent\Model;
use App\Models\SubDistrict;

class TenantAddress extends Model
{
    protected $table = 'tenant_address';
    protected $fillable = [
    	'id','ekraf_tenant_id','street', 'subdistrict_id','postal_code','latitude','longitude', 'rtandrw','created_at','updated_at'
    ];

    function sub_district(){
    	return $this->hasOne( SubDistrict::class, 'id', 'subdistrict_id' );
    }
}
