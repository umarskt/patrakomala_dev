<?php

namespace App\Models\Tenants;

use Illuminate\Database\Eloquent\Model;
use File;

class TenantPortofolio extends Model
{
    protected $table = 'tenant_portofolio_catalogs';
    protected $fillable = [
    	'id','ekraf_tenant_id', 'name','img_portofolio','created_at','updated_at'
    ];

    static function getPathPortofolio($id= null, $file=null){
    	return public_path('storage/app/public/tenant/portofolios/'.$id.'/'.$file);
    }

    function deletePortofolio(){
    	if( !empty( $this->img_portofolio ) ){
    		$path = Self::getPathPortofolio($this->ekraf_tenant_id, $this->img_portofolio);
            if( File::exists( $path ) ){
                File::delete( $path );
    		}
    	}
    }

    static function getUrlPortofolio( $id=null ,$file=null){
    	return url('storage/app/public/tenant/portofolios/'.$id.'/'.$file);
    }

    function getPortofolio(){
    	$content = url('admin/dist/img/no_image_available.png');
        if(!empty( $this->img_portofolio )){
            if( File::exists( Self::getPathPortofolio( $this->ekraf_tenant_id ,$this->img_portofolio ) ) ){
                $content = Self::getUrlPortofolio( $this->ekraf_tenant_id ,$this->img_portofolio );
            }
        }
        return $content;
    }

}
