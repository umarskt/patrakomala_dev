<?php

namespace App\Models\Tenants;

use Illuminate\Database\Eloquent\Model;

class TenantProduct extends Model
{
    protected $table = 'tenant_products';
    protected $fillable = [
    	'id','ekraf_tenant_id','tenant_product_name','tenant_product_type','tenant_product_character', 'tenant_product_category',
        'tenant_product_quantity', 'tenant_product_unit','created_at','updated_at'
    ];

    function tools(){
    	return $this->hasMany(ProductTool::class, 'tenant_product_id', 'id');
    }

    function tool(){
        return $this->hasOne(ProductTool::class, 'tenant_product_id', 'id');
    }

    function material(){
    	return $this->hasMany(ProductMaterial::class, 'tenant_product_id', 'id');
    }

    function getDataArray(){
        return [
            'tenant_product_name' => $this->tenant_product_name,
            'tenant_product_type' => $this->tenant_product_type,
            'tenant_product_character' => $this->tenant_product_character,
            'tenant_product_category' => $this->tenant_product_category,
            'tenant_product_quantity' => $this->tenant_product_quantity,
            'tenant_product_unit' => $this->tenant_product_unit
        ];    
    }
}
