<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TenantBusinessOpportunity extends Model
{
    protected $table = 'tenant_business_opportunities';
    protected $fillable = [
    	'id','ekraf_tenant_id','bussiness_opportunity_title','created_at','updated_at'
    ];
}
