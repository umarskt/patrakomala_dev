<?php

namespace App\Models\Tenants;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class TenantStore extends Model
{
    protected $table = 'tenant_stores';
    protected $fillable = [
    	'id','ekraf_tenant_id','is_local','is_international','marketing_destination_country','marketing_percentage_ekspor','marketing_is_online','description_marketing','store_open_at','store_close_at','is_facility_parking','is_facility_toilet','is_facility_mushola',
    	'is_facility_showroom','store_surface_area','created_at','updated_at'
    ];

    function getDataOperasional(){
    	return[
    		"store_open_at"   => Carbon::parse($this->store_open_at)->format('g:i A'),
            "store_close_at"  => Carbon::parse($this->store_close_at)->format('g:i A')
    	];
    }

    function getDataFacilities(){
        $data = [];
        
        if( $this->is_facility_mushola ){
            $data[] = "Mushola";
        }
        if ( $this->is_facility_toilet ) {
            $data [] = "Toilet";
        }
        if ( $this->is_facility_parking ) {
            $data[] = "Parkir";
        }
        if ( $this->is_facility_showroom ) {
            $data[] = "Showroom";
        }
        return $data;
    }

    function getDataArray(){
        return[
            'marketing_is_online'=>$this->marketing_is_online,
            'store_open_at'   => Carbon::parse($this->store_open_at)->format('g:i A'),
            'store_close_at'  => Carbon::parse($this->store_close_at)->format('g:i A'),
            'is_facility_mushola' => $this->is_facility_mushola,
            'is_facility_toilet' => $this->is_facility_toilet,
            'is_facility_parking' => $this->is_facility_parking,
            'is_facility_showroom' => $this->is_facility_showroom,
            'store_surface_area' => $this->store_surface_area
        ];
    }
}
