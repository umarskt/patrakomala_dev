<?php

namespace App\Models\Tenants;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $fillable = [
    	'id','category_name','category_description','category_img','category_slug','created_at','updated_at'
    ];

    function getDataApi(){
        return[
            'id' => $this->id,
    		'sub_sector_name' => $this->category_name,
    		'sub_sector_description' => $this->category_description,
    		'sub_sector_img' => $this->category_img,
    		'sub_sector_slug' => $this->category_slug
    	];
    }
}
