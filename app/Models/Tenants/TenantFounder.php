<?php

namespace App\Models\Tenants;

use Illuminate\Database\Eloquent\Model;

class TenantFounder extends Model
{
    protected $table = 'tenant_founders';
    protected $fillable = [
    	'id','ekraf_tenant_id','founder_name', 'founder_email', 'founder_phone','founder_last_education','founder_last_education_department','gender', 'founder_npwp','created_at','updated_at'
    ];

    function getDataArray(){
    	return[
    		'founder_name'=>$this->founder_name,
    		'founder_email'=>$this->founder_email,
    		'founder_phone'=>$this->founder_phone,
    		'founder_last_education'=>$this->founder_last_education,
    		'founder_last_education_department'=>$this->founder_last_education_department,
    		'gender'=>$this->gender,
    		'founder_npwp'=>$this->founder_npwp
    	];
    }
}
