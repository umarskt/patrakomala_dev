<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubDistrict extends Model
{
    protected $table = 'sub_districts';
    protected $fillable = [
    	'id','sub_district_name','sub_district_slug','created_at','updated_at'
    ];

    function villages(){
        return $this->hasMany( Village::class, 'sub_district_id', 'id' )->select('id','village_name','village_name_slug','village_postal_code');
    }

    function getDataApi(){
    	return[
    		'id'=> $this->id,
    		'kecamatan'=> $this->sub_district_name,
    		'slug' => $this->sub_district_slug
    	];
    }
}
