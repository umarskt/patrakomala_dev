<?php

namespace App\Models\Hackathon;

use Illuminate\Database\Eloquent\Model;

class MemberTeamHackathon extends Model
{
    protected $connection = 'hackathon';
    protected $table = 'members';

    protected $fillable = [
    	'id','mbr_full_name','mbr_email','mbr_phone','mbr_name_contact_emergency','mbr_phone_emergency','mbr_address_emergency', 'mbr_ill','team_id',
    	'created_at','updated_at'
    ];
}
