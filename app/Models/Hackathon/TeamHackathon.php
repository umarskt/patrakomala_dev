<?php

namespace App\Models\Hackathon;

use Illuminate\Database\Eloquent\Model;

class TeamHackathon extends Model
{
    protected $connection = 'hackathon';
    protected $table = 'teams';

    protected $fillable = [
    	'id','team_name','contact_email','contact_phone', 'access_key', 'created_at','updated_at'
    ];

    function members(){
    	return $this->hasMany( MemberTeamHackathon::class,'team_id', 'id' );
    }

    static function getPathProposal($id=null, $file=null){
    	return public_path('storage/app/public/hackathon/proposals');
    }

    public static function CheckAuthKey($key=''){
        if(empty($key)) return false;
        $check = Self::where('access_key','=',$key)->count();
        if( $check > 0){
            return true;
        }else{
            return false;
        }
    }

    function deleteImgProposal( $id=null ){
    	if( !empty( $this->proposals ) ){
    		$path = Self::getPathProposal($id, $this->proposals);
            if( File::exists( $path ) ){
                File::delete( $path );
    		}
    	}
    }


    static function getUrlProposal( $id=null ,$file=null){
    	return url('storage/app/public/hackathon/proposals/'.$file);
    }

    function contentProposal($id=null){
    	$img = url('admin/dist/img/no_image_available.png');
        if(!empty( $this->proposals )){
            if( File::exists( Self::getPathProposal( $this->id ,$this->proposals ) ) ){
                $img = Self::getUrlProposal( $this->id ,$this->proposals );
            }
        }
        return $img;
    }

    function getContentProposal(){
        return[
            'proposals' => $this->contentProposal()
        ];
    }

    function getData(){
        return[
            'id'            => $this->id,
            'team_name'     => check_view( $this->team_name ),
            'contact_email' => check_view( $this->contact_email ),
            'contact_phone' => check_view( $this->contact_phone ),
            'registered_at' => check_view( $this->created_at ),
            'members'       => $this->getMembers(),
            'access_key'    => $this->access_key
        ];
    }

    function getMembers(){
        $datas = [];
        foreach ($this->members as $key => $member) {
            $datas[] = [
                'mbr_full_name' => $member->mbr_full_name,
                'mbr_email'     => $member->mbr_email,
                'mbr_phone'     => $member->mbr_phone,
                'mbr_name_contact_emergency' => $member->mbr_name_contact_emergency,
                'mbr_address_emergency' => $member->mbr_address_emergency
            ];
        }

        return $datas;
    }


}
