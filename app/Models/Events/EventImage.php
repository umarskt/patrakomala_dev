<?php

namespace App\Models\Events;

use Illuminate\Database\Eloquent\Model;
use File;
use Event;

class EventImage extends Model
{
    protected $table = 'event_images';
    protected $fillable = [
    	'event_id','img_name','img_event','created_at','updated_at'
    ];

    function getImg(){
        $img = url('admin/images/no-img.jpg');
        
        if(!empty( $this->img_event )){
            if( File::exists( Self::getPath( $this->img_event ) ) ){
                $img = Self::getUrl( $this->img_event );
            }
        }
        return $img;
    }

    static function getPath( $file=null ){
        return public_path('uploads/event_news/'.$file);
    }

    function deleteImage(){
        if( !empty( $this->img_event ) ){
            $path = Self::getPath( $this->img_event );
            if( File::exists( $path ) ){
                File::delete( $path );
            }
        }
    }

    static function getUrl($file=null){
        return url('uploads/event_news/'.$file);
    }
}
