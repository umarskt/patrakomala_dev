<?php

namespace App\Models\Events;

use Illuminate\Database\Eloquent\Model;
use App\Http\Requests\EventRequest;
use App\Models\Tag;
use DB;
use Carbon\Carbon;

class Event extends Model
{
    protected $table = 'events';

    protected $fillable = [
    	'id','title','description', 'date', 'event_content_type','take_place','start_time','end_time','start_date', 'end_date','created_at','updated_at','status','is_active','user_app_login_id','user_id'
    ];

    protected static function boot(){
    	parent::boot();
    	self::creating(function ($model){

    	});
    	//delete tag relation when event was deleted
    	self::deleting(function($model){
            DB::table('event_tagging')->where('event_id','=',$model->id)->delete();
        });

    }

    static function filterAllContent($page='', $params='', $keyword=''){
        
        $collect = Self::select('*');

        if(!empty($params['event'])){
            $collect->where(function($query) use ($params){
                $filter = $query->where('event_content_type','=','event');
                if($params['event'] == 'on-going'){
                    $filter->whereDate('start_date', Carbon::today());
                }else if ($params['event'] == 'upcoming') {
                    $filter->whereDate('start_date','>', Carbon::today());
                }else if ($params['event'] == 'past'){
                    $filter->whereDate('start_date','<', Carbon::today());
                }
            });
        }

        if(!empty($params['news'])){
           
            if($params['news'] == 'latest'){
                $collect->where('event_content_type','=','news')->orderBy('created_at','desc');
            }
        }
        if(!empty($keyword)){
            $collect->where('title','like','%'.$keyword.'%');
        }
        
        return $collect->whereStatus('published')->orderBy('created_at','desc')->skip(paging( $page, 9 ))->limit(9)->paginate();

    }

    function image(){
        return $this->hasOne(EventImage::class,'event_id','id');
    }

    function images(){
        return $this->hasMany(EventImage::class,'event_id','id');
    }

    function tag_events(){
        return $this->belongsToMany(TagEvent::class,'id','event_id');
    }

    function urls(){
        return $this->hasMany(EventUrl::class,'event_id','id');
    }

    function save_tags( $params=array() ){
    	$datas = [];
    	foreach($params as $key => $value) {
    		$datas[] = [
    			'event_id' => $this->id,
    			'tag_id'   => $params[$key]
    		];
    	}
    	
    	DB::table('event_tagging')->insert( $datas );
    }

    function saveTagBySlug( $event_id, $values = '' ){
        $tags = Tag::select('slug','id')->pluck('slug','id')->toArray();
        $collects = collect($tags);
        $new_datas = [];
        $datas = [];
        foreach ($values as $key => $value) {
            
            if( $get = $collects->search( $value ) ){
                $datas[] = [
                    'event_id' => $event_id,
                    'tag_id'   => $get
                ];
            }else{
                $id = DB::table('tags')->insertGetId([
                    'name' => $value,
                    'slug' => str_slug($value)
                ]);

                $datas[] = [
                    'event_id' => $event_id,
                    'tag_id'   => $id
                ];
            }
        }

        if(!empty($datas)){
            DB::table('event_tagging')->insert($datas);
        }

    }

    function getTags(){
        return Self::join('event_tagging', function( $join ){
            $join->on('events.id','=','event_tagging.event_id');
        })->join('tags', function( $join ){
            $join->on('tags.id','=','event_tagging.tag_id');
        })->select('tags.id','tags.name','tags.slug')
          ->where('events.id','=',$this->id)
          ->get();
    }

    function getOtherNews(){
        $get_news = Self::select('id','title','created_at')->where('event_content_type','=','news')->limit(3)->get();
        $datas = [];
        foreach ($get_news as $key => $news) {
            $datas[] = [
                "id" => $news->id,
                "title" => $news->title,
                "created_at" => Carbon::parse($news->created_at)->format("D, M Y"),
                "image" => !empty($news->image->getImg()) ? $news->image->getImg() : null,
            ];
        }

        return $datas;
    }

    function getListDataApi(){
        return[
            "id" => $this->id,
            "title" => $this->title,
            "image" => !empty($this->image) ? $this->image->getImg() : null,
            "type"  => $this->event_content_type
        ];
    }

    function getDataApiEvent(){
        return[
            "id"    => $this->id,
            "title" => $this->title,
            "images" => !empty($this->getEventImages()) ? $this->getEventImages() : null,
            "type"  => $this->event_content_type,
            "take_place" => $this->take_place,
            "description" => $this->description,
            "status" => $this->status,
            "type" => $this->event_content_type,
            "start_date" => $this->start_date,
            "end_date"  => $this->end_date,
            "start_time" => $this->start_time,
            "end_time" => $this->end_time,
            "tags" => $this->getTags(),
            "urls" => $this->getDataUrl()
        ];
    }

    function getEventImages(){
        $datas = [];
        foreach ($this->images as $key => $img) {
            $datas[] = $img->getImg();
        }

        return $datas;
    }


    function getDataApiNews(){
        return[
            "id" => $this->id,
            "title" => $this->title,
            "image" => !empty($this->getEventImages()) ? $this->getEventImages() : null,
            "description" => $this->description,
            "created_at"  => Carbon::parse($this->created_at)->format("D, M Y"),
            "other_news" => $this->getOtherNews()
        ];
    }

    function getDataEdit(){
        return[
            'id'    => ($this->id) ? $this->id : null,
            'title' => ($this->title) ? $this->title : null,
            'event_content_type' => ($this->event_content_type) ? $this->event_content_type : null,
            'description' => $this->description,
            'start_time'  => $this->start_time,
            'end_time'    => $this->end_time,
            'start_time_hours'  => $this->setTime( $this->start_time, 'h' ),
            'start_time_minute' => $this->setTime( $this->start_time,'i' ),
            'end_time_hours'    => $this->setTime( $this->end_time,'h' ),
            'end_time_minute'   => $this->setTime( $this->end_time,'i' ),
            'start_date'  => date('Y-m-d', strtotime($this->start_date)),
            'end_date'    => date('Y-m-d', strtotime($this->end_date)),
            'take_place'  => $this->take_place,
            'tags'        => $this->getTags()->pluck('id')
        ];
    }

    function getDataUrl(){
        $datas = [];
        foreach ($this->urls as $key => $url) {
            $datas[] = $url->url;
        }

        return $datas;
    }

    function setTime( $time='', $format='' ){
        return Carbon::parse( $time )->format($format);
    }

}
