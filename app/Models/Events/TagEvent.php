<?php

namespace App\Models\Events;

use Illuminate\Database\Eloquent\Model;
use App\Models\Tag;
class TagEvent extends Model
{
    protected $table = 'event_tagging';

    protected $fillable = [
    	'id','event_id','tag_id'
    ];

    function tags(){
    	return $this->hasMany(Tag::class,'id','tag_id');
    }
}
