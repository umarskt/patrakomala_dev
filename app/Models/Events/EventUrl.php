<?php

namespace App\Models\Events;

use Illuminate\Database\Eloquent\Model;

class EventUrl extends Model
{
    protected $table = 'link_url_events';
    protected $fillable = ['id','event_id','url','created_at','updated_at'];
}
