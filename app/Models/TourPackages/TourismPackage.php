<?php

namespace App\Models\TourPackages;

use Illuminate\Database\Eloquent\Model;
use App\Models\Tenants\TenantTourismPackage;
use DB;

class TourismPackage extends Model
{
    protected $table = 'tourism_package';
    protected $fillable = [
    	'id','package_name','package_description','estimated_cost','provider'
    ];

    protected static function boot(){
    	parent::boot();

    	//deleting table relation
    	self::deleting(function($model){
    		DB::table('tenant_tourism_package')->where('tourism_package_id','=',$model->id)->delete();
            DB::table('belt_packages')->where('tourism_package_id','=',$model->id)->delete();
    	});
    }

    public function tenant_package(){
        return $this->hasOne(TenantTourismPackage::class,'tourism_package_id','id_package');
    }



    public function getDataApi(){
        return[
            "id"=>$this->id,
            "package_name"=>$this->package_name
        ];
    }

    public function detailDataApi(){
        return[
            'tenant_name' => $this->tenant_name,
            'address'=>$this->tenant_package->tenant->address->street,
            'access_itinerary'=> $this->tenant_package->tenant->itinerary->itineraries_title,
            'access_itinerary_description'=> $this->tenant_package->tenant->itinerary->itineraries_description,
            'latitude' => $this->tenant_package->tenant->address->latitude,
            'longitude' => $this->tenant_package->tenant->address->longitude,
            'phone' => $this->tenant_phone,
            'social_sites'=> $this->tenant_package->tenant->social_sites->toArray(),
        ];
    }


}
