<?php

namespace App\Models\TourPackages;

use Illuminate\Database\Eloquent\Model;
use DB;

class TourismBelt extends Model
{
    protected $table = 'tourism_belt';
    protected $fillable = [
    	'id','belt_title','belt_description','created_at','updated_at'
    ];

    public function getDataApi(){
    	return [
    		"id" => $this->id,
    		"belt_title"=> $this->belt_title
    	];
    }

    protected static function boot(){
        parent::boot();

        self::deleting(function($model){
            DB::table('tenant_belts')->where('tourism_belt_id','=',$model->id)->delete();
        });
    }
}
