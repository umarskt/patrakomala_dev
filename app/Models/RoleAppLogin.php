<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoleAppLogin extends Model
{
    protected $table = 'role_app_logins';
    protected $fillable = ['id','app_login_id','role_id','created_at','updated_at'];

    function role(){
    	return $this->hasOne(Role::class, 'id','role_id');
    }
}
