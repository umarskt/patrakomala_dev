<?php

namespace App\Models;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Notifications\Notifiable;
use App\Notifications\verificationClient;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\Projects\Project;
use App\Models\Tenants\Tenant;
use App\Models\Client;
use App\Models\RoleAppLogin;
use Carbon\Carbon;
use JWTAuth;
use Sentinel;

use DB;

class AppLogin extends Authenticatable
{
	use Notifiable;
    
    protected $table = 'app_logins';

    protected $fillable = [
    	'id','app_username','app_email','app_password','is_active', 'generate_pin', 'token','created_at','updated_at'
    ];

    protected $hidden = ['app_password'];
    public $default_password = '12345678';

    public function routeNotificationForMail()
    {
        return $this->app_email;
    }
    
    //for authentication password
    public function getAuthPassword(){
        return $this->app_password;
    }


    public function tenant(){
    	return $this->hasOne( Tenant::class, 'app_login_id', 'id' );
    }

    public function client(){
        return $this->hasOne( Client::class, 'app_login_id', 'id' );
    }

    public function activation(){
        return $this->hasOne( Activation::class, 'app_login_id', 'id' );
    }

    public function type(){
        return $this->hasOne( RoleAppLogin::class, 'app_login_id','id' );
    }

    public function projects(){
        return $this->hasMany( Project::class, 'app_login_id', 'id' );
    }

    public static function checkApiLogin( $token ){
        $user = JWTAuth::toUser( $token );
        if(  $user->type->role->slug == 'client' ){
            return [
                'id'            => $user->client->id,
                'first_name'    => $user->client->client_first_name,
                'last_name'     => $user->client->client_last_name,
                'email'         => $user->app_email,
                'company_name'  => $user->client->company_name,
                'is_active'     => $user->is_active,
                'user_type'     => 'client'
            ];
        }else if( $user->type->role->slug == 'tenant' ){
            return[
                'id'            => $user->tenant->id,
                'tenant_name'   => $user->tenant->tenant_name,
                'tenant_founding_year' => $user->tenant->founding_year,
                'tenant_email' => $user->tenant->tenant_email,
                'tenant_logo'  => $user->tenant->getImg(),
                'tenant_phone' => $user->tenant->tenant_phone,
                'is_active'     => $user->is_active,
                'user_type'    => 'tenant'
            ];
        }
    }

    function registerAndActive( array $credential ){
        return $this->register( $credential, true );
    }

    function register( array $credential, $activate ){
        $login = AppLogin::create([
                'app_username'=> $credential['app_username'],
                'app_email'   => $credential['app_email'],
                'app_password' => Hash::make( $credential['app_password'] ),
                'is_active' => $activate
        ]);

        if($credential['user_type'] == 'client'){
               $role = Sentinel::findRoleBySlug($credential['user_type']); 
               $user = Client::create([
                    'app_login_id' => $login->id,
                    'client_first_name' => $credential['client_first_name'],
                    'client_last_name'  => $credential['client_last_name'],
                    'company_name'      => $credential['company_name']
                ]);

               RoleAppLogin::create([
                    'app_login_id' => $login->id,
                    'role_id'      => $role->id
               ]);

        }else if( $credential['user_type'] == 'tenant' ){
            $role = Sentinel::findRoleBySlug($credential['user_type']); 
            $tenant = Tenant::find( $credential['tenant_id'] );
            $tenant->app_login_id = $login->id;
            $tenant->save();

            RoleAppLogin::create([
                'app_login_id' => $login->id,
                'role_id'      => $role->id
            ]);
        }
            
        if($activate){
            $this->activate($login->id);
        }else{
            $activation = $this->createVerification($login->id);
        }

        return $login;
    }

    function activate($id=''){
         Activation::create([
            'code' => str_random(30).time(0),
            'completed' => true,
            'completed_at' => Carbon::now()->toDateTimeString(),
            'app_login_id' => $id
        ]);
    }

    function createVerification($id){
       return Activation::create([
            'code' => str_random(30).time(0),
            'completed' => false,
            'app_login_id' => $id
        ]);
    }

    static function get_user(){
        return Self::join('role_app_logins', function( $join ){
            $join->on('app_logins.id','=','role_app_logins.app_login_id');
        })->join('roles', function( $join ){
            $join->on('role_app_logins.role_id','=','roles.id');
        })->select('app_logins.id','app_logins.app_username', 'app_logins.app_email', 
                    'app_logins.is_active','roles.name','roles.slug')
          ->get();
    }

    static function getSubSectorTenants( $params='' ){
        return Self::join('ekraf_tenants', function( $join ){
            $join->on('app_logins.id','=','ekraf_tenants.app_login_id');
        })
        ->join('tenant_categories', function( $join ) use ($params){
            $filter  = $join->on('ekraf_tenants.id','=','tenant_categories.ekraf_tenant_id');
            if(!empty($params)){
                $filter->whereIn('tenant_categories.category_id', $params);
            }
        })->select('app_logins.app_email')->get()->toArray();
    }
}
