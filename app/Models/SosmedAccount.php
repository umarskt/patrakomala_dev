<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SosmedAccount extends Model
{
    protected $table = 'social_media_accounts';
    protected $fillable = ['id','app_login_id','social','social_id','created_at','updated_at'];

    function appLogin(){
    	return $this->hasOne(AppLogin::class, 'id', 'app_login_id');
    }
}
