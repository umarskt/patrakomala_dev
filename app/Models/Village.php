<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Village extends Model
{
    protected $table = 'urban_villages';
    protected $fillable = [
    	'id','sub_district_id','village_name','village_name_slug','village_postal_code','created_at','updated_at'
    ];
}
