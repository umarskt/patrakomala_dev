<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Activation;
use App\Models\AppLogin;
use Illuminate\Support\Facades\Hash;

use DB;
use Carbon\Carbon;

class Client extends Model
{
    protected $table = "clients";
    protected $fillable = ['id','app_login_id','client_first_name','client_last_name', 'client_telephone','created_at','updated_at','api_key'];

    public static function CheckAuthKey($key=''){
    	$check = Self::whereApiKey( $key )->first();
    	if($check){
    		return true;
    	}else{
    		return false;
    	}
    }

}
