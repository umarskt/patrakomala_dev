<?php

namespace App\Models\Projects;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'projects';
    protected $fillable = ['id','project_title','project_status','project_type','project_description','project_estimated_cost','app_login_id',
							'created_at','updated_at'];

	function project_categories(){
		return $this->hasMany( ProjectCategory::class( 'project_id', 'id' ) );
	}
}
