<?php

namespace App\Models\Projects;

use Illuminate\Database\Eloquent\Model;
use App\Models\Tenants\Category;
class ProjectCategory extends Model
{
    protected $table = 'project_categories';
    protected $fillable = ['id','project_id','category_id','created_at','updated_id'];

    function categories(){
    	return $this->hasMany( Category::class,'id','category_id');
    }
}
