<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Activation extends Model
{
    protected $table = 'activations';
    protected $fillable = ['id','code','app_login_id','code','completed','completed_at','created_at','expired_date','updated_at','user_id'];

    function appLogin(){
    	return $this->hasOne( AppLogin::class,'id','app_login_id' );
    }
}
