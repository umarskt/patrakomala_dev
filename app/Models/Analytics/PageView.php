<?php

namespace App\Models\Analytics;

use Illuminate\Database\Eloquent\Model;
use DB;

class PageView extends Model
{
	protected $connection = 'analytic';
    protected $table = 'pageview';
    protected $fillable = [
    	'id','page_slug','device','ip_address','browser','os','created_at','updated_at'
    ];

    public static function getAnalytic(){
    	return Self::select( DB::raw(
    		"COUNT( CASE WHEN page_slug = 'hackathon-page' THEN 1 END  ) as hackathon, 
    		 COUNT( CASE WHEN page_slug = 'register-page' THEN 1 END ) as tenant_register") 
         )->first();
    }
}
