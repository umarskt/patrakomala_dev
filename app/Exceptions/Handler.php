<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Support\MessageBag;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {

         if( $request->segment( 1 ) == 'api' ) {
            $data = [
                'File' => $e->getFile(),
                'Line' => $e->getLine(),
                'Message' => $e->getMessage(),
            ];

             if($e instanceof ValidationException){
                 return response()->json( [
                    'status'  => 422,
                    'message' => 'InvalidRequest',
                    'errors'  => $e->validator->errors()->all()
                ], 422 );
             }


             if($e instanceof NotFoundHttpException){
                return response()->json( [
                    'status' => 404,
                    'message' => 'NotFound',
                    'errors' => $e->getMessage()
                ], 404 );
             }

             if($e instanceof MethodNotAllowedHttpException){
                return response()->json( [
                    'status' => 405,
                    'message' => 'MethodNotAllowed',
                    'errors' => $e->getMessage()
                ], 405 );
             }


            if( ! $e instanceof \Illuminate\Http\Exception\HttpResponseException ) {
                return response()->json( [
                    'error'     => count($e->getMessage()),
                    'status'    => 500,
                    'data_error'=> $data,
                    
                ], 500 );
            }

            
        }
        return parent::render($request, $e);
    }
}
