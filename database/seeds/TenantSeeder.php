<?php

use Illuminate\Database\Seeder;

class TenantSeeder extends Seeder
{

	protected $user_id = null;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('ekraf_tenants')->truncate();
    	DB::table('tenant_social_sites')->truncate();
    	DB::table('tenant_stores')->truncate();
    	DB::table('tenant_address')->truncate();
    	DB::table('tenant_categories')->truncate();
    	DB::table('tenant_itineraries')->truncate();

    	
    	$datas = [
			[
				'street'=> 'Jl. Cigadung Raya Timur No. 107',
				'postal_code'=>'40265',
				'tenant_name'=>'C59',
				'tenant_phone'=>'+62222506640',
				'tenant_email'=>'C59@mail.com',
				'subdistrict_id'=>13,
				'latitude'=>-6.885611,
				'longitude'=>107.631291,
				'category_id'=>6,
				'itineraries_title'=>'Akses ke Cigadung',
				'itineraries_description'=>'Melalui perempatan borma, belok kanan dari arah pahlawan',
				'social_sites'=> array(
					[
					 'social_site_type'=>'Twitter',
					 'social_site_url' =>'https://twitter.com/C59Tshirt'
					],
					[
					 'social_site_type'=>'Websites',
					 'social_site_url' =>'www.C59.co.id'
					]
				),
				"users" => [
					"username"=> "fiftynine",
					"password"=> "12345678",
					"role" => "tenant"
				]
			],
			[
				'street'=> 'Jl. Cigadung Raya Timur No. 136',
				'postal_code'=>'40265',
				'tenant_name'=>'Hasan Batik Bandung',
				'tenant_email'=>'hasan_batik@mail.com',
				'tenant_phone'=>'+62222501029',
				'subdistrict_id'=>13,
				'latitude'=>-6.885425,
				'longitude'=>107.631012,
				'category_id'=>6,
				'itineraries_title'=>'Akses ke Cigadung',
				'itineraries_description'=>'Melalui perempatan borma, belok kanan dari arah pahlawan',
				'social_sites'=> array(
					[
					 'social_site_type'=> null,
					 'social_site_url' => null
					],
				),
				"users" => [
					"username"=> "hasan_batik",
					"password"=> "12345678",
					"role" => "tenant"
				]
			],
			[
				'tenant_name'=>'Wonderful Galeri Batik',
				'street'=> 'Jl. Cigadung Selatan I No. 2',
				'postal_code'=>'40265',
				'tenant_phone'=>'+620222501090',
				'tenant_email'=>'wonderful.ledie@yahoo.com',
				'subdistrict_id'=>13,
				'latitude'=>-6.885423,
				'longitude'=>107.631012,
				'category_id'=>6,
				'itineraries_title'=>'Akses ke Cigadung',
				'itineraries_description'=>'Melalui perempatan borma, belok kanan dari arah pahlawan',
				'social_sites'=> array(
					[
					 'social_site_type'=> null,
					 'social_site_url' => null
					],
				)
			],
			[
				'tenant_name'=>'Rumah Batik Komar',
				'street'=> 'Jl. Cigadung Raya Timur I No. 5',
				'postal_code'=>'40265',
				'tenant_phone'=>'+62222514788',
				'tenant_email'=>'rumah.batik@yahoo.com',
				'subdistrict_id'=>13,
				'latitude'=>-6.885423,
				'longitude'=>107.631022,
				'category_id'=>6,
				'itineraries_title'=>'Akses ke Cigadung',
				'itineraries_description'=>'Melalui perempatan borma, belok kanan dari arah pahlawan',
				'social_sites'=> array(
					[
					 'social_site_type'=> null,
					 'social_site_url' => null
					],
				),
				'tenant_stores' => array(
					[
						'store_open_at'=>'08:00',
						'store_close_at'=>'16:00',
						'is_facility_showroom' => true
					]
				), 
			],
			[
				'tenant_name'=>'Minen Leather',
				'street'=> 'Jl Mengger',
				'postal_code'=>'40265',
				'tenant_phone'=>'+6281805181983',
				'tenant_email'=>'default@mail.com',
				'subdistrict_id'=>1,
				'latitude'=>-6.885423,
				'longitude'=>107.631022,
				'category_id'=>6,
				'itineraries_title'=>'Akses ke Cigadung',
				'itineraries_description'=>'Melalui perempatan borma, belok kanan dari arah pahlawan',
				'social_sites'=> array(
					[
					 'social_site_type'=> 'instagram',
					 'social_site_url' => 'https://www.instagram.com/minenleather/'
					],
				)
			],
			[
				'tenant_name'=>'Galeri Yuliansyah Akbar (URBANE)',
				'street'=> 'Jl. Cigadung Raya Barat No. 5',
				'postal_code'=>'40265',
				'tenant_phone'=>'+62222531004',
				'tenant_email'=>'contact@urbane.co.id',
				'subdistrict_id'=>13,
				'latitude'=>-6.885423,
				'longitude'=>107.631032,
				'category_id'=>12,
				'itineraries_title'=>'Akses ke Cigadung',
				'itineraries_description'=>'Melalui perempatan borma, belok kanan dari arah pahlawan',
				'social_sites'=> array(
					[
					 'social_site_type'=> 'website',
					 'social_site_url' => 'www.urbane.co.id'
					],
					[
						'social_site_type'=> 'instagram',
					 	'social_site_url' => 'https://www.instagram.com/urbane.indonesia/'	
					],
				),
				'tenant_stores' => array(
					'store_open_at'=>'09:00',
					'store_close_at' => '17:00'
				),
			],
			[
				'tenant_name'=>'Kamones Workshop Gallery',
				'street'=> 'Jl. Cigadung Raya Barat No. 28A',
				'postal_code'=>'40265',
				'tenant_phone'=>'+6287822547919',
				'tenant_email'=>'Kamones@mail.co.id',
				'subdistrict_id'=>13,
				'latitude'=>-6.885423,
				'longitude'=>107.631032,
				'category_id'=>6,
				'itineraries_title'=>'Akses ke Cigadung',
				'itineraries_description'=>'Melalui perempatan borma, belok kanan dari arah pahlawan',
				'social_sites'=> array(
					[
					 'social_site_type'=> 'website',
					 'social_site_url' => 'www.urbane.co.id'
					],
					[
						'social_site_type'=> 'instagram',
					 	'social_site_url' => 'https://www.instagram.com/urbane.indonesia/'	
					],
				),
				'tenant_stores' => array(
					'store_open_at'		=> '09:00',
					'store_close_at' 	=> '17:00'
				),
			],
			[
				'tenant_name'=>'Ramen Udin',
				'street'=> 'Jl. Cijagra',
				'postal_code'=>'40264',
				'tenant_phone'=>'+6287722255887',
				'tenant_email'=>'ramen.udin@mail.com',
				'subdistrict_id'=>22,
				'latitude'=>-6.946058,
				'longitude'=>107.626690,
				'category_id'=>5,
				'itineraries_title'=>'Akses ke Tempat',
				'itineraries_description'=>'Di jalan cijagra dekat hotel',
				'social_sites'=> array(
					[
					 'social_site_type'=> 'facebook',
					 'social_site_url' => 'https://www.facebook.com/'
					],
				),
				'tenant_stores' => array(
					[
						'store_open_at'=>'10:00',
						'store_close_at'=>'22:00',
						'is_facility_showroom' => true
					]
				), 
			],
			[
				'tenant_name'=>'Sushi Teio',
				'street'=> 'Jl. Buah Batu No. 205 Lengkong',
				'postal_code'=>'40264',
				'tenant_phone'=>'+6281222119987',
				'tenant_email'=>'sushi.teio@mail.com',
				'subdistrict_id'=>22,
				'latitude'=>-6.941627,
				'longitude'=>107.628138,
				'category_id'=>5,
				'itineraries_title'=>'Akses ke Tempat',
				'itineraries_description'=>'Melalui perempatan batununggal dari arah soekarno hatta, terus lurus',
				'social_sites'=> array(
					[
					 'social_site_type'=> 'facebook',
					 'social_site_url' => 'https://www.facebook.com/'
					],
				),
				'tenant_stores' => array(
					[
						'store_open_at'=>'10:00',
						'store_close_at'=>'22:00',
						'is_facility_showroom' => true
					]
				), 
			],
    	];//close
   
    		foreach ($datas as $key => $data) {

	    		/*if( !empty($data['users']) ){
	    			$user = Sentinel::registerAndActivate([
	    				"username"=> $data['users']['username'],
						"password"=> $data['users']['password'],
						"email" =>  $data['tenant_email']
	    			]);
	    			$this->user_id = $user->id;
	    			Sentinel::findRoleBySlug( $data['users']['role'] )->users()->attach( $user );
	    		}*/

    			$tenant = App\Models\Tenants\Tenant::create([
	    			'tenant_name' => $data['tenant_name'],
	    			'tenant_phone' => $data['tenant_phone'],
	    			'tenant_email' => $data['tenant_email'],
	    			'user_id'	=> $this->user_id
	    		]);

	    		App\Models\Tenants\TenantAddress::create([
	    			'ekraf_tenant_id' => $tenant->id,
	    			'street' => $data['street'],
	    			'postal_code' => $data['postal_code'],
	    			'subdistrict_id'=> $data['subdistrict_id'],
	    			'latitude'=> $data['latitude'],
	    			'longitude' => $data['longitude']
	    		]);

	    		App\Models\Tenants\TenantItinerary::create([
	    			'ekraf_tenant_id' => $tenant->id,
	    			'itineraries_title'=> $data['itineraries_title'],
	    			'itineraries_description' => $data['itineraries_description']
	    		]);

	    		DB::table('tenant_categories')->insert([
        			'category_id' => $data['category_id'],
        			'ekraf_tenant_id' => $tenant->id
        		]);

	    		if( !empty($data['social_sites']) ){
					foreach ($data['social_sites'] as $key => $social_site) {
	    			   App\Models\Tenants\TenantSocialSite::create([
		    			   	'ekraf_tenant_id' => $tenant->id,
		    			   	'social_site_type' => $social_site['social_site_type'],
		    			   	'social_site_url'  => $social_site['social_site_url']
	    			   ]);
	    			}
	    		}
    	   }//close foreach
    	
    }
}
