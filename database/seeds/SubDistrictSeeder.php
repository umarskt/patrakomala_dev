<?php

use Illuminate\Database\Seeder;

class SubDistrictSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sub_districts')->truncate();
        DB::table('urban_villages')->truncate();

        $datas = [
        	[
                'sub_district_name'=>'Andir',
                'sub_district_slug'=>'andir',
                'villages' =>  [
                    [
                        'village_name'=>'Campaka',
                        'village_postal_code' => '40184'
                    ],
                    [
                        'village_name'=>'Ciroyom',
                        'village_postal_code' => '40182'
                    ],
                    [
                        'village_name'=>'Dungus Cariang',
                        'village_postal_code' => '40183'
                    ],
                    [
                        'village_name'=>'Garuda',
                        'village_postal_code' => '40184'
                    ],
                    [
                        'village_name'=>'Kebon Jeruk',
                        'village_postal_code' => '40181'
                    ],
                    [
                        'village_name'=>'Maleber (Maleer)',
                        'village_postal_code' => '40184'
                    ]
                ]
            ],
            [ 
                'sub_district_name'=>'Antapani',
                'sub_district_slug'=>'antapani',
                'villages' =>  [
                    [
                        'village_name'=>'Antapani Kidul',
                        'village_postal_code' => '40291'
                    ],
                    [
                        'village_name'=>'Antapani Kulon',
                        'village_postal_code' => '40291'
                    ],
                    [
                        'village_name'=>'Antapani Tengah',
                        'village_postal_code' => '40291'
                    ],
                    [
                        'village_name'=>'Antapani Wetan',
                        'village_postal_code' => '40291'
                    ]
                ]
            ],
            [
                'sub_district_name'=>'Arcamanik',
                'sub_district_slug'=>'arcamanik',
                'villages' =>  [
                    [
                        'village_name'=>'Cisaranten Kulon',
                        'village_postal_code' => '40293'
                    ],
                    [
                        'village_name'=>'Cisaranten Bina Harapan',
                        'village_postal_code' => '40294'
                    ],
                    [
                        'village_name'=>'Sindang Jaya',
                        'village_postal_code' => '40293'
                    ],
                    [
                        'village_name'=>'Sukamiskin',
                        'village_postal_code' => '40293'
                    ]
                ]
            ],
            [    
                'sub_district_name'=>'Astana Anyar',
                'sub_district_slug'=>'astana-anyar',
                'villages' =>  [
                    [
                        'village_name'=>'Cibadak',
                        'village_postal_code' => '40241'
                    ],
                    [
                        'village_name'=>'Karanganyar',
                        'village_postal_code' => '40241'
                    ],
                    [
                        'village_name'=>'Karasak',
                        'village_postal_code' => '40243'
                    ],
                    [
                        'village_name'=>'Nyengseret',
                        'village_postal_code' => '40242'
                    ],
                    [
                        'village_name'=>'Panjunan',
                        'village_postal_code' => '40242'
                    ],
                    [
                        'village_name'=>'Pelindung Hewan',
                        'village_postal_code' => '40243'
                    ]
                ]
            ],
            [    
                'sub_district_name'=>'Babakan Ciparay',
                'sub_district_slug'=>'babakan-ciparay',
                'villages' =>  [
                    [
                        'village_name'=>'Babakan',
                        'village_postal_code' => '40222'
                    ],
                    [
                        'village_name'=>'Babakan Ciparay',
                        'village_postal_code' => '40223'
                    ],
                    [
                        'village_name'=>'Cirangrang',
                        'village_postal_code' => '40227'
                    ],
                    [
                        'village_name'=>'Margahayu Utara',
                        'village_postal_code' => '40224'
                    ],
                    [
                        'village_name'=>'Margasuka',
                        'village_postal_code' => '40225'
                    ],
                    [
                        'village_name'=>'Sukahaji',
                        'village_postal_code' => '40221'
                    ]
                ]
            ],
            [    
                'sub_district_name'=>'Bandung Kidul',
                'sub_district_slug'=>'bandung-kidul',
                'villages' =>  [
                    [
                        'village_name'=>'Batununggal',
                        'village_postal_code' => '40266'
                    ],
                    [
                        'village_name'=>'Kujangsari',
                        'village_postal_code' => '40287'
                    ],
                    [
                        'village_name'=>'Mengger',
                        'village_postal_code' => '40267'
                    ],
                    [
                        'village_name'=>'Wates',
                        'village_postal_code' => '40256'
                    ]
                ]
            ],
            [
                'sub_district_name'=>'Bandung Kulon',
                'sub_district_slug'=>'bandung-kulon',
                'villages' =>  [
                    [
                        'village_name'=>'Caringin',
                        'village_postal_code' => '40212'
                    ],
                    [
                        'village_name'=>'Cibuntu',
                        'village_postal_code' => '40212'
                    ],
                    [
                        'village_name'=>'Cigondewah Kaler',
                        'village_postal_code' => '40214'
                    ],
                    [
                        'village_name'=>'Cigondewah Kidul',
                        'village_postal_code' => '40214'
                    ],
                    [
                        'village_name'=>'Cigondewah Rahayu',
                        'village_postal_code' => '40215'
                    ],
                    [
                        'village_name'=>'Cijerah',
                        'village_postal_code' => '40213'
                    ],
                    [
                        'village_name'=>'Gempolsari',
                        'village_postal_code' => '40215'
                    ],
                    [
                        'village_name'=>'Warung Muncang',
                        'village_postal_code' => '40211'
                    ]
                ]
            ],
            [
                'sub_district_name'=>'Bandung Wetan',
                'sub_district_slug'=>'bandung-wetan',
                'villages' =>  [
                    [
                        'village_name'=>'Cihapit',
                        'village_postal_code' => '40114'
                    ],
                    [
                        'village_name'=>'Citarum',
                        'village_postal_code' => '40115'
                    ],
                    [
                        'village_name'=>'Tamansari',
                        'village_postal_code' => '40116'
                    ]
                ]
            ],
            [
                'sub_district_name'=>'Batununggal',
                'sub_district_slug'=>'batununggal',
                'villages' =>  [
                    [
                        'village_name'=>'Binong',
                        'village_postal_code' => '40275'
                    ],
                    [
                        'village_name'=>'Cibangkong',
                        'village_postal_code' => '40273'
                    ],
                    [
                        'village_name'=>'Gumuruh',
                        'village_postal_code' => '40275'
                    ],
                    [
                        'village_name'=>'Kacapiring',
                        'village_postal_code' => '40271'
                    ],
                    [
                        'village_name'=>'Kebon Gedang',
                        'village_postal_code' => '40274'
                    ],
                    [
                        'village_name'=>'Kebonwaru',
                        'village_postal_code' => '40272'
                    ],
                    [
                        'village_name'=>'Maleer',
                        'village_postal_code' => '40274'
                    ],
                    [
                        'village_name'=>'Samoja',
                        'village_postal_code' => '40273'
                    ]
                ]
            ],
            [
                'sub_district_name'=>'Bojongloa Kidul',
                'sub_district_slug'=>'bojongloa-kidul',
                'villages' =>  [
                    [
                        'village_name'=>'Cibaduyut',
                        'village_postal_code' => '40236'
                    ],
                    [
                        'village_name'=>'Cibaduyut Kidul',
                        'village_postal_code' => '40239'
                    ],
                    [
                        'village_name'=>'Cibaduyut Wetan',
                        'village_postal_code' => '40238'
                    ],
                    [
                        'village_name'=>'Kebon Lega',
                        'village_postal_code' => '40235'
                    ],
                    [
                        'village_name'=>'Mekarwangi',
                        'village_postal_code' => '40237'
                    ],
                    [
                        'village_name'=>'Situsaeur',
                        'village_postal_code' => '40234'
                    ]
                ]
            ],
            [
            'sub_district_name'=>'Buahbatu',
            'sub_district_slug'=>'buahbatu',
                'villages' =>  [
                    [
                        'village_name'=>'Cijarua (Margasenang)',
                        'village_postal_code' => '40287'
                    ],
                    [
                        'village_name'=>'Jatisari',
                        'village_postal_code' => '40286'
                    ],
                    [
                        'village_name'=>'Margasari',
                        'village_postal_code' => '40286'
                    ],
                    [
                        'village_name'=>'Sekejati',
                        'village_postal_code' => '40286'
                    ]
                ]
            ],
            [
            'sub_district_name'=>'Cibeunying Kaler',
            'sub_district_slug'=>'cibeunying-kaler',
                'villages' =>  [
                    [
                        'village_name'=>'Cigadung',
                        'village_postal_code' => '40191'
                    ],
                    [
                        'village_name'=>'Cihaur Geulis',
                        'village_postal_code' => '40122'
                    ],
                    [
                        'village_name'=>'Neglasari',
                        'village_postal_code' => '40124'
                    ],
                    [
                        'village_name'=>'Sukaluyu',
                        'village_postal_code' => '40123'
                    ]
                ]
            ],
            [
                'sub_district_name'=>'Cibeunying Kidul',
                'sub_district_slug'=>'cibeunying-kidul',
                'villages' =>  [
                    [
                        'village_name'=>'Cicadas',
                        'village_postal_code' => '40121'
                    ],
                    [
                        'village_name'=>'Cikutra',
                        'village_postal_code' => '40124'
                    ],
                    [
                        'village_name'=>'Padasuka',
                        'village_postal_code' => '40125'
                    ],
                    [
                        'village_name'=>'Pasirlayung',
                        'village_postal_code' => '40192'
                    ],
                    [
                        'village_name'=>'Sukamaju',
                        'village_postal_code' => '40121'
                    ],
                    [
                        'village_name'=>'Sukapada',
                        'village_postal_code' => '40125'
                    ]
                ]
            ],
            [
                'sub_district_name'=>'Cibiru',
                'sub_district_slug'=>'cibiru',
                'villages' =>  [
                    [
                        'village_name'=>'Cipadung',
                        'village_postal_code' => '40614'
                    ],
                    [
                        'village_name'=>'Cisurupan',
                        'village_postal_code' => '40614'
                    ],
                    [
                        'village_name'=>'Palasari',
                        'village_postal_code' => '40615'
                    ],
                    [
                        'village_name'=>'Pasirbiru',
                        'village_postal_code' => '40615'
                    ]
                ]
            ],
            [
                'sub_district_name'=>'Cicendo',
                'sub_district_slug'=>'cicendo',
                'villages' =>  [
                    [
                        'village_name'=>'Arjuna',
                        'village_postal_code' => '40172'
                    ],
                    [
                        'village_name'=>'Husen Sastranegara',
                        'village_postal_code' => '40174'
                    ],
                    [
                        'village_name'=>'Pajajaran',
                        'village_postal_code' => '40173'
                    ],
                    [
                        'village_name'=>'Pamoyanan',
                        'village_postal_code' => '40173'
                    ],
                    [
                        'village_name'=>'Pasir Kaliki',
                        'village_postal_code' => '40171'
                    ],
                    [
                        'village_name'=>'Sukaraja',
                        'village_postal_code' => '40175'
                    ]
                ]
            ],
            [
                'sub_district_name'=>'Cidadap',
                'sub_district_slug'=>'cidadap',
                'villages' =>  [
                    [
                        'village_name'=>'Ciumbuleuit',
                        'village_postal_code' => '40142'
                    ],
                    [
                        'village_name'=>'Hegarmanah',
                        'village_postal_code' => '40141'
                    ],
                    [
                        'village_name'=>'Ledeng',
                        'village_postal_code' => '40143'
                    ]
                ]
            ],
            [
                'sub_district_name'=>'Cinambo',
                'sub_district_slug'=>'cinambo',
                'villages' =>  [
                    [
                        'village_name'=>'Babakan Penghulu',
                        'village_postal_code' => '40294'
                    ],
                    [
                        'village_name'=>'Cisaranten Wetan',
                        'village_postal_code' => '40294'
                    ],
                    [
                        'village_name'=>'Pakemitan',
                        'village_postal_code' => '40294'
                    ],
                    [
                        'village_name'=>'Sukamulya',
                        'village_postal_code' => '40294'
                    ]
                ]
            ],
            [
                'sub_district_name'=>'Coblong',
                'sub_district_slug'=>'coblong',
                'villages' =>  [
                    [
                        'village_name'=>'Cipaganti',
                        'village_postal_code' => '40131'
                    ],
                    [
                        'village_name'=>'Dago',
                        'village_postal_code' => '40135'
                    ],
                    [
                        'village_name'=>'Lebak Gede',
                        'village_postal_code' => '40132'
                    ],
                    [
                        'village_name'=>'Lebak Siliwangi',
                        'village_postal_code' => '40132'
                    ],
                    [
                        'village_name'=>'Sadang Serang',
                        'village_postal_code' => '40133'
                    ],
                    [
                        'village_name'=>'Sekeloa',
                        'village_postal_code' => '40134'
                    ]
                ]
            ],
            [
                'sub_district_name'=>'Gedebage',
                'sub_district_slug'=>'gedebage',
                'villages' =>  [
                    [
                        'village_name'=>'Cimenerang (Cimincrang)',
                        'village_postal_code' => '40294'
                    ],
                    [
                        'village_name'=>'Cisaranten Kidul',
                        'village_postal_code' => '40294'
                    ],
                    [
                        'village_name'=>'Rancabalong',
                        'village_postal_code' => '40294'
                    ],
                    [
                        'village_name'=>'Rancanumpang',
                        'village_postal_code' => '40294'
                    ]
                ]
            ],
            [
                'sub_district_name'=>'Kiaracondong',
                'sub_district_slug'=>'kiaracondong',
                'villages' =>  [
                    [
                        'village_name'=>'Babakan Sari',
                        'village_postal_code' => '40283'
                    ],
                    [
                        'village_name'=>'Babakan Surabaya',
                        'village_postal_code' => '40281'
                    ],
                    [
                        'village_name'=>'Cicaheum',
                        'village_postal_code' => '40282'
                    ],
                    [
                        'village_name'=>'Kebon Kangkung',
                        'village_postal_code' => '40284'
                    ],
                    [
                        'village_name'=>'Kebun Jayanti',
                        'village_postal_code' => '40281'
                    ],
                    [
                        'village_name'=>'Sukapura',
                        'village_postal_code' => '40285'
                    ]
                ]
            ],
            [
                'sub_district_name'=>'Lengkong',
                'sub_district_slug'=>'lengkong',
                'villages' =>  [
                    [
                        'village_name'=>'Burangrang',
                        'village_postal_code' => '40262'
                    ],
                    [
                        'village_name'=>'Cijagra',
                        'village_postal_code' => '40265'
                    ],
                    [
                        'village_name'=>'Cikawao',
                        'village_postal_code' => '40261'
                    ],
                    [
                        'village_name'=>'Lingkar Selatan',
                        'village_postal_code' => '40263'
                    ],
                    [
                        'village_name'=>'Malabar',
                        'village_postal_code' => '40262'
                    ],
                    [
                        'village_name'=>'Paledang',
                        'village_postal_code' => '40261'
                    ],
                    [
                        'village_name'=>'Turangga',
                        'village_postal_code' => '40264'
                    ]
                ]
            ],
            [
                'sub_district_name'=>'Mandalajati',
                'sub_district_slug'=>'mandalajati',
                'villages' =>  [
                    [
                        'village_name'=>'Jatihandap',
                        'village_postal_code' => '40195'
                    ],
                    [
                        'village_name'=>'Karang Pamulang',
                        'village_postal_code' => '40195'
                    ],
                    [
                        'village_name'=>'Pasir Impun',
                        'village_postal_code' => '40195'
                    ],
                    [
                        'village_name'=>'Sindang Jaya',
                        'village_postal_code' => '40195'
                    ]
                ]
            ],
            [
                'sub_district_name'=>'Panyileukan',
                'sub_district_slug'=>'panyileukan',
                'villages' =>  [
                    [
                        'village_name'=>'Cipadung Kidul',
                        'village_postal_code' => '40614'
                    ],
                    [
                        'village_name'=>'Cipadung Kulon',
                        'village_postal_code' => '40614'
                    ],
                    [
                        'village_name'=>'Cipadung Wetan',
                        'village_postal_code' => '40614'
                    ],
                    [
                        'village_name'=>'Mekarmulya',
                        'village_postal_code' => '40614'
                    ]
                ]
            ],
            [
                'sub_district_name'=>'Rancasari',
                'sub_district_slug'=>'rancasari',
                'villages' =>  [
                    [
                        'village_name'=>'Cipamokolan',
                        'village_postal_code' => '40292'
                    ],
                    [
                        'village_name'=>'Darwati',
                        'village_postal_code' => '40292'
                    ],
                    [
                        'village_name'=>'Manjahlega (Cisarantenkidul)',
                        'village_postal_code' => '40295'
                    ],
                    [
                        'village_name'=>'Mekar Mulya (Mekarjaya)',
                        'village_postal_code' => '40292'
                    ]
                ]
            ],
            [
                'sub_district_name'=>'Regol',
                'sub_district_slug'=>'regol',
                'villages' =>  [
                    [
                        'village_name'=>'Ancol',
                        'village_postal_code' => '40254'
                    ],
                    [
                        'village_name'=>'Balong Gede',
                        'village_postal_code' => '40251'
                    ],
                    [
                        'village_name'=>'Ciateul',
                        'village_postal_code' => '40252'
                    ],
                    [
                        'village_name'=>'Cigereleng',
                        'village_postal_code' => '40253'
                    ],
                    [
                        'village_name'=>'Ciseureuh',
                        'village_postal_code' => '40255'
                    ],
                    [
                        'village_name'=>'Pasirluyu',
                        'village_postal_code' => '40254'
                    ],
                    [
                        'village_name'=>'Pungkur',
                        'village_postal_code' => '40252'
                    ]
                ]
            ],
            [
                'sub_district_name'=>'Sukajadi',
                'sub_district_slug'=>'sukajadi',
                'villages' =>  [
                    [
                        'village_name'=>'Cipedes',
                        'village_postal_code' => '40162'
                    ],
                    [
                        'village_name'=>'Pasteur',
                        'village_postal_code' => '40161'
                    ],
                    [
                        'village_name'=>'Sukabungah',
                        'village_postal_code' => '40162'
                    ],
                    [
                        'village_name'=>'Sukagalih',
                        'village_postal_code' => '40163'
                    ],
                    [
                        'village_name'=>'Sukawarna',
                        'village_postal_code' => '40164'
                    ]
                ]
            ],
            [
                'sub_district_name'=>'Sukasari',
                'sub_district_slug'=>'sukasari',
                'villages' =>  [
                    [
                        'village_name'=>'Geger Kalong',
                        'village_postal_code' => '40153'
                    ],
                    [
                        'village_name'=>'Isola',
                        'village_postal_code' => '40154'
                    ],
                    [
                        'village_name'=>'Sarijadi',
                        'village_postal_code' => '40151'
                    ],
                    [
                        'village_name'=>'Sukarasa',
                        'village_postal_code' => '40152'
                    ]
                ]
            ],
            [
            'sub_district_name'=>'Sumur Bandung',
            'sub_district_slug'=>'sumur-bandung',
                'villages' =>  [
                    [
                        'village_name'=>'Babakan Ciamis',
                        'village_postal_code' => '40117'
                    ],
                    [
                        'village_name'=>'Braga',
                        'village_postal_code' => '40111'
                    ],
                    [
                        'village_name'=>'Kebon Pisang',
                        'village_postal_code' => '40112'
                    ],
                    [
                        'village_name'=>'Merdeka',
                        'village_postal_code' => '40113'
                    ]
                ]
            ],
            [
            'sub_district_name'=>'Ujung Berung',
            'sub_district_slug'=>'ujung-berung',
                'villages' =>  [
                    [
                        'village_name'=>'Cigending',
                        'village_postal_code' => '40611'
                    ],
                    [
                        'village_name'=>'Pasanggrahan',
                        'village_postal_code' => '40617'
                    ],
                    [
                        'village_name'=>'Pasir Endah',
                        'village_postal_code' => '40619'
                    ],
                    [
                        'village_name'=>'Pasirjati',
                        'village_postal_code' => '40616'
                    ],
                    [
                        'village_name'=>'Pasirwangi',
                        'village_postal_code' => '40618'
                    ]
                ]
            ]
        ];

        foreach ($datas as $key => $data) {
        	$subdistrict = App\Models\SubDistrict::Create( [
                'sub_district_name' => $data['sub_district_name'],
                'sub_district_slug' => $data['sub_district_slug']
            ] );
            if( !empty($data['villages']) ){
                foreach ($data['villages'] as $key => $village) {
                    App\Models\Village::create([
                        'sub_district_id' => $subdistrict->id,
                        'village_name' => $village['village_name'],
                        'village_name_slug' => str_slug( $village['village_name'], '-' ),
                        'village_postal_code' => $village['village_postal_code']
                    ]);
                }
            }
        }
    }
}
