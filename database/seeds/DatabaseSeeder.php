<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleUserSeeder::class);
        $this->call(UserAdminSeeder::class);
        $this->call(TenantSeeder::class);
        $this->call(SubsectorTenantSeeder::class);
        $this->call(SubDistrictSeeder::class);
    }
}
