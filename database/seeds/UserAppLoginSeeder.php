<?php

use Illuminate\Database\Seeder;
use App\Models\AppLogin;

class UserAppLoginSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
        	[
        		'app_email' => 'fikurimufc@gmail.com',
        		'app_username' => 'mochammad.fikri',
        		'is_active' => true,
        		'client_first_name' => 'Mochammad',
        		'client_last_name'  => 'Hikmatiar',
        		'company_name' => 'Kartala',
                'user_type' => 'client'
        	],
        	[
        		'app_email' =>'aj.biner@gmail.com',
        		'app_username' => 'ainun.jariya',
        		'is_active'  => true,
        		'client_first_name' => 'Ainun',
        		'client_last_name'  => 'Jariya',
        		'company_name' => 'Kartala',
                'user_type' => 'client'
        	]
        ];

        foreach ($datas as $key => $data) {
        	$client =  new AppLogin;
            $client->registerAndActive($data);
        }
    }
}
