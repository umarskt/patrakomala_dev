<?php

use Illuminate\Database\Seeder;

class RoleUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('role_users')->truncate();
        DB::table('roles')->truncate();

        Sentinel::getRoleRepository()->createModel()->create([
            'slug' => 'super-admin',
            'name' => 'Super Administrator',
            'permissions' => [
                'admin' => true
            ]
        ]);

        Sentinel::getRoleRepository()->createModel()->create([
            'slug' => 'system-administrator',
            'name' => 'System Administrator',
            'permissions' => [
                'admin' => true
            ]
        ]);

        Sentinel::getRoleRepository()->createModel()->create([
            'slug' => 'tenant',
            'name' => 'Tenant',
            'permissions' => [
                'admin' => false
            ]
        ]);

        Sentinel::getRoleRepository()->createModel()->create([
            'slug' => 'user-jobs',
            'name' => 'User Jobs',
            'permissions' => [
                'admin' => false
            ]
        ]);

    }
}
