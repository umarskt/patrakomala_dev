<?php

use Illuminate\Database\Seeder;

class UserAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('activations')->truncate();
        DB::table('persistences')->truncate();
        DB::table('reminders')->truncate();
        DB::table('role_users')->truncate();
        DB::table('throttle')->truncate();
        // DB::table('users')->truncate();
        DB::statement('TRUNCATE users CASCADE');

        $datas = [
            [
                'email' => 'admin@ekraf.com',
                'full_name' => 'super administrator',
                'role' => 'super-admin',
                'username' => 'admin'
            ],
            [
                'email' => 'fikurimufc@gmail.com',
                'full_name'=>'Fikri hikmatiar',
                'role' => 'system-administrator',
                'username' => 'admin'
            ],
            [
                'email' => 'dhon.joe@gmail.com',
                'full_name' => 'Dhon Joe',
                'role' => 'system-administrator',
                'username' => 'admin'
            ],
            [
                'email' => 'aj.biner@gmail.com',
                'full_name' => 'aj.biner@gmail.com',
                'role' => 'system-administrator',
                'username' => 'admin'
            ]
        ];

        foreach ( $datas as $key => $data ) {
            $user = Sentinel::registerAndActivate( [
                'email' => $data[ 'email' ],
                'password' => '12345678',
                'full_name' => $data[ 'full_name' ],
                'username'  => $data['username']
            ] );
            Sentinel::findRoleBySlug( $data[ 'role' ] )->users()->attach( $user );
        }

       /* $faker = \Faker\Factory::create();
        for ($i = 2; $i <= 5; $i++) {
            $user = Sentinel::registerAndActivate( [
                'email' => $faker->freeEmail,
                'password' => '12345678',
                'full_name' => $faker->firstName,
                'username' => $faker->userName
            ] );

            Sentinel::findRoleBySlug('tenant')->users()->attach( $user );
        }*/
    }
}
