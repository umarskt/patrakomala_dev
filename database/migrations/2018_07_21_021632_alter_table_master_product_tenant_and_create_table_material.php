<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableMasterProductTenantAndCreateTableMaterial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenant_products', function (Blueprint $table) {
            $table->dropColumn('material_product_amount');
            $table->dropColumn('product_supplier');
            $table->string('tenant_product_quantity')->nullable();
            $table->string('tenant_product_unit')->nullable();
        });

        Schema::create('tenant_product_materials', function( Blueprint $table ){
            $table->increments('id');
            $table->integer('tenant_product_id');
            $table->string('material_name', 33)->nullable();
            $table->string('material_yearly_quantity')->nullable();
            $table->string('material_yearly_expenses')->nullable();
            $table->string('material_provider')->nullable();
            $table->timestamps();
        });

        Schema::table('tool_products', function( Blueprint $table ){
            $table->string('tool_name')->nullable();
            $table->renameColumn('machine_qty','tool_quantity');
            $table->renameColumn('machine_capacity','tool_capacity');
            $table->renameColumn('machine_price', 'tool_price');
            $table->timestamps();
        });

        Schema::rename('tool_products','tenant_product_tools');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_products', function (Blueprint $table) {
            $table->string('material_product_amount')->nullable();
            $table->string('product_supplier')->nullable();
            $table->dropColumn('tenant_product_quantity');
            $table->dropColumn('tenant_product_unit');
        });

        Schema::dropIfExists('tenant_product_materials');

        Schema::table('tenant_product_tools', function( Blueprint $table ){
            $table->dropColumn('tool_name');
            $table->renameColumn('tool_quantity','machine_qty');
            $table->renameColumn('tool_capacity','machine_capacity');
            $table->renameColumn('tool_price','machine_price');
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });

        Schema::rename('tenant_product_tools','tool_products');

    }
}
