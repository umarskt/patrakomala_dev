<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTenantEmployees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_employees', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_male_total')->nullable();
            $table->integer('employee_female_total')->nullable();
            $table->string('employee_general_fee')->nullable();
            $table->integer('employee_elementary_school_total')->nullable();
            $table->integer('employee_junior_high_total')->nullable();
            $table->integer('employee_high_total')->nullable();
            $table->integer('employee_d1_total')->nullable();
            $table->integer('employee_d2_total')->nullable();
            $table->integer('employee_d3_total')->nullable();
            $table->integer('employee_bacheloor_degree_total')->nullable();
            $table->integer('employee_post_graduate_total')->nullable();
            $table->integer('employee_doctoral_degree_total')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenant_employees');
    }
}
