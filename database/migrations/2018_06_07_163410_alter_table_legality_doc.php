<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableLegalityDoc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenant_legality_docs', function (Blueprint $table) {
            $table->string('tenant_legality_img', 90);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_legality_docs', function (Blueprint $table) {
            $table->dropColumn('tenant_legality_img');
        });
    }
}
