<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTenantLegalityDocAddedHkiAndTypeCompany extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenant_legality_docs', function (Blueprint $table) {
            $table->string('company_type',20)->nullable();
            $table->string('company_hki',20)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_legality_docs', function (Blueprint $table) {
            $table->dropColumn('company_type');
            $table->dropColumn('company_hki');
        });
    }
}
