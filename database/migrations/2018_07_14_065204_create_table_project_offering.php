<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProjectOffering extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('app_login_id');
            $table->string('project_title', 80);
            $table->text('project_description')->nullable();
            $table->string('project_status',20);
            $table->string('project_estimated_cost');
            $table->string('project_type')->nullable();
            $table->timestamps();
        });

        Schema::create('project_categories', function( Blueprint $table ){
            $table->increments('id');
            $table->integer('project_id');
            $table->integer('category_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
        Schema::dropIfExists('project_categories');
    }
}
