<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTenantAddressChangeTypeLatLong extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('tenant_address')->truncate();
        Schema::table('tenant_address', function (Blueprint $table) {
            $table->float('latitude',11, 7)->nullable()->change();
            $table->float('longitude',11, 7)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_address', function (Blueprint $table) {
            $table->decimal('latitude',11, 7)->nullable()->change();
            $table->decimal('longitude',11, 7)->nullable()->change();
        });
    }
}
