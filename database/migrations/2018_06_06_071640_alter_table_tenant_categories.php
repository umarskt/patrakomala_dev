<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTenantCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('categories', function( Blueprint $table ){
            $table->string('category_img',50);
            $table->string('category_slug',60);
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function( Blueprint $table ){
            $table->dropColumn('category_img');
            $table->dropColumn('category_slug');
        });
    }
}
