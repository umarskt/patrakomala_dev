<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableAppLoginAddPinCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('app_logins', function (Blueprint $table) {
            $table->string('generate_pin',8)->nullable();
            $table->string('token', 30)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('app_logins', function (Blueprint $table) {
            $table->dropColumn('generate_pin');
            $table->dropColumn('token');
        });
    }
}
