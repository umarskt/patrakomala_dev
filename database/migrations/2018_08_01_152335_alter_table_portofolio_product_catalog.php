<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePortofolioProductCatalog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('tenant_portofolio_product_catalog', 'tenant_portofolio_catalogs');
        Schema::table('tenant_portofolio_catalogs', function( Blueprint $table ) {
            $table->renameColumn('img_product','img_portofolio');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('tenant_portofolio_catalogs', 'tenant_portofolio_product_catalog');
        Schema::table('tenant_portofolio_catalogs', function( Blueprint $table ){
            $table->renameColumn('img_portofolio','img_product');
        });
    }
}
