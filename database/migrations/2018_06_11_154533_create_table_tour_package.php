<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTourPackage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('tourism_belt', function( Blueprint $table ){
            $table->increments('id');
            $table->string('belt_title',40);
            $table->text('belt_description');
            $table->timestamps();
        });

        Schema::create('tourism_package', function( Blueprint $table ){
            $table->increments('id');
            $table->string('package_name',40);
            $table->integer('tourism_belt_id');
            $table->text('package_description')->nullable();
            $table->integer('estimated_cost');
            $table->string('provider',40);
            $table->timestamps();
        });

        Schema::create('tenant_tourism_package', function( Blueprint $table ){
            $table->increments('id');
            $table->integer('ekraf_tenant_id');
            $table->integer('tourism_package_id');
        });

        Schema::create('itineraries', function( Blueprint $table ){
            $table->increments('id');
            $table->integer('ekraf_tenant_id');
            $table->string('itineraries_title',40);
            $table->text('itineraries_description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::dropIfExists('tourism_package');
          Schema::dropIfExists('tenant_tourism_package');
          Schema::dropIfExists('itineraries');
          Schema::dropIfExists('tourism_belt');
    }
}
