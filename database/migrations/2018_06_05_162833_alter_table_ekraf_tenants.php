<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableEkrafTenants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ekraf_tenants', function( Blueprint $table ){
             $table->string('ceo',30)->nullable()->change();
             $table->date('founding_year')->nullable()->change();
             $table->text('about')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ekraf_tenants', function( Blueprint $table ){
            $table->string('ceo', 30)->change();
            $table->date('founding_year')->change();
            $table->text('about')->change();
        });
    }
}
