<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTenantStores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenant_stores', function( Blueprint $table ){
            $table->boolean('is_local')->nullable()->change();
            $table->boolean('is_international')->nullable()->change();
            $table->string('marketing_destination_country')->nullable()->change();
            $table->float('marketing_percentage_ekspor')->nullable()->change();
            $table->boolean('marketing_by_online')->nullable()->change();
            $table->boolean('is_facility_parking')->nullable()->change();
            $table->boolean('is_facility_toilet')->nullable()->change();
            $table->boolean('is_facility_mushola')->nullable()->change();
            $table->boolean('is_facility_showroom')->nullable()->change();
            $table->integer('store_surface_area')->nullable()->change();
            $table->dropColumn('marketing_by_offline')->nullable()->change();
        });

        Schema::table('tenant_social_sites', function( Blueprint $table ){
            $table->renameColumn('social_sites_type','social_site_type');
            $table->renameColumn('social_sites_url','social_site_url');
        });

        Schema::table('tenant_social_sites', function( Blueprint $table ){
            $table->string('social_site_type',15)->nullable()->change();
            $table->string('social_site_url',100)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_social_sites', function( Blueprint $table ){
            $table->renameColumn('social_site_type','social_sites_type');
            $table->string('social_site_type',15)->change();
            $table->renameColumn('social_site_url','social_sites_url');
            $table->string('social_site_url')->change();
        });

        Schema::table('tenant_stores', function( Blueprint $table ){
            $table->boolean('is_local')->change();
            $table->boolean('is_international')->change();
            $table->string('marketing_destination_country')->change();
            $table->float('marketing_percentage_ekspor')->change();
            $table->boolean('marketing_by_online')->change();
            $table->boolean('marketing_by_offline');
            $table->boolean('is_facility_parking')->change();
            $table->boolean('is_facility_toilet')->change();
            $table->boolean('is_facility_mushola')->change();
            $table->boolean('is_facility_showroom')->change();
            $table->integer('store_surface_area')->change();
        });
    }
}
