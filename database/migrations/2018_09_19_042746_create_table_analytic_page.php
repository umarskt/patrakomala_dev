<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAnalyticPage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('analytic')->create('pageview', function($table){
            $table->increments('id');
            $table->string('page_slug', 40);
            $table->string('device', 15)->nullable();
            $table->ipAddress('ip_address')->nullable();
            $table->string('browser', 15)->nullable();
            $table->string('os', 15)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('analytic')->dropIfExists('pageview');
    }
}
