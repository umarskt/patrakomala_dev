<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableAppLoginAndCreateSosmedAccount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('app_logins', function (Blueprint $table) {  
            $table->dropColumn('facebook_id');
            $table->dropColumn('google_id');
        });

        Schema::table('clients', function ( Blueprint $table ){
            $table->string('client_last_name',40)->nullable()->change();
            $table->string('company_name', 40)->nullable()->change();
        });

        Schema::create('social_media_accounts', function( Blueprint $table ){
            $table->increments('id');
            $table->integer('app_login_id');
            $table->string('social',30);
            $table->string('social_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('app_logins', function (Blueprint $table) {  
            $table->string('facebook_id',80)->nullable();
            $table->string('google_id',80)->nullable();
        });

        Schema::dropIfExists('social_media_accounts');
    }
}
