<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCoachingCommunityBussiness extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_coaching_events', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ekraf_tenant_id');
            $table->string('coaching');
            $table->string('coaching_type');
            $table->string('coaching_year');
            $table->timestamps();
        });

        Schema::create('tenant_community_memberships', function( Blueprint $table ){
            $table->increments('id');
            $table->integer('ekraf_tenant_id');
            $table->string('membership_name');
            $table->timestamps();
        });

        Schema::create('tenant_business_strengths', function( Blueprint $table ){
            $table->increments('id');
            $table->integer('ekraf_tenant_id');
            $table->string('bussiness_strength_title', 80);
            $table->timestamps();
        });

        Schema::create('tenant_business_opportunities', function( Blueprint $table ){
            $table->increments('id');
            $table->integer('ekraf_tenant_id');
            $table->string('bussiness_opportunity_title', 80);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenant_coaching_events');
        Schema::dropIfExists('tenant_community_memberships');
        Schema::dropIfExists('tenant_business_strengths');
        Schema::dropIfExists('tenant_business_opportunities');
    }
}
