<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTenantProductionRenameColumnAndMakeCategoryNull extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenant_products', function (Blueprint $table) {
            $table->renameColumn('tenant_production_type', 'tenant_product_type');
            $table->string('tenant_product_category', 50)->nullable()->change();
        });

        Schema::table('tenant_stores', function (Blueprint $table){
            $table->string('store_surface_area')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_products', function (Blueprint $table) {
            $table->renameColumn('tenant_product_type', 'tenant_production_type');
            $table->string('tenant_product_category', 50)->change();
        });

        Schema::table('tenant_stores', function (Blueprint $table){
            $table->string('store_surface_area')->nullable()->change();
        });
    }
}
