<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAlbums extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('albums', function( Blueprint $table){
            $table->increments('id');
            $table->integer('ekraf_tenant_id');
            $table->string('album_name');
            $table->timestamps();
        });

        Schema::create('album_galleries', function( Blueprint $table){
            $table->increments('id');
            $table->integer('gallery_id');
            $table->integer('album_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('albums');
        Schema::dropIfExists('album_galleries');
    }
}
