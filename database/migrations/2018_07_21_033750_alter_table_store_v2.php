<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableStoreV2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenant_stores', function (Blueprint $table) {
            $table->renameColumn('marketing_by_online','marketing_is_online');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_stores', function (Blueprint $table) {
            $table->renameColumn('marketing_is_online','marketing_by_online');
        });
    }
}
