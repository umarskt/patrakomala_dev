<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableEkrafTenantsAddedTenantIntro extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ekraf_tenants', function (Blueprint $table) {
            $table->text('tenant_intro')->nullable();
            $table->text('tenant_mission')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ekraf_tenants', function (Blueprint $table) {
            $table->dropColumn('tenant_intro');
            $table->dropColumn('tenant_mission');
        });
    }
}
