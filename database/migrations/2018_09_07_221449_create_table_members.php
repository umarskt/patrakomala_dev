<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMembers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('hackathon')->create('members', function($table){
            $table->increments('id');
            $table->string('mbr_full_name', 40);
            $table->string('mbr_email')->unique();
            $table->string('mbr_phone',15);
            $table->string('mbr_name_contact_emergency',40)->nullable();
            $table->string('mbr_phone_emergency',15);
            $table->text('mbr_address_emergency')->nullable();
            $table->text('mbr_ill')->nullable();
            $table->unsignedInteger('team_id');
            $table->foreign('team_id')
            ->references('id')->on('teams');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('hackathon')->table('members', function($table){
            $table->dropForeign('members_team_id_foreign');
        });
        Schema::connection('hackathon')->dropIfExists('members');
    }
}
