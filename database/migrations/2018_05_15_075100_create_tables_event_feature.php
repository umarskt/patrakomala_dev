<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablesEventFeature extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //event
        Schema::create('events', function( Blueprint $table ){
            $table->increments('id');
            $table->string('title',60);
            $table->text('description');
            $table->text('take_place');
            $table->time('time');
            $table->timestamps();
            $table->engine = 'InnoDB';
        });

        //event tagging
        Schema::create('event_tagging', function( Blueprint $table ){
            $table->increments('id');
            $table->integer('event_id');
            $table->integer('tag_id');
            $table->timestamps();
            $table->engine = 'InnoDB';
        });

        //tag
        Schema::create('tags', function( Blueprint $table ){
            $table->increments('id');
            $table->string('name', 30);
            $table->timestamps();
            $table->engine = 'InnoDB';
        });

        //event image
        Schema::create('event_images', function( Blueprint $table ){
            $table->increments('id');
            $table->integer('event_id');
            $table->string('img_name', 30);
            $table->string('img_event')->nullable();
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
        Schema::dropIfExists('event_tagging');
        Schema::dropIfExists('tags');
        Schema::dropIfExists('event_images');
    }
}
