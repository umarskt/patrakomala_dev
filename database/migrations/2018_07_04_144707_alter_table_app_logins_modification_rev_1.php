<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableAppLoginsModificationRev1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('app_logins', function (Blueprint $table) {
            $table->string('facebook_id',80)->nullable();
            $table->string('google_id',80)->nullable();
            $table->timestamp('last_login')->nullable();
            $table->string('app_password',80)->nullable()->change();
        });

        Schema::table('activations', function( Blueprint $table ){
            $table->integer('user_id')->nullable()->change();
            $table->integer('app_login_id')->nullable();
            $table->timestamp('expired_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('app_logins', function (Blueprint $table) {
            $table->dropColumn('facebook_id');
            $table->dropColumn('google_id');
            $table->dropColumn('last_login');
        });

        Schema::table('activations', function( Blueprint $table ){
            $table->integer('user_id')->change();
            $table->dropColumn('app_login_id');
            $table->dropColumn('expired_date');
        });
    }
}
