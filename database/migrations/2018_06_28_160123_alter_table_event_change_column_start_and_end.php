<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableEventChangeColumnStartAndEnd extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->renameColumn('time', 'start_time');
            $table->renameColumn('date', 'start_date');
            $table->time('end_time')->nullable();
            $table->date('end_date')->nullable();
        });

        Schema::table('events', function( Blueprint $table){
            $table->time('start_time')->nullable()->change();
            $table->date('start_date')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('events', function( Blueprint $table){
            $table->time('start_time')->change();
            $table->date('start_date')->change();
        });

        Schema::table('events', function (Blueprint $table) {
            $table->renameColumn('start_time', 'time');
            $table->renameColumn('start_date', 'date');
            $table->dropColumn('end_time');
            $table->dropColumn('end_date');
        });
    }
}
