<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTenantProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenant_products', function (Blueprint $table) {
            $table->renameColumn('tenant_product_type','tenant_production_type');
            $table->string('tenant_product_category',15);
            $table->dropColumn('material_product_weight');
            $table->string('material_product_amount',10)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_products', function (Blueprint $table) {
            $table->renameColumn('tenant_production_type','tenant_product_type');
            $table->dropColumn('tenant_product_category');
            $table->float('material_product_weight')->nullable();
            $table->integer('material_product_amount')->nullable();
        });
    }
}
