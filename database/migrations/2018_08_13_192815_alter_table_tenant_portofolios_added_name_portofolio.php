<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTenantPortofoliosAddedNamePortofolio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenant_portofolio_catalogs', function (Blueprint $table) {
            $table->string('name',33)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_portofolio_catalogs', function (Blueprint $table) {
            $table->dropColumn('name');
        });
    }
}
