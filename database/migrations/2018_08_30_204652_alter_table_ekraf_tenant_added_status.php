<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableEkrafTenantAddedStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ekraf_tenants', function( Blueprint $table ){
            $table->dropColumn('start_salary_employee');
            $table->dropColumn('end_salary_employee');
            $table->dropColumn('tenant_intro');
            $table->dropColumn('tenant_mission');
        });

        Schema::create('tenant_claims', function( Blueprint $table ){
            $table->increments('id');
            $table->integer('ekraf_tenant_id');
            $table->integer('app_login_id');
            $table->string('status', 15);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ekraf_tenants', function( Blueprint $table ){
            $table->string('start_salary_employee')->nullable();
            $table->string('end_salary_employee')->nullable();
            $table->text('tenant_intro')->nullable();
            $table->text('tenant_mission')->nullable();
        });

         Schema::dropIfExists('tenant_claims');
    }
}
