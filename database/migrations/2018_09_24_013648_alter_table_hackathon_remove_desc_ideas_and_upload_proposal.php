<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableHackathonRemoveDescIdeasAndUploadProposal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('hackathon')->table('teams', function( $table ){
            $table->dropColumn('desc_ideas');
            $table->dropColumn('proposals');
            $table->string('access_key', 30)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('hackathon')->table('teams', function( $table ){
            $table->text('desc_ideas')->nullable();
            $table->string('proposals')->nullable();
            $table->dropColumn('access_key');
        });
    }
}
