<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableForRelationBeltTenantPackage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_belts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ekraf_tenant_id');
            $table->integer('tourism_belt_id');
        });

        Schema::create('belt_packages', function( Blueprint $table ){
            $table->increments('id');
            $table->integer('tourism_belt_id');
            $table->integer('tourism_package_id');
        });

        Schema::table('tourism_package', function(Blueprint $table){
            $table->dropColumn('tourism_belt_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('tenant_belts');

         Schema::dropIfExists('belt_packages');

         Schema::table('tourism_package', function(Blueprint $table){
            $table->integer('tourism_belt_id')->nullable();
        });
    }
}
