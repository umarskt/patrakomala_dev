<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTenantTableAndAddSomeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ekraf_tenants', function (Blueprint $table){
            $table->string('tenant_npwp', 15)->nullable();
            $table->string('tenant_corporation',40)->nullable();
            $table->integer('tenant_total_employee')->nullable();
            $table->integer('tenant_total_man_employee')->nullable();
            $table->integer('tenant_total_woman_employee')->nullable();
            $table->string('tenant_email')->unique();
            $table->string('tenant_phone');
            $table->string('start_salary_employee')->nullable();
            $table->string('end_salary_employee')->nullable();
            $table->renameColumn('name','tenant_name');
        });

        Schema::table('tenant_social_sites', function ( Blueprint $table ){
            $table->renameColumn('type','social_sites_type');
            $table->renameColumn('url','social_sites_url');
        });

        //create table founder tenant
        Schema::table('tenant_founders', function( Blueprint $table){
            $table->string('founder_last_education')->nullable();
            $table->string('founder_last_education_department')->nullable();
            $table->string('gender',1)->nullable();
            $table->timestamps();
        });

        Schema::table('tenant_address', function( Blueprint $table ){
            $table->dropColumn('building_number');
            $table->dropColumn('state_country');
            $table->decimal('latitude')->default(0)->change();
            $table->decimal('longitude')->default(0)->change();
        });

        //legality docs
        Schema::create('tenant_legality_docs', function( Blueprint $table ){
            $table->increments('id');
            $table->integer('ekraf_tenant_id');
            $table->string('legality_doc_name',33)->nullable();
            $table->string('legality_doc_slug',33)->nullable();
            $table->timestamps();
        });

        //tenant product
        Schema::create('tenant_products', function( Blueprint $table ){
            $table->increments('id');
            $table->integer('ekraf_tenant_id');
            $table->string('tenant_product_name',40)->nullable();
            $table->string('tenant_product_type',33)->nullable();
            $table->char('tenant_product_character',10)->nullable();
            $table->float('material_product_weight')->nullable();
            $table->integer('material_product_amount')->nullable();
            $table->string('product_supplier')->nullable();
            $table->timestamps();
        });

        //tool
        Schema::create('tool_products', function( Blueprint $table ){
            $table->increments('id');
            $table->integer('tenant_product_id');
            $table->integer('machine_qty')->nullable();
            $table->float('machine_capacity')->nullable();
            $table->integer('machine_price')->nullable();
            $table->string('tool_brand_name',33)->nullable();
            $table->boolean('tool_is_copyright')->nullable();
        });

        //Table for marketing areas and way of marketing
        Schema::create('tenant_stores', function( Blueprint $table){
            $table->increments('id');
            $table->integer('ekraf_tenant_id');
            $table->boolean('is_local');
            $table->boolean('is_international');
            $table->string('marketing_destination_country');
            $table->float('marketing_percentage_ekspor');
            $table->boolean('marketing_by_online');
            $table->boolean('marketing_by_offline');
            $table->string('description_marketing', 50)->nullable();
            $table->time('store_open_at')->nullable();
            $table->time('store_close_at')->nullable();
            $table->boolean('is_facility_parking');
            $table->boolean('is_facility_toilet');
            $table->boolean('is_facility_mushola');
            $table->boolean('is_facility_showroom');
            $table->integer('store_surface_area');
            $table->timestamps();
        });

        Schema::create('tenant_development_agencies', function( Blueprint $table ){
            $table->increments('id');
            $table->integer('ekraf_tenant_id');
            $table->string('development_name');
            $table->string('development_format');
            $table->year('development_year');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('tenant_address', function( Blueprint $table ){
            $table->string('building_number',5);
            $table->string('state_country',20);
            $table->decimal('latitude',10,8)->change();
            $table->decimal('longitude',11,8)->change();
        });

         Schema::table('tenant_social_sites', function ( Blueprint $table ){
            $table->renameColumn('social_sites_type','type');
            $table->renameColumn('social_sites_url','url');
        });

        Schema::table('ekraf_tenants', function( Blueprint $table ){
            $table->dropColumn('tenant_npwp');
            $table->dropColumn('tenant_corporation');
            $table->dropColumn('tenant_total_employee');
            $table->dropColumn('tenant_total_man_employee');
            $table->dropColumn('tenant_total_woman_employee');
            $table->dropColumn('start_salary_employee');
            $table->dropColumn('end_salary_employee');
            $table->renameColumn('tenant_name','name');
        });

        Schema::table('tenant_founders', function( Blueprint $table ){
            $table->dropColumn('founder_last_education');
            $table->dropColumn('founder_last_education_department');
            $table->dropColumn('gender');
        });

        Schema::dropIfExists('tenant_legality_docs');
        Schema::dropIfExists('tenant_products');
        Schema::dropIfExists('tool_products');
        Schema::dropIfExists('tenant_stores');
        Schema::dropIfExists('tenant_development_agencies');
    }
}
