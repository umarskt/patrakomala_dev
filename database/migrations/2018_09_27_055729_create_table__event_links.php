<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEventLinks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('link_url_events', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('event_id');
             $table->foreign('event_id')
            ->references('id')->on('events')->onDelete('cascade');
            $table->string('url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('link_url_events', function($table) {
            $table->dropForeign('link_url_events_event_id_foreign');
        });
        Schema::dropIfExists('link_url_events');
    }
}
