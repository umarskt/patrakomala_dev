<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTenantSocialSitesAndTableTenantFounders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenant_social_sites', function( Blueprint $table ){
            $table->string('social_site_type', 20)->change();
        });

        Schema::table('tenant_founders', function( Blueprint $table ){
            $table->string('gender', 11)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_social_sites', function( Blueprint $table ){
            $table->string('social_site_type', 15)->change();
        });

        Schema::table('tenant_founders', function( Blueprint $table ){
            $table->string('gender', 1)->change();
        });
    }
}
