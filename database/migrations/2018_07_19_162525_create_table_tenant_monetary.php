<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTenantMonetary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_monetaries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ekraf_tenant_id');
            $table->integer('monetary_asset')->nullable();
            $table->integer('monetary_daily_omset')->nullable();
            $table->integer('monetary_monthly_omset')->nullable();
            $table->integer('monetary_yearly_omset')->nullable();
            $table->integer('monetary_daily_profit')->nullable();
            $table->integer('monetary_monthly_profit')->nullable();
            $table->integer('monetary_yearly_profit')->nullable();
            $table->string('monetary_access_financing',30)->nullable();
            $table->string('monetary_investment',30)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenant_monetaries');
    }
}
