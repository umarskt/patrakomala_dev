<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTeamsAndMemebers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('hackathon')->create('teams', function($table){
            $table->increments('id');
            $table->string('team_name',40);
            $table->text('desc_ideas');
            $table->string('contact_email', 40)->unique();
            $table->string('contact_phone', 15);
            $table->string('proposals')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('hackathon')->dropIfExists('teams');
    }
}
