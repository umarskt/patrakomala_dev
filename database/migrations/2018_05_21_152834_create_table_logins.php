<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLogins extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_logins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('app_username',50);
            $table->string('app_email', 40);
            $table->string('app_password', 40);
            $table->boolean('is_active');
            $table->timestamps();
            $table->engine = 'InnoDB';
            $table->unique('app_email');
            $table->unique('app_username');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_logins');
    }
}
