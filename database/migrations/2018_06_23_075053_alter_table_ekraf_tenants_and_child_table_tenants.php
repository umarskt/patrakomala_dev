<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableEkrafTenantsAndChildTableTenants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ekraf_tenants', function (Blueprint $table) {
            $table->dropColumn('ceo');
            $table->dropColumn('tenant_npwp');
        });

        Schema::table('tenant_founders', function(Blueprint $table){
            $table->string('founder_npwp', 15)->nullable();
            $table->string('founder_name', 33)->nullable()->change();
            $table->string('founder_email',33)->nullable();
            $table->string('founder_phone',14)->nullable();
            $table->unique('founder_email');
        });

        Schema::table('tenant_legality_docs', function( Blueprint $table ){
            $table->string('tenant_legality_img',90)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ekraf_tenants', function (Blueprint $table) {
            $table->string('ceo', 33)->nullable();
            $table->string('tenant_npwp',15)->nullable();
        });

        Schema::table('tenant_founders', function( Blueprint $table){
            $table->dropColumn('founder_npwp')->nullable();
            $table->dropColumn('founder_email')->nullable();
            $table->dropColumn('founder_phone')->nullable();
        });

        Schema::table('tenant_legality_docs', function( Blueprint $table ){
            $table->string('tenant_legality_img',90)->change();
        });
    }
}
