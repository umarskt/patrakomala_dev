<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablesTenantFeature extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //tenant
        Schema::create('ekraf_tenants', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('app_login_id')->nullable();
            $table->string('name',33);
            $table->string('ceo',30);
            $table->date('founding_year');
            $table->text('about');
            $table->text('additional_info')->nullable();
            $table->boolean('is_active')->default(false);
            $table->string('img_logo')->nullable();
            $table->timestamps();
            $table->engine = 'InnoDB';
        });

        //clients
        Schema::create('tenant_clients', function (Blueprint $table){
            $table->increments('id');
            $table->integer('ekraf_tenant_id');
            $table->string('name');
            $table->string('img_icon');
            $table->timestamps();
            $table->engine = 'InnoDB';
        });

        //services
        Schema::create('tenant_services', function (Blueprint $table){
            $table->increments('id');
            $table->integer('ekraf_tenant_id');
            $table->string('title',50);
            $table->text('description');
            $table->timestamps();
            $table->engine = 'InnoDB';
        });

        //table galleries
        Schema::create('tenant_galleries', function (Blueprint $table){
            $table->increments('id');
            $table->integer('ekraf_tenant_id');
            $table->string('title',50);
            $table->string('img_gallery');
            $table->timestamps();
            $table->engine = 'InnoDB';
        });

        //file tenants
        Schema::create('tenant_additional_files', function (Blueprint $table){
            $table->increments('id');
            $table->integer('ekraf_tenant_id');
            $table->string('filename');
            $table->timestamps();
            $table->engine = 'InnoDB';
        });

        //product catalog
        Schema::create('tenant_portofolio_product_catalog', function (Blueprint $table){
            $table->increments('id');
            $table->integer('ekraf_tenant_id');
            $table->string('img_product');
            $table->timestamps();
            $table->engine = 'InnoDB';
        });

        //social sites
        Schema::create('tenant_social_sites', function( Blueprint $table){
            $table->increments('id');
            $table->integer('ekraf_tenant_id');
            $table->string('type',15);
            $table->string('url');
            $table->timestamps();
            $table->engine = 'InnoDB';
        });

        //founder
        Schema::create('tenant_founders', function( Blueprint $table){
            $table->increments('id');
            $table->integer('ekraf_tenant_id');
            $table->string('founder_name',33);
            $table->engine = 'InnoDB';
        });

        //contact person
        Schema::create('tenant_contact_persons', function( Blueprint $table){
            $table->increments('id');
            $table->integer('ekraf_tenant_id');
            $table->text('detail');
            $table->string('contact_type',15);
            $table->timestamps();
            $table->engine = 'InnoDB';
        });        

        //addreses
        Schema::create('tenant_address', function( Blueprint $table){
            $table->increments('id');
            $table->integer('ekraf_tenant_id');
            $table->text('street');
            $table->string('building_number',5);
            $table->string('state_country',20);
            $table->char('postal_code',7);
            $table->decimal('latitude',10,8);
            $table->decimal('longitude',11,8);
            $table->timestamps();
            $table->engine = 'InnoDB';
        });

        Schema::create('tenant_categories', function( Blueprint $table ){
            $table->increments('id');
            $table->integer('ekraf_tenant_id');
            $table->integer('category_id');
            $table->engine = 'InnoDB';
        });

        Schema::create('categories', function( Blueprint $table ){
            $table->increments('id');
            $table->string('category_name',40);
            $table->text('category_description');
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ekraf_tenants');
        Schema::dropIfExists('tenant_clients');
        Schema::dropIfExists('tenant_services');
        Schema::dropIfExists('tenant_galleries');
        Schema::dropIfExists('tenant_additional_files');
        Schema::dropIfExists('tenant_portofolio_product_catalog');
        Schema::dropIfExists('tenant_social_sites');
        Schema::dropIfExists('tenant_founders');
        Schema::dropIfExists('tenant_contact_persons');
        Schema::dropIfExists('tenant_address');
        Schema::dropIfExists('tenant_categories');
        Schema::dropIfExists('categories');
    }
}
