<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableEvents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->string('event_content_type',5)->nullable();
        });

        Schema::table('event_images', function (Blueprint $table) {
            $table->string('img_name',100)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->dropColumn('event_content_type');
        });

        Schema::table('event_images', function (Blueprint $table) {
            $table->string('img_name',30)->nullable()->change();
        });
    }
}
