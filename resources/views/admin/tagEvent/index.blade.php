@extends('admin.layouts.app')
@section('content')
<div class="box box-default">
	<div class="box-header with-border">
		<i class="fa fa-user"></i>
		<h3 class="box-title">Tag Event Management</h3>
		<div class="box-body">
			@include('admin.main.flash-message')
			<a href="{{route('admin.event.tag.create')}}" class="btn btn-success pull-right"> Create New Tag Event</a>
			<section>
			<table id="datatables" class="table table-bordered table-hover">
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Slug</th>
						<th>Created</th>
						<th>Action</th>
					</tr>
				</thead>
			</table>
		</section>
		</div>
	</div>
</div>
@endsection
@section('scripts')
	{!! Html::script('admin/bower_components/datatables.net/js/jquery.dataTables.min.js') !!}
	{!! Html::script('admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}
	<script type="text/javascript">
		$(function(){
			$('#datatables').DataTable({
				processing : true,
				serverSide : true,
				ajax : "{!! route('admin.tag.datatables') !!}",
				columns : [
					{data : 'id', name : 'id'},
					{data : 'name', name : 'name'},
					{data : 'slug', name : 'slug'},
					{data : 'created_at', name : 'created_at'},
					{data : 'action', name : 'action'}
				]
			});
		});
	</script>
@endsection