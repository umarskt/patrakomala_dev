@extends('admin.layouts.app')
@section('styles')
{!! Html::style('admin/bower_components/timepicker/bootstrap-timepicker.min.css') !!}
@endsection
@section('content')
<div class="box box-default">
	<div class="box-header with border">
		<span><i class="fa fa-users"></i></span>
		<h3 class="box-title">Edit Tags Event</h3>
		<div class="box-body">
			@include('admin.main.flash-message')
			{!! Form::open(['route'=>['admin.event.tag.store'],'method'=>'post','files'=>'true']) !!}
			<div class="row">
				<div class="col-md-8">
					<div class="form-group  {!! hasError($errors,'name') !!}">
						{!! Form::label('Name Tag') !!}
						{!! Form::text('name', $tag->name,['class'=>'form-control','placeholder'=>'input name']) !!}
						<span class="help-block">{!! $errors->first('name') !!}</span>
					</div>
					<div class="form-group  {!! hasError($errors,'slug') !!}">
						<label>Slug</label>
						{!! Form::text('slug', $tag->slug,['class'=>'form-control','placeholder'=>'Slug Name']) !!}
						<span class="help-block">{!! $errors->first('slug') !!}</span>
					</div>
					{!! Form::submit('Update Tag', ['class'=>'btn btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
	$('[name=name]').keyup(function(){
		var get = $(this).val();
		var slug = get.toLowerCase().replace(/[^\w ]+/g,'').replace(/ +/g,'-');
		$('input[name=slug]').val(slug);
	});
</script>
@endsection