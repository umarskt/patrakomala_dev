<!DOCTYPE html>
<html>
    <head>
        {!! Html::meta(null, null, ['charset' => 'UTF-8']) !!}
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>EKRAF | Dashboard</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        {!! Html::style('admin/bower_components/bootstrap/dist/css/bootstrap.min.css') !!}
        {!! Html::style('admin/bower_components/font-awesome/css/font-awesome.min.css') !!}
        {!! Html::style('admin/bower_components/Ionicons/css/ionicons.min.css') !!}
        {!! Html::style('admin/dist/css/AdminLTE.min.css') !!}
        {!! Html::style('admin/dist/css/skins/_all-skins.min.css') !!}
        {!! Html::style('admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') !!}
        {!! Html::style('admin/bower_components/bootstrap-daterangepicker/daterangepicker.css') !!}
        {!! Html::style('admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') !!}
        {!! Html::style('admin/plugins/phone-code/css/intlTelInput.css') !!}
        {!! Html::style('admin/custom.css') !!}
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        @yield('styles')
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <header class="main-header">
            <!-- Logo -->
            
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
              <!-- Sidebar toggle button-->
              <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
              </a>
              <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                  <!-- User Account: style can be found in dropdown.less -->
                  <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        {!! Html::image( 'admin/dist/img/Icon_admin.png', 'User Image', [ 'class' => 'img-circle' ] ) !!} 
                      <span class="hidden-xs">{!! user_info()->first_name !!}</span>
                    </a>
                    <ul class="dropdown-menu">
                      <!-- User image -->
                      <li class="user-header">
                          {!! Html::image( 'admin/dist/img/Icon_admin.png', 'User Image', [ 'class' => 'img-circle' ] ) !!} 
                        <p>
                          {!! user_info()->first_name.' '.user_info()->last_name !!}
                          <small>Member since Nov. 2012</small>
                        </p>
                      </li>
                      <!-- Menu Footer-->
                      <li class="user-footer">
                        <div class="pull-left">
                          <a href="#" class="btn btn-default btn-flat">Profile</a>
                        </div>
                        <div class="pull-right">
                          <a href="{{ route('admin.post.logout') }}" class="btn btn-default btn-flat">Sign out</a>
                        </div>
                      </li>
                    </ul>
                  </li>
                  <!-- Control Sidebar Toggle Button -->
                  <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                  </li>
                </ul>
              </div>
            </nav>
           </header>
           <aside class="main-sidebar">
                <section class="sidebar">
                    @include('admin.layouts.sidebar')
                </section>
            </aside>
            <div class="content-wrapper">
                <section class="content-header">
                    <h1>@yield('page-header')</h1>
                    @yield('breadcrumb')
                </section>
                <section class="content">
                    @yield('content')
                </section>
            </div> <!-- close content wrapper -->
        </div><!-- Close wrapper -->
        <footer class="main-footer">
            <strong>Copyright &copy; {{ date('Y') }}</strong>. All rights reserved.
        </footer>
        {!! Html::script('admin/bower_components/jquery/dist/jquery.min.js') !!}
        {!! Html::script('admin/bower_components/jquery-ui/jquery-ui.min.js') !!}
        {!! Html::script('admin/bower_components/bootstrap/dist/js/bootstrap.min.js') !!}
        {!! Html::script('admin/dist/js/adminlte.min.js') !!}
        {!! Html::script('admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') !!}
        {!! Html::script('admin/bower_components/ckeditor/ckeditor.js') !!}
        {!! Html::script('admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') !!}
        {!! Html::script('admin/plugins/phone-code/js/intlTelInput.min.js') !!}
        {!! Html::script('admin/bower_components/inputmask/dist/min/jquery.inputmask.bundle.min.js') !!}
        {!! Html::script('js/general-helper.js') !!}

        @yield('scripts')
        @yield('scripts_extends')
        <script>
          //datepicker
          $(".datepicker").datepicker({autoclose: true, format: 'dd-mm-yyyy'});
          $(".datepicker").inputmask("dd-mm-yyyy");

          $('.phone').inputmask("9999-9999-9999", { 'placeholder': '0812-3456-7890' });
        </script>
    </body>
</html>
