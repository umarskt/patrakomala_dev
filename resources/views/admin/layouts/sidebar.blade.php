<div class="user-panel">
    {!! Html::image( 'admin/dist/img/Logo.png', 'User Image', [ 'width'=>'180','class'=>'img-responsive' ] ) !!}
</div>
<ul class="sidebar-menu" data-widget="tree">
    <li class="header">NAVIGATION</li>
    <div class="category-content">
        <div class="media">
            <div class="media-body">
                <span class="media-heading text-muted" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden; width: 230px;">
                    <i class="fa fa-user position-left"></i>{!! user_info()->full_name !!}
                </span>
                <span class="media-heading text-muted" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden; width: 230px;">
                    <i class="fa fa-credit-card position-left"></i>{!! user_info()->roles[0]->name !!}
                </span>
            </div>
        </div>
    </div>
    <li>
        <a href="{{ route('admin.dahsboard') }}">
           <i class="fa fa-home"></i><span>Home</span>
        </a>
    </li>
    <li class="treeview">
       <a href="#">
            <i class="fa fa-dashboard"></i> <span>Access</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i>Group Management</a></li>
            <li><a href="{!! route('admin.user.index') !!}"><i class="fa fa-circle-o"></i>User Management</a></li>
        </ul>
    </li>
    <li class="treeview">
       <a href="#">
            <i class="fa fa-dashboard"></i><span>{!! trans('ekraf.tenant') !!}</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="{!! route('admin.tenant.index') !!}"><i class="fa fa-circle-o"></i>Tenant</a></li>
        </ul>
    </li>
    <li class="treeview">
       <a href="#">
            <i class="fa fa-dashboard"></i> <span>{!! trans('ekraf.event') !!}</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="{!! route('admin.event.index') !!}"><i class="fa fa-circle-o"></i>{!! trans('ekraf.event_management') !!}</a></li>
            <li><a href="{!! route('admin.tag.index') !!}"><i class="fa fa-circle-o"></i>{!! trans('ekraf.set_tags_event') !!}</a></li>
        </ul>
    </li>
    <li class="treeview">
       <a href="#">
            <i class="fa fa-dashboard"></i> <span>{!! trans('ekraf.tourism') !!}</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="{!! route('admin.belts.index') !!}"><i class="fa fa-circle-o"></i>{!! trans('ekraf.belts') !!}</a></li>
            <li><a href="{!! route('admin.packages.index') !!}"><i class="fa fa-circle-o"></i>{!! trans('ekraf.tourism_management') !!}</a></li>
            <li><a href="{!! route('admin.itineraries.index') !!}"><i class="fa fa-circle-o"></i>{!! trans('ekraf.itinerary_management') !!}</a></li>
        </ul>
    </li>
    <li>
        <a href="{{ route('admin.hackathons.index') }}">
            <i class="fa fa-calendar"></i>  <span>{!! trans('ekraf.hackathon') !!}</span>
        </a>
    </li>
</ul>