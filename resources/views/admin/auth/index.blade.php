<!DOCTYPE html>
<html>
    <head>
        {!! Html::meta( null, null, ['charset' => 'UTF-8' ] ) !!}
        {!! Html::meta( 'robots', 'noindex, nofollow' ) !!}
        {!! Html::meta( 'product', env( 'APP_WEB_ADMIN_NAME', 'My Tour Web Admin' )) !!}
        {!! Html::meta( 'description', 'MyTours'  ) !!}
        {!! Html::meta( 'author', 'Fikri Hikmatiar' ) !!}
        {!! Html::meta( 'viewport', 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' ) !!}
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Login Page</title>
        {!! Html::style( 'admin/bower_components/bootstrap/dist/css/bootstrap.min.css' ) !!}
        {!! Html::style( 'admin/bower_components/font-awesome/css/font-awesome.min.css' ) !!}
        {!! Html::style( 'admin/dist/css/AdminLTE.min.css' ) !!}
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <a href="../../index2.html"><b>EKRAF</b><br/>Admin Panel</a>
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body">
            {!! Form::open( ['route' => 'admin.post.login' ] ) !!}
                <p class="login-box-msg">Sign in to start your session</p>
                @include('admin.main.flash-message')
                    <div class="form-group form-email {!! hasError($errors,'inp_email') !!} has-feedback">
                        {!! Form::text('inp_email', null, ['class' => 'form-control', 'placeholder' => 'Your email' ]) !!}
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        <span class="help-block">{!! $errors->first('inp_email') !!}</span>
                    </div>
                    <div class="form-group form-password {!! hasError($errors,'password') !!} has-feedback">
                        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Your Password']) !!}
                         <span class="help-block password">{!! $errors->first('password') !!}</span>
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <div class="col-xs-8">
                            <!-- <div class="checkbox icheck">
                                <label>
                                    <input type="checkbox"> Remember Me
                                </label>
                            </div> -->
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                        </div>
                        <!-- /.col -->
                    </div>
                <a href="#">I forgot my password</a><br>
            {!! Form::close() !!}   
            </div>
            <!-- /.login-box-body -->
        </div>
        <!-- /.login-box -->
        <!-- jQuery 3 -->
        {!! Html::script('admin/bower_components/jquery/dist/jquery.min.js') !!}
        {!! Html::script('admin/bower_components/bootstrap/dist/js/bootstrap.min.js') !!}
        {!! Html::script('admin/plugins/iCheck/icheck.min.js') !!}
        
        <!-- iCheck -->
        <script>
        /*check();
        function check(){
            if( {!! $errors->has('email') !!} ){
                $('.form-email').addClass('has-error');
                $('.help-block').html("{!! $errors->first('email') !!}");
            }else if ( {!! $errors->has('password') !!} ){
                $('.form-password').addClass('has-error');
                $('.password').html("{!! $errors->first('password') !!}");
            }
        }*/
        </script>
    </body>
</html>