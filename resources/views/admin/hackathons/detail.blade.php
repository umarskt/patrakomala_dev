@extends('admin.layouts.app')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box box-default">
			<div class="box-header with-border">
				<i class="fa fa-cubes"></i>
				<button class="btn btn-info pull-right">Edit team</button>
				<h3 class="box-title">Tim</h3>
			</div>
			<div class="box-body">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Nama Tim</th>
							<td>{{ $hackathon['team_name'] }}</td>
						</tr>
						<tr>
							<th>Email</th>
							<td>{{ $hackathon['contact_email'] }}</td>
						</tr>
						<tr>
							<th>No Telp Tim</th>
							<td>{{ $hackathon['contact_phone'] }}</td>
						</tr>
						<tr>
							<th>Terdaftar</th>
							<td>{{ $hackathon['registered_at']->toDateString() }}</td>
						</tr>
						<tr>
							<th>Access Key</th>
							@if( !empty($hackathon['access_key']) )
								<td>{{  $hackathon['access_key'] }} <a href="#" id="btn-generate" class="btn btn-info pull-right" id="btn-generate">Re Generate Access Key</a></td>
							@else
								<td>- <a href="#" class="btn btn-success pull-right" id="btn-generate">Generate Access Key</a></td>
							@endif
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="box box-default">
			<div class="box-header with-border">
				<i class="fa fa-user"></i>
				<h3 class="box-title">Anggota Tim</h3>
			</div>
			<div class="box-body">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Nama</th>
							<th>Email</th>
							<th>No Telp</th>
							<th>Kontak Darurat</th>
							<th>Alamat Darurat</th>
						</tr>
					</thead>
					<tbody>
						@foreach($hackathon['members'] as $key => $member)
						<tr>
							<td>{{ $member['mbr_full_name'] }}</td>
							<td>{{ $member['mbr_email'] }}</td>
							<td>{{ $member['mbr_phone'] }}</td>
							<td>{{ $member['mbr_name_contact_emergency'] }}</td>
							<td>{{ $member['mbr_address_emergency'] }}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div> <!-- close col md-12 -->
</div> <!-- close row -->
@endsection
@section('scripts')
<script type="text/javascript">
	$('#btn-generate').click(function(event) {
		/* Act on the event */
		$.ajax({
			url: "{{ route('api.generate.key') }}",
			type: 'POST',
			dataType: 'json',
			data: {
				id: "{{  $hackathon['id'] }}",
				"_token" : "{{ csrf_token() }}"
			},
			success : function(result){
				if( result.status == 'success' ){
                   location.reload();
				}
			}
		})
		.done(function() {
			console.log("success");
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	});
</script>
@endsection