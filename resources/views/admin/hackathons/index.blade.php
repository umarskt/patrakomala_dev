@extends('admin.layouts.app')
@section('content')
<div class="box box-default">
	<div class="box-header with-border">
		<i class="fa fa-user"></i>
		<h3 class="box-title">Hackathon</h3>
		@include('admin.main.flash-message')
		<div class="box-tools">
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-md-2 pull-right">
					
				</div>
				<div class="col-md-12">
					<table id="datatables" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>Nama Tim</th>
								<th>Kontak Email</th>
								<th>No HP Team</th>
								<th>Terdaftar</th>
								<th>Detail</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
	{!! Html::script('admin/bower_components/datatables.net/js/jquery.dataTables.min.js') !!}
	{!! Html::script('admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}
	<script type="text/javascript">
		$(function(){
			$('#datatables').DataTable({
				processing : true,
				serverSide : true,
				ajax : "{!! route('admin.hackathons.datatables') !!}",
				columns : [
					{data : 'team_name', name : 'team_name'},
					{data : 'contact_email', name : 'contact_email'},
					{data : 'contact_phone', name : 'contact_phone'},
					{data : 'created_at', name : 'created_at'},
					{data : 'action', name : 'action'}
				]
			});
		});
	</script>
@endsection