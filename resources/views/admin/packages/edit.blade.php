@extends('admin.layouts.app')
@section('page-header','Form Edit Tourism Package')
@section('styles')
{!! Html::style('admin/bower_components/select2/dist/css/select2.min.css') !!}
<style type="text/css">
	.entry:not(:first-of-type)
{
    margin-top: 10px;
}

.glyphicon
{
    font-size: 12px;
}
</style>
@endsection
@section('content')
<div class="box box-default">
	<div class="box-header with-border">
		<span><i class="fa fa-book"></i></span>
		<h3 class="box-title">Edit Tourism Package</h3>
	</div>
	<div class="box-body">
		@include('admin.main.flash-message')
		{!! Form::open(['route'=>['admin.packages.store'],'method'=>'post']) !!}
		<div class="row">
			<div class="col-md-7">
				<div class="form-group">
					{!! Form::label('Pilih Belt') !!}
					{!! Form::select('selected_belt', $belts, null,['class'=>'form-control select2','data-placeholder'=>'Pilih Belt']) !!}
				</div>
				<div class="form-group {!! hasError($errors,'package_name') !!}">
					{!! Form::label('Package Name') !!}
					{!! Form::text('package_name', null,['class'=>'form-control','placeholder'=>'Input package name']) !!}
					<span class="help-block">{!! $errors->first('belt_title') !!}</span>
				</div>
				<div class="form-group {!! hasError($errors,'package_description') !!}">
					{!! Form::label('Package Desctiption') !!}
					{!! Form::textarea('package_description', null,['class'=>'form-control','placeholder'=>'Description Event','id'=>'editor1']) !!}
				</div>
				<div class="form-group {!! hasError($errors,'selected_tenants') !!}">
 					{!! Form::label('Pilih Tenant') !!}
            		{!! Form::select('selected_tenants[]', $tenants, null,['multiple'=>'multiple','class'=>'form-control select2','data-placeholder'=>'Pilih Tenant']) !!}
            		 <small>Urutan thread dari kiri :)</small>
				</div>
				<div class="form-group {!! hasError($errors,'estimated_cost') !!}">
					{!! Form::label('Estimated Cost') !!}
					<div class="input-group">
						<span class="input-group-addon">Rp.</span>
						{!! Form::text('estimated_cost',null,['class'=>'form-control']) !!}
						<span class="input-group-addon">.00</span>
					</div>
				</div>
				<div class="form-group {!! hasError($errors,'provider') !!}">
					{!! Form::label('Vendor') !!}
					{!! Form::text('provider',null,['class'=>'form-control']) !!}
				</div>
			</div>
		</div>
		{!! Form::submit('Create',['class'=>'btn btn-primary']) !!}
		{!! Form::close() !!}
	</div>
</div>
@endsection
@section('scripts')
{!! Html::script('admin/bower_components/ckeditor/ckeditor.js') !!}
{!! Html::script('admin/bower_components/select2/dist/js/select2.full.min.js') !!}
<script type="text/javascript">
	$('.select2').select2();
	$('#datepicker').datepicker({
		autoclose: true
	});
	$(function () {
	CKEDITOR.replace('editor1')
	});

$(function()
{
    $(document).on('click', '.btn-add', function(e)
    {
        
        var controlForm = $('.controls form:first'),
            currentEntry = $(this).parents('.entry:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .btn-add')
            .removeClass('btn-add').addClass('btn-remove')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="glyphicon glyphicon-minus"></span>');
    }).on('click', '.btn-remove', function(e)
    {
		$(this).parents('.entry:first').remove();

		e.preventDefault();
		return false;
	});
});
</script>
@endsection