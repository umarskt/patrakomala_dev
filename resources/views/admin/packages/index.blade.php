@extends('admin.layouts.app')
@section('page-header','Tourism Management')
@section('content')
<div class="box box-default">
	<div class="box-header with-border">
		<div class="box-title">
			<i class="fa fa-book"></i>
			<h3 class="box-title">List Tourism Package</h3>
		</div>
		<div class="box-body">
			@include('admin.main.flash-message')
			<a href="{{route('admin.packages.create')}}" class="btn btn-success pull-right">Create</a>
			<section>
				<table id="datatables" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>Id</th>
							<th>Package Name</th>
							<th>Estimated Cost</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</section>
		</div>
	</div>
</div>
@endsection
@section('scripts')
	{!! Html::script('admin/bower_components/datatables.net/js/jquery.dataTables.min.js') !!}
	{!! Html::script('admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}
	<script type="text/javascript">
		$(function(){
			$('#datatables').DataTable({
				processing : true,
				serverSide : true,
				ajax : "{!! route('admin.packages.datatables') !!}",
				columns : [
					{data : 'id', name : 'id'},
					{data : 'package_name', name : 'package_name'},
					{data : 'estimated_cost', name : 'estimated_cost'},
					{data : 'action', name : 'action'}
				]
			});
		});
	</script>
@endsection