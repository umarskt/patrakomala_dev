@extends('admin.layouts.app')
@section('content')
<div class="box box-default">
	<div class="box-header with border">
		<span><i class="fa fa-users"></i></span>
		<h3 class="box-title">Detail User</h3>
		<div class="box-body">
			@include('admin.main.flash-message')
			{!! Form::open(['route'=>['admin.user.create'],'method'=>'post']) !!}
			<div class="row">
				<div class="col-md-3">
				<div class="form-group  {!! hasError($errors,'full_name') !!}">
					<label>Full Name</label>
					{!! Form::text('full_name', null,['class'=>'form-control','placeholder'=>'input name']) !!}
					<span class="help-block">{!! $errors->first('full_name') !!}</span>
				</div>
				<div class="form-group {!! hasError($errors,'email') !!}">
					<label>Email</label>
					{!! Form::text('email', null,['class'=>'form-control','placeholder'=>'input email']) !!}
					<span class="help-block">{!! $errors->first('email') !!}</span>
				</div>
				<div class="form-group {!! hasError($errors,'username') !!}">
					<label>Username</label>
					{!! Form::text('username', null,['class'=>'form-control','placeholder'=>'input username']) !!}
					<span class="help-block">{!! $errors->first('username') !!}</span>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group {!! hasError($errors,'password') !!}">
					<label>Password</label>
					{!! Form::password('password', ['class'=>'form-control','placeholder'=>'input password']) !!}
					<span class="help-block">{!! $errors->first('password') !!}</span>
				</div>
				<div class="form-group {!! hasError($errors,'confirm_password') !!}">
					<label>Confirm Password</label>
					{!! Form::password('confirm_password', ['class' => 'form-control', 'placeholder' => 'Input Confirm Password']) !!}
					<span class="help-block">{!! $errors->first('confirm_password') !!}</span>
				</div>
			</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection