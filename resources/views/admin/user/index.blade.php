@extends('admin.layouts.app')
@section('content')
<div class="nav-tabs-custom">
	<ul class="nav nav-tabs">
		<li class="active"><a href="#tab_user" data-toggle="tab">User Admin</a></li>
		<li><a href="#tenant_address" data-toggle="tab">User Tenant</a></li>
		<li><a href="#tenant_address" data-toggle="tab">User Client</a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="tab_user">
			@include('admin.main.flash-message')
			<a href="{{route('admin.user.create')}}" class="btn btn-success pull-right">Create</a>
			<section>
				<table id="datatables" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>ID</th>
							<th>Email</th>
							<th>Full Name</th>
							<th>Username</th>
							<th>last_login</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</section>
		</div>
		<div class="tab-pane" id="tenant_address">
			@include('admin.main.flash-message')
			<a href="{{route('admin.user.create')}}" class="btn btn-success pull-right">Create</a>
			<section>
				<table id="data_user_tenant" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>ID</th>
							<th>Username</th>
							<th>Email</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</section>
		</div>
	</div>
</div>


@endsection
@section('scripts')
	{!! Html::script('admin/bower_components/datatables.net/js/jquery.dataTables.min.js') !!}
	{!! Html::script('admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}
	<script type="text/javascript">
		$(function(){
			$('#datatables').DataTable({
				processing : true,
				serverSide : true,
				ajax : "{!! route('admin.user.datatables') !!}",
				columns : [
					{data : 'id', name : 'id'},
					{data : 'email', name : 'email'},
					{data : 'full_name', name : 'full_name'},
					{data : 'username', name : 'username'},
					{data : 'last_login', name : 'last_login'},
					{data : 'action', name : 'action'}
				]
			});

			$('#data_user_tenant').DataTable({
				processing : true,
				serverSide : true,
				ajax : "{!! route('admin.user.tenant.datatables') !!}",
				columns : [
					{data : 'id', name : 'id'},
					{data : 'app_username', name : 'app_username'},
					{data : 'app_email', name : 'app_email'},
					{data : 'is_active', name : 'is_active'},
					{data : 'action', name : 'action'}
				]
			});
		});
	</script>
@endsection