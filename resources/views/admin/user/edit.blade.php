@extends('admin.layouts.app')
@section('content')
<div class="box box-default">
	<div class="box-header with border">
		<h3 class="box-title">
			<span><i class="fa fa-edit">Edit User</i></span>
		</h3>
		<div class="box-body">
			@include('admin.main.flash-message')
			{!! Form::open(['route'=>['admin.user.update', $user->id],'method'=>'put']) !!}
			<div class="row">
				<div class="col-md-3">
					<div class="form-group {!! hasError($errors,'full_name') !!}">
						<label>Full Name</label>
						{!! Form::text('full_name', $user->full_name,['class'=>'form-control']) !!}
						<span class="help-block">{!! $errors->first('full_name') !!}</span>
					</div>
					<div class="form-group {!! hasError($errors,'email') !!}">
						<label>Email</label>
						{!! Form::text('email', $user->email,['class'=>'form-control']) !!}
						<span class="help-block">{!! $errors->first('email') !!}</span>
					</div>
					<div class="form-group {!! hasError($errors,'username') !!}">
						<label>Username</label>
						{!! Form::text('username', $user->username,['class'=>'form-control']) !!}
						<span class="help-block">{!! $errors->first('username') !!}</span>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group {!! hasError($errors,'password') !!}">
						<label>Password</label>
						{!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Your Password']) !!}
						<span class="help-block">{!! $errors->first('password') !!}</span>
					</div>
				</div>
			</div>
			<button type="submit" class="btn btn-primary">Edit</button>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection