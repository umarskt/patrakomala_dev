@extends('admin.layouts.app')
@section('styles')
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="alert alert-success">
            <b>Welcome to admin page</b>
            'Succes login to Dashboard'
        </div>
    </div>
    @include('admin.main.analytic.page-view')
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-bar-chart-o"></i>

              <h3 class="box-title">Sub sektor Tenant</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div id="bar-chart" style="height: 300px;"></div>
            </div>
            <!-- /.box-body-->
          </div>
    </div>
    <div class="col-md-6">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Job List</h3>
            </div>
            <div class="box-body">
                <table  class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Nama Tenant</th>
                            <th>Email</th>
                            <th>Logo</th>
                            <th>No Telp</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        </div><!-- close md 6 -->
        <div class="col-md-6">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Tenant Project List</h3>
                </div>
                <div class="box-body">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Status</th>
                                <th>Date</th>
                                <th>User</th>
                                <th>Value</th>
                            </tr>
                            <tr>
                                <td>1.</td>
                                <td><span class="badge bg-green">Accepted</span></td>
                                <td>19/06/2018</td>
                                <td>Dhon Joe</td>
                                <td>20%</td>
                            </tr>
                            <tr>
                                <td>2.</td>
                                <td><span class="badge bg-red">rejected</span></td>
                                <td>19/06/2018</td>
                                <td>Damien</td>
                                <td>20%</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            </div><!-- close col md 6 -->
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Registrasi Tenant</h3>
                    </div>
                    <div class="box-body">
                        <table id="datatables" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Nama Tenant</th>
                                    <th>Email</th>
                                    <th>Logo</th>
                                    <th>No Telp</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">Claim Tenant</h3>
                </div>
                <div class="box-body">
                  <table id="datatables-claim-tenant" class="table table-bordered">
                    <thead>
                      <tr>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Claim Tenant</th>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
            </div>
        </div>
        @endsection
        @section('scripts')
        {!! Html::script('admin/bower_components/datatables.net/js/jquery.dataTables.min.js') !!}
        {!! Html::script('admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}
        {!! Html::script('admin/bower_components/Flot/jquery.flot.js') !!}
        {!! Html::script('admin/bower_components/Flot/jquery.flot.resize.js') !!}
        {!! Html::script('admin/bower_components/Flot/jquery.flot.pie.js') !!}
        {!! Html::script('admin/bower_components/Flot/jquery.flot.categories.js') !!}
        <script type="text/javascript">
        $(document).ready(function () {
        var unique_id = $.gritter.add({
        // (string | mandatory) the heading of the notification
        title: "User Info",
        // (string | mandatory) the text inside the notification
        text: "<b>Name</b>&nbsp;&nbsp;{{user_info()->full_name}}</br><b>Email</b>&nbsp;&nbsp;{{user_info()->email}}</br>",
        // (string | optional) the image to display on the left
        /*image: 'assets/img/ui-sam.jpg',*/
        // (bool | optional) if you want it to fade out on its own or just sit there
        sticky: true,
        // (int | optional) the time you want it to be alive for before fading out
        time: '',
        // (string | optional) the class name you want to apply to that specific message
        class_name: 'my-sticky-class'
        });
        return false;
        });
        $('#datatables').DataTable({
            processing : true,
            serverSide : true,
            ajax : "{!! route('tenant.unregistered.datatables') !!}",
            columns : [
                {data : 'tenant_name', name : 'tenant_name'},
                {data : 'tenant_email', name : 'Email'},
                {data : 'img_logo', name : 'Logo'},
                {data : 'tenant_phone', name : 'Phone'},
                {data : 'action', name : 'action'}
            ]
        });

        $('#datatables-claim-tenant').DataTable({
            processing : true,
            serverSide : true,
            ajax : "{!! route('tenant.claimed.datatables') !!}",
            columns : [
                {data : 'app_username', name : 'Username'},
                {data : 'app_email', name : 'Email'},
                {data : 'tenant_name', name : 'Claim Tenant'}
            ]
        });
    $(function (){
    var datas = [];
    /*
     * BAR CHART
     * ---------
     */
     $.ajax({
         url:  "{{ route('tenant.analyze.chart') }}",
         type: 'POST',
         dataType: 'json',
         data: {type: 'tenant-chart', "_token": "{{ csrf_token() }}"},
         success : function(data){
            if(data){
                datas.push();
            }
         }
     })
     .done(function() {
         console.log("success");
     });
    console.log( ); 
    var bar_data = {
      data : [['Periklanan', 10], ['Aplikasi dan games', 8], ['Arsitektur', 4], ['Seni Rupa', 13], ['Kuliner', 17], ['Fesyen', 9], 
              ['Film, Animasi, dan Video', 9], ['Graphic Design', 4], ['Desain Interior ', 4], ['Kriya', 4], ['Musik', 4], ['Seni Pertunjukan',3], ['Fotografi', 4], ['Desain Produk', 4], ['Penerbitan', 4], ['Televisi dan Radio', 4]],
      color: '#3c8dbc'
    }
    $.plot('#bar-chart', [bar_data], {
      grid  : {
        borderWidth: 1,
        borderColor: '#f3f3f3',
        tickColor  : '#f3f3f3'
      },
      series: {
        bars: {
          show    : true,
          barWidth: 0.5,
          align   : 'center'
        }
      },
      xaxis : {
        mode      : 'categories',
        tickLength: 0
      }
    })

  /*
   * Custom Label formatter
   * ----------------------
   */
  function labelFormatter(label, series) {
    return '<div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">'
      + label
      + '<br>'
      + Math.round(series.percent) + '%</div>'
  }
    });  
    </script>
    @endsection