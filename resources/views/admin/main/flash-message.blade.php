@if (Session::has('success'))
<div class="alert alert-success alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<h4><i class="icon fa fa-check"></i>Message</h4>
	{{Session::get('success')}}
</div>
@elseif (Session::has('failed_login'))
<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<h4><i class="icon fa fa-close"></i> Message</h4>
	{{Session::get('failed_login')}}
</div>
@elseif (Session::has('alert-danger'))
<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<h4><i class="icon fa fa-close"></i> Message</h4>
	{{Session::get('alert-danger')}}
</div>
@endif