<div class="col-lg-3 col-xs-6">
	<div class="small-box bg-green">
		<div class="inner">
			<h3>{{ $analytic->hackathon }}</h3>
			Page viewer Hackathon
		</div>
		<div class="icon">
			<i class="ion ion-pie-graph"></i>
		</div>
		<div class="small-box-footer"></div>
	</div>
</div>
<div class="col-lg-3 col-xs-6">
	<div class="small-box bg-red">
		<div class="inner">
			<h3>{{ $analytic->tenant_register }}</h3>
			Viewer Register Tenant
		</div>
		<div class="icon">
			<i class="ion ion-person-add"></i>
		</div>
		<div class="small-box-footer"></div>
	</div>
</div>

