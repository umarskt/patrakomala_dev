@extends('admin.layouts.app')
@section('styles')
{!! Html::style('admin/bower_components/lightbox/css/lightbox.min.css') !!}
@endsection
@section('content')
<div class="row">
	<div class="col-md-12">
		<div id="flash-message"></div>
	</div>
	<div class="col-md-3">
		<div class="content-panel pn">
			<div id="spotify">
			</div>
		</div>
		<div class="box box-default box-solid">
			<div class="box-header with-border">
				<h4 class="box-title">Logo</h4>
			</div>
			<div class="box-body">
				{!! Html::image( $tenant['tenant_logo'] ,'profile tenant',['class'=>'img-responsive','width'=>'250']); !!}
			</div>
		</div>
		<!-- Ekraf Kategori -->
		<div class="box box-default">
			<div class="box-header with-border">
				<span><i class="fa fa-info"></i></span>
				<h5 class="box-title">Kategori Ekraf</h5>
			</div>
			<div class="box-body">
				<div class="row">
					@foreach($tenant['sub_sectors'] as $key => $row)
						<div class="col-md-4">
							<span class="label label-success">{!! $row['sub_sector_name'] !!}</span>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
	@include('admin.tenant.detail-information')
</div>
@endsection
@section('scripts')
{!! Html::script('admin/bower_components/lightbox/js/lightbox.min.js')  !!}
<script type="text/javascript">
	$("#btn-approve").click(function(event) {
		/* Act on the event */
		$.ajax({
			url: "{{ route('admin.tenant.approve') }}",
			type: 'POST',
			dataType: 'json',
			data: {
				id: '{{ $tenant['id'] }}',
				"_token": "{{ csrf_token() }}"
			},
			success : function(result){
				if(result.status){
					$('div#flash-message').html(result.view);
					$("#btn-approve").hide();
				}else{
					$('div#flash-message').append(result.view);
				}
			}
		});
		
	});
	lightbox.option({
'resizeDuration': 200,
'wrapAround': true
})
</script>
@endsection