<div class="box box-default">
	<div class="box-header with border">
		<span><i class="fa fa-info"></i></span>
		<h3 class="box-title">Company Identity</h3>
		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse">
			<i class="fa fa-minus"></i>
			</button>
		</div>
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group  {!! hasError($errors,'name') !!}">
					{!! Form::label('Company Name') !!}
					{!! Form::text('name', $tenant->tenant_name,['class'=>'form-control','placeholder'=>'input name']) !!}
					<span class="help-block">{!! $errors->first('name') !!}</span>
				</div>
				<div class="form-group  {!! hasError($errors,'founding_year') !!}">
					<label>Company Since</label>
					{!! Form::text('founding_year', $tenant->founding_year,['class'=>'form-control','placeholder'=>'Founding Year','id'=>'datepicker']) !!}
					<span class="help-block">{!! $errors->first('founding_year') !!}</span>
				</div>
				<div class="form-group  {!! hasError($errors,'tenant_email') !!}">
					<label>Email</label>
					{!! Form::email('tenant_email', $tenant->tenant_email,['class'=>'form-control','placeholder'=>'Tenant Email']) !!}
					<span class="help-block">{!! $errors->first('tenant_email') !!}</span>
				</div>
				<div class="form-group  {!! hasError($errors,'tenant_phone') !!}">
					<label>Phone</label>
					{!! Form::number('tenant_phone', $tenant->tenant_phone,['class'=>'form-control','placeholder'=>'Phone']) !!}
					<span class="help-block">{!! $errors->first('tenant_phone') !!}</span>
				</div>
			</div>
			<div class="col-md-7">
				<div class="form-group {!! hasError($errors,'street') !!}">
					<label>Street Address</label>
					{!! Form::text('street', $tenant->address->street,['class'=>'form-control']) !!}
					<span class="help-block">{!! $errors->first('street') !!}</span>
				</div>
				<div class="row">
					<div class="col-md-5">
						{!! Form::label('Latitude') !!}
						{!! Form::text('latitude', $tenant->address->latitude,['class'=>'form-control']) !!}
					</div>
					<div class="col-md-5">
						{!! Form::label('Longitude') !!}
						{!! Form::text('longitude', $tenant->address->longitude,['class'=>'form-control']) !!}
					</div>
				</div>
				<div class="form-group">
					<div style="width: 700px; height: 300px;">
						{!! Mapper::render() !!}
					</div>
				</div>
				<div class="form-group">
					<label>Kecamatan</label>
					{!! Form::select('subdistrict', $listSubDistrict, $tenant->address->sub_district->id,
					array('placeholder' => 'Pilih kecamatan...',
					'class' => 'form-control')) !!}
					<span class="help-block">{!! $errors->first('state_country') !!}</span>
				</div>
				<div class="form-group {!! hasError($errors,'postal_code') !!}">
					<label>Postal Code</label>
					{!! Form::text('postal_code', $tenant->address->postal_code, ['class'=>'form-control','min'=>'4']) !!}
					<span class="help-block">{!! $errors->first('postal_code') !!}</span>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="box box-default">
			<div class="box-header with border">
				<span><i class="fa fa-users"></i></span>
				<h3 class="box-title">Company profile</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
					<i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-8">
						<div class="form-group {!! hasError($errors,'') !!}">
							{!! Form::label('Logo Perusahaan') !!}
							{!! Form::file('img_logo', null,['class'=>'form-control','placeholder'=>'logo']) !!}
							<span class="help-block">{!! $errors->first('img_logo') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('Tentang Perusahaan') !!}
							{!! Form::textarea('about', $tenant->about,['class'=>'form-control','id'=>'editor1','rows'=>'1']) !!}
							<span class="help-block">{!! $errors->first('about') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('Pengenalan Perusahaan') !!}
							{!! Form::textarea('tenant_intro', $tenant->tenant_intro,['class'=>'form-control','id'=>'editor-tenant-intro','rows'=>'1']) !!}
							<span class="help-block">{!! $errors->first('tenant_intro') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('Misi Perusahaan') !!}
							{!! Form::textarea('tenant_mission', $tenant->tenant_mission,['class'=>'form-control','id'=>'editor-tenant-mission','rows'=>'1']) !!}
							<span class="help-block">{!! $errors->first('tenant_mission') !!}</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="box box-default">
			<div class="box-header with border">
				<span><i class="fa fa-users"></i></span>
				<h3 class="box-title">Client Tenant</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
					<i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-12">
						<table id="datatables-client" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>Nama Client</th>
									<th>Logo</th>
									<th>Action</th>
								</tr>
							</thead>
						</table>
					</div>
					<div class="col-md-6">
						<div class="increment-form-client">
							<div class="form-group">
								{!! Form::label('Nama Client') !!}
								{!! Form::text('name_client[]',null,['class'=>'form-control']) !!}
							</div>
							<div class="form-group">
								{!! Form::label('Logo') !!}
								{!! Form::file('img_logo_client[]',['class'=>'form-control']) !!}
							</div>
						</div>
						<div class="hide form-control-client clone-form-client">
							<div class="form-group">
								<label>Nama Client</label>
								<div class="input-group">
									<input type="text" name="name_client[]" class="form-control">
									<div class="input-group-addon">
										<a href="#" class="remove_field">Remove</a>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label>Logo</label>
								<input type="file" name="img_logo_client[]" class="form-control">
							</div>
						</div>
						<button class="add_field_button btn btn-primary">Add More Fields</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="box box-default">
			<div class="box-header with border">
				<span><i class="fa fa-users"></i></span>
				<h3 class="box-title">Gallery Tenant</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
					<i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-12">
						<table id="datatables-gallery" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>Image</th>
									<th>Action</th>
								</tr>
							</thead>
						</table>
					</div>
					<div class="col-md-6">
						{!! Form::label('Tambahkan Foto Tenant') !!}
						<div class="input-group control-group increment" >
							<input type="file" name="files_gallery[]" class="form-control">
							<div class="input-group-btn">
								<button class="btn btn-success" type="button"><i class="glyphicon glyphicon-plus"></i>Add</button>
							</div>
						</div>
						<div class="clone hide">
							<div class="control-group input-group" style="margin-top:10px">
								<input type="file" name="files_gallery[]" class="form-control">
								<div class="input-group-btn">
									<button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="box box-default">
			<div class="box-header with border">
				<span><i class="fa fa-users"></i></span>
				<h3 class="box-title">Owner Identity</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
					<i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group {!! hasError($errors,'founder_name') !!}">
							{!! Form::label('Owner Name') !!}
							{!! Form::text('founder_name', $datas['founders']['founder_name'],['class'=>'form-control']) !!}
							<span class="help-block">{!! $errors->first('founder_name') !!}</span>
						</div>
						<div class="form-group {!! hasError($errors,'founder_email') !!}">
							{!! Form::label('Email Owner') !!}
							{!! Form::text('founder_email', $datas['founders']['founder_email'], ['class'=>'form-control']) !!}
							<span class="help-block">{!! $errors->first('founder_email') !!}</span>
						</div>
						<div class="form-group {!! hasError($errors,'founder_phone') !!}">
							{!! Form::label('Phone Owner') !!}
							{!! Form::text('founder_phone', $datas['founders']['founder_phone'], ['class'=>'form-control']) !!}
							<span class="help-block">{!! $errors->first('founder_phone') !!}</span>
						</div>
						<div class="form-group  {!! hasError($errors,'founder_npwp') !!}">
							{!! Form::label('NPWP') !!}
							{!! Form::select('founder_npwp', tenant_list_npwp(), $datas['founders']['founder_npwp'],['class'=>'form-control']) !!}
							<span class="help-block">{!! $errors->first('founder_npwp') !!}</span>
						</div>
						<div class="form-group {!! hasError($errors,'gender') !!}">
							<label>Gender</label>
							{!! Form::select('gender', [null=>'-- Select Gender --','m'=>'Male','f'=>'Female'], $datas['founders']['gender'],['class'=>'form-control']) !!}
							<span class="help-block">{!! $errors->first('gender') !!}</span>
						</div>
						<div class="form-group {!! hasError($errors,'founder_last_education') !!}">
							<label>Founder Last Education</label>
							{!! Form::select('founder_last_education', tenant_founder_education(), $datas['founders']['founder_last_education'], ['class'=>'form-control']) !!}
							<span class="help-block">{!! $errors->first('founder_last_education') !!}</span>
						</div>
						<div class="form-group {!! hasError($errors,'founder_last_education_department') !!}">
							<label>Department</label>
							{!! Form::text('founder_last_education_department', $datas['founders']['founder_last_education_department'],['class'=>'form-control']) !!}
							<span class="help-block">{!! $errors->first('founder_last_education_department') !!}</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="box box-default">
			<div class="box-header with border">
				<span><i class="fa fa-users"></i></span>
				<h3 class="box-title">Legalitas Dokument</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
					<i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-12">
						<table id="datatables-legality" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>Dokumen</th>
									<th>Preview</th>
									<th>Action</th>
								</tr>
							</thead>
						</table>
					</div>
					<div class="col-md-5">
						<div class="repeat">
							<div class="wrapper-increment">
								<div class="container">
									<div class="template row">
										<div class="col-md-5">
											<div class="form-group {!! hasError($errors,'legality_doc_name') !!}">
												{!! Form::label('Doc Type') !!}
												<div class="input-group">
													{!!  Form::select('legality_doc_name',['Sertifikat Pendirian','NPWP','SIUP','SKU'],null,['class'=>'form-control']) !!}
													<span class="input-group-btn">
														<a href="javascript:void(0)" class="btn btn-danger remove">Remove</a>
													</span>
													<span class="help-block">{!! $errors->first('legality_doc_name') !!}</span>
												</div>
											</div>
											<div class="form-group {!! hasError($errors,'tenant_legality_img') !!}">
												{!! Form::label('File') !!}
												{!! Form::file('tenant_legality_img', null,['class'=>'form-control','placeholder'=>'logo']) !!}
												<span class="help-block">{!! $errors->first('tenant_legality_img') !!}</span>
											</div>
										</div>
									</div>
								</div>
								<a class="btn btn-default add" href="javascript:void(0)"><i class="fa fa-plus">Add More</i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="box box-default">
			<div class="box-header with border">
				<span><i class="fa fa-users"></i></span>
				<h3 class="box-title">Jenis Usaha</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
					<i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-8">
						{!! Form::label('Pilih Jenis usaha') !!}
						{!! Form::select('selected_tenants[]', $SubSectors, $datas['sub_sectors'],['multiple'=>'multiple','class'=>'form-control select2','data-placeholder'=>'Pilih Tenant']) !!}
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="box box-default">
			<div class="box-header with border">
				<span><i class="fa fa-users"></i></span>
				<h3 class="box-title">Produk</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
					<i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-5">
						<h3 class="lead">Product</h3>
						<div class="form-group {!! hasError($errors,'tenant_product_name') !!}">
							{!! Form::label('Product') !!}
							{!! Form::select('tenant_product_name', ['service'=>'Service','product'=>'Product'], $datas['products']['tenant_product_name'], ['class'=>'form-control']) !!}
							<span class="help-block">{!! $errors->first('tenant_product_name') !!}</span>
						</div>
						<div class="form-group  {!! hasError($errors,'tenant_production_type') !!}">
							{!! Form::label('Production type') !!}
							<div class="row">
								<div class="col-md-4">
									{!! Form::label('Continues') !!}
									{!! Form::radio('tenant_production_type','continues', 
									($datas['products']['tenant_product_type'] == 'continues') ? true : false  ) !!}
								</div>
								<div class="col-md-4">
									{!! Form::label('Intermitten') !!}
									{!! Form::radio('tenant_production_type','intermitten', 
									($datas['products']['tenant_product_type'] == 'intermitten') ? true : false ) !!}
								</div>
								<div class="col-md-4">
									{!! Form::label('Project') !!}
									{!! Form::radio('tenant_production_type','project', 
									($datas['products']['tenant_product_type'] == 'project') ? true : false ) !!}
								</div>
								<span class="help-block">{!! $errors->first('tenant_production_type') !!}</span>
							</div>
						</div>
						<div class="form-group">
							{!! Form::label('Product Character') !!}
							<div class="row">
								<div class="col-md-4">
									{!! Form::label('Inovasi') !!}
									{!! Form::radio('tenant_product_character','inovation',
									($datas['products']['tenant_product_character'] == 'inovation') ? true : false ) !!}
								</div>
								<div class="col-md-4">
									{!! Form::label('Imitasi') !!}
									{!! Form::radio('tenant_product_character','imitasi',
									($datas['products']['tenant_product_character'] == 'imitasi') ? true : false ) !!}
								</div>
							</div>
						</div>
						<h3 class="lead">Product Quantitiy</h3>
						<div class="row">
							<div class="col-md-5">
								{!! Form::label('Quantity') !!}
								<div class="form-group {!! hasError($errors,'tenant_product_quantity') !!}">
									{!! Form::text('tenant_product_quantity', $datas['products']['tenant_product_quantity'],['class'=>'form-control','size'=>'1']) !!}
								</div>
								<span class="help-block">{!! $errors->first('material_product_amount') !!}</span>
							</div>
							<div class="col-md-3">
								{!! Form::label('Unit') !!}
								<div class="form-group {!! hasError($errors,'unit') !!}">
									{!! Form::select('unit',
									[ null ,'ton'=>'Ton','kg'=>'Kg','buah'=>'Buah','set'=>'Set'], $datas['products']['tenant_product_unit'], ['class'=>'form-control']) !!}
								</div>
								<span class="help-block">{!! $errors->first('unit') !!}</span>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<h3 class="lead">Production Equipment</h3>
						<div class="form-group {!! hasError($errors,'tool_brand_name') !!}">
							{!! Form::label('Name') !!}
							{!! Form::text('tool_brand_name',$datas['tools']['tool_brand_name'],['class'=>'form-control']) !!}
							<span class="help-block">{!! $errors->first('tool_brand_name') !!}</span>
						</div>
						<div class="form-group {!! hasError($errors,'tool_quantity') !!}">
							{!! Form::label('Quantitiy') !!}
							{!! Form::text('tool_quantity', $datas['tools']['tool_quantity'], ['class'=>'form-control']) !!}
						</div>
						<div class="form-group {!! hasError($errors,'tool_capacity') !!}">
							{!! Form::label('Capacity') !!}
							{!! Form::text('tool_capacity', $datas['tools']['tool_capacity'], ['class'=>'form-control']) !!}
							<span class="help-block">{!! $errors->first('tool_capacity') !!}</span>
						</div>
						<div class="form-group {!! hasError($errors,'machine_price') !!}">
							{!! Form::label('Price') !!}
							{!! Form::text('tool_price', $datas['tools']['tool_price'], ['class'=>'form-control']) !!}
							<span class="help-block">{!! $errors->first('tool_price') !!}</span>
						</div>
						<div class="form-group {!! hasError($errors,'tool_is_copyright') !!}">
							{!! Form::label('HKI') !!}
							{!! Form::select('tool_is_copyright', [true => 'Ada',false=>'Tidak Ada'], $datas['tools']['tool_is_copyright'],['class'=>'form-control']) !!}
							<span class="help-block">{!! $errors->first('tool_is_copyright') !!}</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="box box-default">
			<div class="box-header with border">
				<span><i class="fa fa-users"></i></span>
				<h3 class="box-title">Marketing and Operational</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
					<i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-5">
						<h3 class="lead">Marketing</h3>
						<div class="form-group {!! hasError($errors,'marketing_type') !!}">
							{!! Form::label('Tipe Marketing') !!}
							{!! Form::select('marketing_type', ['true'=>'Online','false'=>'Offline'], $datas['stores']['marketing_is_online'], ['class'=>'form-control']) !!}
							<span class="help-block">{!! $errors->first('social_site_type') !!}</span>
						</div>
						<div class="form-group {!! hasError($errors,'social_site_type') !!}">
							{!! Form::label('Type Social Sites') !!}
							{!! Form::select('social_site_type',['facebook'=>'Facebook','twitter'=>'Twitter','instagram'=>'Instagram','website'=>'Website','bbm'=>'BBM','commerce'=>'Commerce'],null,['class'=>'form-control']) !!}
							<span class="help-block">{!! $errors->first('social_site_type') !!}</span>
						</div>
						<div class="form-group {!! hasError($errors,'social_site_type') !!}">
							{!! Form::label('URL') !!}
							{!! Form::text('social_site_url',null,['class'=>'form-control']) !!}
							<span class="help-block">{!! $errors->first('social_site_url') !!}</span>
						</div>
					</div>
					<div class="col-md-5">
						<div class="bootstrap-timepicker">
							<h3 class="lead">Operational</h3>
							<div class="form-group">
								{!! Form::label('Store Opened Hour') !!}
								<div class="input-group">
									{!! Form::text('store_open_at', $datas['stores']['store_open_at'], ['class'=>'form-control timepicker']) !!}
									<div class="input-group-addon">
										<i class="fa fa-clock-o"></i>
									</div>
								</div>
							</div>
						</div>
						<div class="bootstrap-timepicker">
							<div class="form-group">
								{!! Form::label('Store Closed Hour') !!}
								<div class="input-group">
									{!! Form::text('store_close_at', $datas['stores']['store_close_at'], ['class'=>'form-control timepicker']) !!}
									<div class="input-group-addon">
										<i class="fa fa-clock-o"></i>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							{!! Form::label('Facilities') !!}
							<div class="row">
								<div class="col-md-6">
									{!! Form::label('Mushola Facility') !!}
									{!! Form::checkbox('is_facility_mushola', true, $datas['stores']['is_facility_mushola']) !!}
								</div>
								<div class="col-md-6">
									{!! Form::label('Toilet Facility') !!}
									{!! Form::checkbox('is_facility_toilet', true, $datas['stores']['is_facility_toilet']) !!}
								</div>
								<div class="col-md-6">
									{!! Form::label('Parking Facility') !!}
									{!! Form::checkbox('is_facility_parking', true, $datas['stores']['is_facility_parking']) !!}
								</div>
								<div class="col-md-6">
									{!! Form::label('Showroom Facility') !!}
									{!! Form::checkbox('is_facility_showroom', true, $datas['stores']['is_facility_showroom']) !!}
								</div>
							</div>
						</div>
						<div class="form-group">
							{!! Form::label('Surface Area') !!}
							<div class="input-group">
								{!! Form::text('store_surface_area', $datas['stores']['store_surface_area'], ['class'=>'form-control']) !!}
								<div class="input-group-addon">
									m2.
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="box box-default">
			<div class="box-header with border">
				<span><i class="fa fa-users"></i></span>
				<h3 class="box-title">SDM</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool">
					<i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-5">
						<h3 class="lead">Karyawan</h3>
						<div class="form-group">
							{!! Form::label('Total Karyawan Laki-laki') !!}
							{!! Form::text('employee_male_total', $datas['employees']['employee_male_total'],['class'=>'form-control']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('Total Karyawan Perempuan') !!}
							{!! Form::text('employee_female_total', $datas['employees']['employee_female_total'],['class'=>'form-control']) !!}
						</div>
						{!! Form::label('Rata - rata gaji karyawan') !!}
						<div class="form-group">
							<div class="col-md-4">
								<div class="radio">
									<label>
										{!! Form::radio('employee_general_fee', '≤ 1jt',
										($datas['employees']['employee_general_fee'] == '≤ 1jt') ? true : false ) !!}
										≤ 1jt
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="radio">
									<label>
										{!! Form::radio('employee_general_fee', '1.1 - 1.5 jt', 
										($datas['employees']['employee_general_fee'] == '1.1 - 1.5 jt') ? true : false ) !!}
										1.1 - 1.5 jt
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="radio">
									<label>
										{!! Form::radio('employee_general_fee', '1.6 - 2 jt',
										($datas['employees']['employee_general_fee'] == '1.6 - 2 jt') ? true : false ) !!}
										1.6 - 2 jt
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="radio">
									<label>
										{!! Form::radio('employee_general_fee', '2.1 - 2.5 jt',
										($datas['employees']['employee_general_fee'] == '2.1 - 2.5 jt') ? true : false ) !!}
										 2.1 - 2.5 jt
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="radio">
									<label>
										{!! Form::radio('employee_general_fee', '2.6 - 3 jt',
										($datas['employees']['employee_general_fee'] == '2.6 - 3 jt') ? true : false ) !!}
										 2.6 - 3 jt
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="radio">
									<label>
										{!! Form::radio('employee_general_fee', '2.6 - 3 jt',
										($datas['employees']['employee_general_fee'] == '2.6 - 3 jt') ? true : false ) !!}
										 2.6 - 3 jt
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="radio">
									<label>
										{!! Form::radio('employee_general_fee', '3 jt',
										($datas['employees']['employee_general_fee'] == '2.6 - 3 jt') ? true : false ) !!}
										 3 jt
									</label>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<h3 class="lead">Latar Belakang Pendidikan Karyawan</h3>
						<div class="form-group">
							{!! Form::label('Total Karyawan lulusan SD') !!}
							{!! Form::text('employee_elementary_school_total', $datas['employees']['employee_elementary_school_total'],['class'=>'form-control']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('Total Karyawan lulusan SMP') !!}
							{!! Form::text('employee_junior_high_total', $datas['employees']['employee_junior_high_total'],['class'=>'form-control']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('Total Karyawan lulusan SMA/SMK') !!}
							{!! Form::text('employee_high_total', $datas['employees']['employee_high_total'],['class'=>'form-control']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('Total Karyawan lulusan D1') !!}
							{!! Form::text('employee_d1_total', $datas['employees']['employee_d1_total'],['class'=>'form-control']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('Total Karyawan lulusan D2') !!}
							{!! Form::text('employee_d2_total', $datas['employees']['employee_d2_total'],['class'=>'form-control']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('Total Karyawan lulusan D3') !!}
							{!! Form::text('employee_d3_total', $datas['employees']['employee_d3_total'],['class'=>'form-control']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('Total Karyawan lulusan Sarjana') !!}
							{!! Form::text('employee_bacheloor_degree_total', $datas['employees']['employee_bacheloor_degree_total'],['class'=>'form-control']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('Total Karyawan lulusan Pasca Sarjana') !!}
							{!! Form::text('employee_post_graduate_total', $datas['employees']['employee_post_graduate_total'],['class'=>'form-control']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('Total Karyawan lulusan Doktoral') !!}
							{!! Form::text('employee_doctoral_degree_total', $datas['employees']['employee_doctoral_degree_total'],['class'=>'form-control']) !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="box box-default">
			<div class="box-header with border">
				<span><i class="fa fa-users"></i></span>
				<h3 class="box-title">Keuangan</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
					<i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-5">
						<div class="form-group">
							{!! Form::label('Asset') !!}
							<div class="input-group">
								<span class="input-group-addon">Rp.</span>
								{!! Form::text('monetary_asset', $datas['monetaries']['monetary_asset'],['class'=>'form-control']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label('Daily Omset') !!}
							<div class="input-group">
								<span class="input-group-addon">Rp.</span>
								{!! Form::text('monetary_daily_omset', $datas['monetaries']['monetary_daily_omset'],['class'=>'form-control']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label('Monthly Omset') !!}
							<div class="input-group">
								<span class="input-group-addon">Rp.</span>
								{!! Form::text('monetary_monthly_omset', $datas['monetaries']['monetary_monthly_omset'],['class'=>'form-control']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label('Yearly Omset') !!}
							<div class="input-group">
								<span class="input-group-addon">Rp.</span>
								{!! Form::text('monetary_yearly_omset', $datas['monetaries']['monetary_yearly_omset'],['class'=>'form-control']) !!}
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							{!! Form::label('Daily Profit') !!}
							<div class="input-group">
								<span class="input-group-addon">Rp.</span>
								{!! Form::text('monetary_daily_profit', $datas['monetaries']['monetary_daily_profit'],['class'=>'form-control']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label('Monthly Profit') !!}
							<div class="input-group">
								<span class="input-group-addon">Rp.</span>
								{!! Form::text('monetary_monthly_profit', $datas['monetaries']['monetary_monthly_profit'],['class'=>'form-control']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label('Yearly Profit') !!}
							<div class="input-group">
								<span class="input-group-addon">Rp.</span>
								{!! Form::text('monetary_yearly_profit', $datas['monetaries']['monetary_yearly_profit'],['class'=>'form-control']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label('Akses Pembiayaan') !!}
							<div class="input-group">
								<label>
									{!! Form::radio('monetary_access_financing', 'easy',
									( $datas['monetaries']['monetary_access_financing'] ==  'easy' ) ? true : false ) !!}
									Mudah
								</label>
								<label>
									{!! Form::radio('monetary_access_financing', 'moderate',
									( $datas['monetaries']['monetary_access_financing'] ==  'moderate') ? true : false ) !!}
									Sedang
								</label>
								<label>
									{!! Form::radio('monetary_access_financing', 'difficult',
									( $datas['monetaries']['monetary_access_financing'] ==  'difficult') ? true : false ) !!}
									Sulit
								</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="box box-default">
			<div class="box-header with border">
				<span><i class="fa fa-users"></i></span>
				<h3 class="box-title">Pelatihan Event</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
					<i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('Nama Pelatih') !!}
							{!! Form::text('coaching[]', null,['class'=>'form-control']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('Tipe Pelatihan') !!}
							{!! Form::text('coaching_type[]',null,['class'=>'form-control']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('Tahun Pelatihan') !!}
							{!! Form::text('coaching_year[]', null, ['class'=>'form-control']) !!}
						</div>
					</div>
				</div>
				<div class="repeat-form-event">
					<div class="wrapper-increment">
						<div class="container">
							<div class="template row">
								<div class="col-md-6">
									<div class="form-group">
										{!! Form::label('Nama Pelatih') !!}
										<div class="input-group">
											{!! Form::text('coaching[]', null,['class'=>'form-control']) !!}
											<span class="input-group-btn">
												<a href="javascript:void(0)" class="btn btn-danger remove">Remove</a>
											</span>
										</div>
									</div>
									<div class="form-group">
										{!! Form::label('Tipe Pelatihan') !!}
										{!! Form::text('coaching_type[]',null,['class'=>'form-control']) !!}
									</div>
									<div class="form-group">
										{!! Form::label('Tahun Pelatihan') !!}
										{!! Form::text('coaching_year[]', null, ['class'=>'form-control']) !!}
									</div>
								</div>
							</div> <!-- template row -->
						</div> <!-- container -->
						<a class="btn btn-default add" href="javascript:void(0)"><i class="fa fa-plus"></i>  Add More</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="box box-default">
			<div class="box-header with border">
				<span><i class="fa fa-users"></i></span>
				<h3 class="box-title">Keanggotaan Komunitas</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
					<i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('Nama Komunitas') !!}
							{!! Form::text('community_name[]',null,['class'=>'form-control'])  !!}
						</div>
					</div>
				</div>
				<div class="repeat-form-community">
					<div class="wrapper-increment">
						<div class="container">
							<div class="template row">
								<div class="col-md-6">
									<div class="form-group">
										{!! Form::label('Nama Komunitas') !!}
										<div class="input-group">
											{!! Form::text('community_name[]',null,['class'=>'form-control'])  !!}
											<span class="input-group-btn">
												<a href="javascript:void(0)" class="btn btn-danger remove">Remove</a>
											</span>
										</div>
									</div>
								</div>
							</div> <!-- template row -->
						</div> <!-- container -->
						<a class="btn btn-default add" href="javascript:void(0)"><i class="fa fa-plus"></i> Add More</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Bussiness -->
	<div class="col-md-12">
		<div class="box box-default">
			<div class="box-header with border">
				<span><i class="fa fa-users"></i></span>
				<h3 class="box-title">Kelebihan Bisnis</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
					<i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-8">
						<div class="form-group">
							{!! Form::label('Deskripsi kelebihan bisnis') !!}
							{!! Form::text('business_strength[]', null, ['class'=>'form-control'])  !!}
						</div>
					</div>
				</div>
				<div class="repeat-form-bussiness-strength">
					<div class="wrapper-increment">
						<div class="container">
							<div class="template row">
								<div class="col-md-6">
									<div class="form-group">
										{!! Form::label('Deskripsi kelebihan bisnis') !!}
										<div class="input-group">
											{!! Form::text('business_strength[]', null, ['class'=>'form-control'])  !!}
											<span class="input-group-btn">
												<a href="javascript:void(0)" class="btn btn-danger remove">Remove</a>
											</span>
										</div>
									</div>
								</div>
							</div> <!-- template row -->
						</div> <!-- container -->
						<a class="btn btn-default add" href="javascript:void(0)"><i class="fa fa-plus"></i> Add More</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="box box-default">
			<div class="box-header with border">
				<span><i class="fa fa-users"></i></span>
				<h3 class="box-title">Peluang Bisnis</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
					<i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-8">
						<div class="form-group">
							{!! Form::label('Peluang bisnis yang di dapat') !!}
							{!! Form::text('business_opportunities[]', null, ['class'=>'form-control'])  !!}
						</div>
					</div>
				</div>
				<div class="repeat-form-opportunities">
					<div class="wrapper-increment">
						<div class="container">
							<div class="template row">
								<div class="col-md-6">
									<div class="form-group">
										{!! Form::label('Deskripsi kelebihan bisnis') !!}
										<div class="input-group">
											{!! Form::text('business_opportunities[]', null, ['class'=>'form-control'])  !!}
											<span class="input-group-btn">
												<a href="javascript:void(0)" class="btn btn-danger remove">Remove</a>
											</span>
										</div>
									</div>
								</div>
							</div> <!-- template row -->
						</div> <!-- container -->
						<a class="btn btn-default add" href="javascript:void(0)"><i class="fa fa-plus"></i> Add More</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="box box-default">
			<div class="box-header with border">
				<span><i class="fa fa-users"></i></span>
				<h3 class="box-title">Itineraries</h3>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-8">
						<div class="form-group">
							{!! Form::label('Penambahan informasi (Itineraries)') !!}
							{!! Form::text('itinerary_title[]', null, ['class'=>'form-control']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('Deskripsi Informasi') !!}
							{!! Form::textarea('itineraries_description[]', null, ['class'=>'form-control']) !!}
						</div>
					</div>
				</div>
				<div class="repeat-form-itineraries">
					<div class="wrapper-increment">
						<div class="container">
							<div class="template row">
								<div class="col-md-6">
									<div class="form-group">
										{!! Form::label('Penambahan informasi (Itineraries)') !!}
										<div class="input-group">
											{!! Form::text('itinerary_title[]', null, ['class'=>'form-control']) !!}
											<span class="input-group-btn">
												<a href="javascript:void(0)" class="btn btn-danger remove">Remove</a>
											</span>
										</div>
									</div>
									<div class="form-group">
										{!! Form::label('Deskripsi Informasi') !!}
										{!! Form::textarea('itineraries_description[]', null, ['class'=>'form-control']) !!}
									</div>
								</div>
							</div> <!-- template row -->
						</div> <!-- container -->
						<a class="btn btn-default add" href="javascript:void(0)"><i class="fa fa-plus"></i> Add More</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!-- close row -->