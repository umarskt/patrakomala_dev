@extends('admin.layouts.app')
@section('page-header','Edit Tenant')
@section('styles')
{!! Html::style('admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') !!}
{!! Html::style('admin/bower_components/select2/dist/css/select2.min.css') !!}
{!! Html::style('admin/plugins/timepicker/bootstrap-timepicker.min.css') !!}
@endsection
@section('content')
{!! Form::open(['route'=>['admin.tenant.update', $tenant->id],'method'=>'put','files'=>'true']) !!}
@include('admin.main.flash-message')
@include('admin.tenant.form-edit')
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="pull-left">
			<a class="btn btn-default" onclick="cancel()">Cancel</a>
		</div>
		<div class="pull-right">
			<button type="submit" class="btn btn-primary">Update</button>
		</div>
	</div>
</div>
{!! Form::close() !!}
@endsection
@section('scripts')
{!! Html::script('admin/bower_components/datatables.net/js/jquery.dataTables.min.js') !!}
{!! Html::script('admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}
{!! Html::script('admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') !!}
{!! Html::script('admin/bower_components/ckeditor/ckeditor.js') !!}
{!! Html::script('admin/bower_components/select2/dist/js/select2.full.min.js') !!}
{!! Html::script('admin/plugins/timepicker/bootstrap-timepicker.min.js') !!}
{!! Html::script('admin/dist/js/repeatable-fields.js') !!}

<script type="text/javascript">
	$('[name=user_id]').change(function(){
		var selected = $('[name=user_id] :selected').text();
		$('[name=ceo]').val(selected);
	});
	$(function () {
		CKEDITOR.replace('editor1')
		CKEDITOR.replace('editor-tenant-intro')
		CKEDITOR.replace('editor-tenant-mission')
		//select 2
		$('.select2').select2()
	
		$('#datepicker').datepicker({
			autoclose: true
		})

		//Timepicker
	    $('.timepicker').timepicker({
	      showInputs: false
	    })
	    
	    //datatables 
	    $('#datatables-gallery').DataTable({
				processing : true,
				serverSide : true,
				ajax : "{!! route('admin.tenant.datatables.galleries', $tenant->id) !!}",
				columns : [
					{data : 'img_gallery', name : 'img_gallery'},
					{data : 'action', name : 'action'}
				]
		});

		$('#datatables-client').DataTable({
				processing : true,
				serverSide : true,
				ajax : "{!! route('admin.tenant.datatables.clients', $tenant->id) !!}",
				columns : [
					{data : 'name', name : 'name'},
					{data : 'img_icon', name : 'Logo'},
					{data : 'action', name : 'action'}
				]
		});

		$('#datatables-legality').DataTable({
				processing : true,
				serverSide : true,
				ajax : "{!! route('admin.tenant.datatables.legalities', $tenant->id) !!}",
				columns : [
					{data : 'legality_doc_name', name : 'legality_doc_name'},
					{data : 'tenant_legality_img', name : 'tenant_legality_img'},
					{data : 'action', name : 'action'}
				]
		});

	});


	//add tool
	$(document).ready( function() {
		$("#btnAddTool").click(function(){
			var html = $(".row-tool").html();
			$("#row-append-tool").append(html);
		});
	});

	$(document).ready(function() {

      $(".btn-success").click(function(){ 
          var html = $(".clone").html();
          $(".increment").after(html);
      });

      $("body").on("click",".btn-danger",function(){ 
          $(this).parents(".control-group").remove();
      });

      	//form dynamic
       	var max_fields      = 10; //maximum input boxes allowed
	    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
	    var add_button      = $(".add_field_button"); //Add button ID
	    
	    var x = 1; //initlal text box count
	    $(add_button).click(function(e){ //on add input button click
	        e.preventDefault();
	        if(x < max_fields){ //max input box allowed
	            x++; //text box increment
	            var html = $(".clone-form-client").html();
          		$(".increment-form-client").after(html);
	        }
	    });
	    
	    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
	        e.preventDefault(); $(this).parents('.form-control-client').remove(); x--;
	    }) 

    });

    $(function(){
    	$('.repeat').each(function() {
    		$(this).repeatable_fields();
    	});

    	$('.repeat-form-event').each(function() {
    		$(this).repeatable_fields();
    	});

    	$('.repeat-form-community').each(function() {
    		$(this).repeatable_fields();
    	});

    	$('.repeat-form-bussiness-strength').each(function() {
    		$(this).repeatable_fields();
    	});

    	$('.repeat-form-opportunities').each(function() {
    		$(this).repeatable_fields();
    	});

    	$('.repeat-form-itineraries').each(function() {
    		$(this).repeatable_fields();
    	});
    })
</script>
@endsection