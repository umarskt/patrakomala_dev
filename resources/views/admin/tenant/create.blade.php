@extends('admin.layouts.app')
@section('styles')
{!! Html::style('admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') !!}
{!! Html::style('admin/bower_components/select2/dist/css/select2.min.css') !!}
{!! Html::style('admin/plugins/timepicker/bootstrap-timepicker.min.css') !!}

@endsection
@section('page-header','Create Tenant')
@section('content')
{!! Form::open(['route'=>['admin.tenant.store'],'method'=>'post','files'=>'true']) !!}
@include('admin.main.flash-message')
@include('admin.tenant.form')
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="pull-left">
			<a class="btn btn-default" onclick="cancel()">Cancel</a>
		</div>
		<div class="pull-right">
			<button type="submit" class="btn btn-primary">Submit</button>
		</div>
	</div>
</div>
{!! Form::close() !!}
@endsection
@section('scripts')

{!! Html::script('admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') !!}
{!! Html::script('admin/bower_components/ckeditor/ckeditor.js') !!}
{!! Html::script('admin/bower_components/select2/dist/js/select2.full.min.js') !!}
{!! Html::script('admin/plugins/timepicker/bootstrap-timepicker.min.js') !!}
<script type="text/javascript">
	$('[name=user_id]').change(function(){
		var selected = $('[name=user_id] :selected').text();
		$('[name=ceo]').val(selected);
	});
	$(function () {
		CKEDITOR.replace('editor1')
		CKEDITOR.replace('editor-tenant-intro')
		CKEDITOR.replace('editor-tenant-mission')
		//select 2
		$('.select2').select2()
	
		$('#datepicker').datepicker({
			autoclose: true
		})

		//Timepicker
	    $('.timepicker').timepicker({
	      showInputs: false
	    })

	});


	//add tool
	$(document).ready( function() {
		$("#btnAddTool").click(function(){
			var html = $(".row-tool").html();
			$("#row-append-tool").append(html);
		});
	});


	$(document).ready(function() {

      $(".btn-success").click(function(){ 
          var html = $(".clone").html();
          $(".increment").after(html);
      });

      $("body").on("click",".btn-danger",function(){ 
          $(this).parents(".control-group").remove();
      });


      	//form dynamic
       	var max_fields      = 3; //maximum input boxes allowed
	    var wrapper         = $(".increment-form-client"); //Fields wrapper
	    var add_button      = $(".add_field_button"); //Add button ID
	    
	    var x = 1; //initlal text box count
	    $(add_button).click(function(e){ //on add input button click
	        e.preventDefault();
	        if(x < 3){ //max input box allowed
	            x++; //text box increment
	            var html = $(".clone-form-client").html();
          		$(wrapper).append(html);
	        }
	    });

	    $('.increment-form-client').on("click",".remove_field", function(e){ //user click on remove text
	    	e.preventDefault(); $(this).parent('.group-increment').remove(); x--;
	    })

	   $('.add_field_button_social_type').click(function(e) {
	   	/* Act on the event */
	   	e,preventDefault();
		   	if(x < 3){
		   		x++;
		   		var html = $(".clone-form-socialtype").html();
		   		$(".increment-form-socialtype").append(html);
		   	}
	   });

	   //show and hide form social
	   
	   $("[name=marketing_type]").on('change', function() {
	   		if(this.value){
	   			$(".hide-form-marketing").removeClass('hide');
	   		}else{
	   			$(".hide-form-marketing").addClass('hide');
	   		}
	   });

	   	$("[name=subdistrict]").on('change', function(e) {
	   		e.preventDefault();
	   		/* Act on the event */
	   		$("[name=villages]").find('option')
		    .remove()
		    .end()
		    .append('<option value="">Pilih kelurahan...</option>')
		    .val('whatever');

	   		var id = this.value;
	   		$.ajax({
	   			url: "{!! route('api.list-villages') !!}",
	   			type: 'POST',
	   			dataType: 'json',
	   			data: {
	   				subdistrict_id: id,
	   				"_token": "{{ csrf_token() }}"
	   			},
	   			success : function(data){
	   				var options = {};
	   				$.each(data.data, function(i, data) {
	   					$("[name=villages]").append(  $("<option></option>")  
	   					.attr('value', data.id)
	   					.text(data.village_name) );
	   					//options[data.id] = data.village_name;
	   				});
	   			}
	   		});
	   		
	   	});
    });
</script>
@endsection