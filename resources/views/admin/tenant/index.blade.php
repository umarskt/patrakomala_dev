@extends('admin.layouts.app')
@section('content')
<div class="box box-default">
	<div class="box-header with-border">
		<i class="fa fa-user"></i>
		<h3 class="box-title">Management Tenant</h3>
		@include('admin.main.flash-message')
		<div class="box-tools">
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-md-2 pull-right">
					<a href="{{route('admin.tenant.create')}}" class="btn btn-default"><i class="fa fa-plus"></i> Create New Tenant</a>	
				</div>
				<div class="col-md-12">
					<table id="datatables" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>Name</th>
								<th>Founding Year</th>
								<th>Logo</th>
								<th>Action</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
	{!! Html::script('admin/bower_components/datatables.net/js/jquery.dataTables.min.js') !!}
	{!! Html::script('admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}
	<script type="text/javascript">
		$(function(){
			$('#datatables').DataTable({
				processing : true,
				serverSide : true,
				ajax : "{!! route('admin.tenant.datatables') !!}",
				columns : [
					{data : 'tenant_name', name : 'tenant_name'},
					{data : 'founding_year', name : 'founding_year'},
					{data : 'img_logo', name : 'img_logo'},
					{data : 'action', name : 'action'}
				]
			});
		});
	</script>
@endsection