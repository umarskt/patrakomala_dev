@extends('admin.layouts.app')
@section('content')
<div class="box box-default">
	<div class="box-header with-border">
		<div class="box-title">
			<h3 class="box-title">List Belts</h3>
		</div>
		<div class="box-body">
			@include('admin.main.flash-message')
			<a href="{{route('admin.belts.create')}}" class="btn btn-success pull-right">Create</a>
			<section>
				<table id="datatables" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>Id</th>
							<th>Title</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</section>
		</div>
	</div>
</div>
@endsection
@section('scripts')
	{!! Html::script('admin/bower_components/datatables.net/js/jquery.dataTables.min.js') !!}
	{!! Html::script('admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}
	<script type="text/javascript">
		$(function(){
			$('#datatables').DataTable({
				processing : true,
				serverSide : true,
				ajax : "{!! route('admin.belts.datatables') !!}",
				columns : [
					{data : 'id', name : 'id'},
					{data : 'belt_title', name : 'belt_title'},
					{data : 'action', name : 'action'}
				]
			});
		});
	</script>
@endsection