@extends('admin.layouts.app')
@section('page-header','Form Create Belt')
@section('styles')
{!! Html::style('admin/bower_components/select2/dist/css/select2.min.css') !!}	
@endsection
@section('content')
<div class="box box-default">
	<div class="box-header with-border">
		<span><i class="fa fa-book"></i></span>
		<h3 class="box-title">Create Tourism Belts</h3>
		<div class="box-body">
			@include('admin.main.flash-message')
			{!! Form::open(['route'=>['admin.belts.store'], 'method'=>'post']) !!}
			<div class="row">
				<div class="col-md-8">
					<div class="form-group {!! hasError($errors,'belt_title') !!}">
						{!! Form::label('Title') !!}
						{!! Form::text('belt_title', null,['class'=>'form-control','placeholder'=>'input name']) !!}
						<span class="help-block">{!! $errors->first('belt_title') !!}</span>
					</div>
					<div class="form-group  {!! hasError($errors,'belt_description') !!}">
						<label>Description</label>
						{!! Form::textarea('belt_description', null,['class'=>'form-control','placeholder'=>'Description Event','id'=>'editor1']) !!}
						<span class="help-block">{!! $errors->first('belt_description') !!}</span>
					</div>
					<div class="form-group {!! hasError($errors,'selected_tenants') !!}">
						{!! Form::label('Pilih Tenant') !!}
            			{!! Form::select('selected_tenants[]', $tenants, null,['multiple'=>'multiple','class'=>'form-control select2','data-placeholder'=>'Pilih Tenant']) !!}
					</div>
					{!! Form::submit('create belt',['class'=>'btn btn-primary']) !!}
				</div>
			</div>	
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection
@section('scripts')
{!! Html::script('admin/bower_components/select2/dist/js/select2.full.min.js') !!}
{!! Html::script('admin/bower_components/ckeditor/ckeditor.js') !!}
<script type="text/javascript">
	$('.select2').select2();
	$('#datepicker').datepicker({
		autoclose: true
	});
	$(function () {
	CKEDITOR.replace('editor1')
	});
</script>
@endsection