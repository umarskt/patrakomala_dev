@extends('admin.layouts.app')
@section('page-header','Content Management')
@section('content')
<div class="box box-default">
	<div class="box-header with-border">
		<i class="fa fa-book"></i>
		<h3 class="box-title">List Event</h3>
		<div class="box-body">
			@include('admin.main.flash-message')
			<a href="{{route('admin.event.create')}}" class="btn btn-success pull-right">Create</a>
			<section>
			<table id="datatables" class="table table-bordered table-hover">
				<thead>
					<tr>
						<th>Judul</th>
						<th>Lokasi</th>
						<th>Pelaksanaan</th>
						<th>Action</th>
					</tr>
				</thead>
			</table>
		</section>
		</div>
	</div>
</div>
@endsection
@section('scripts')
	{!! Html::script('admin/bower_components/datatables.net/js/jquery.dataTables.min.js') !!}
	{!! Html::script('admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}
	<script type="text/javascript">
		$(function(){
			$('#datatables').DataTable({
				processing : true,
				serverSide : true,
				ajax : "{!! route('admin.event.datatables') !!}",
				columns : [
					{data : 'title', name : 'title'},
					{data : 'take_place', name : 'Take Place'},
					{data : 'start_date', name : 'Start Date'},
					{data : 'action', name : 'action'}
				]
			});
		});
	</script>
@endsection