@extends('admin.layouts.app')
@section('page-header','Form Content')
@section('styles')
{!! Html::style('admin/bower_components/timepicker/bootstrap-timepicker.min.css') !!}
{!! Html::style('admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') !!}
{!! Html::style('admin/bower_components/select2/dist/css/select2.min.css') !!}
{!! Html::style('admin/bower_components/daterangepicker/daterangepicker.css') !!}
@endsection
@section('content')
<div class="box box-default">
		<div class="box-header with border">
			<span><i class="fa fa-users"></i></span>
			<h3 class="box-title">Create Content</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse">
					<i class="fa fa-minus"></i>
				</button>
			</div>
		</div>		
		<div class="box-body">
			@include('admin.main.flash-message')
			{!! Form::open(['route'=>['admin.event.store'],'method'=>'post','files'=>'true']) !!}
			<div class="row">
				<div class="col-md-8">
					<div class="form-group {!! hasError($errors,'event_content_type') !!}">
						{!! Form::label('Tipe Kontent') !!}
						{!!  Form::select('event_content_type', ['event'=>'Event','news'=>'News'], null,['class'=>'form-control','data-placeholder'=>'Pilih Type']) !!}
					</div>
					<div class="form-group  {!! hasError($errors,'title') !!}">
						{!! Form::label('Judul') !!}
						{!! Form::text('title', null,['class'=>'form-control','placeholder'=>'input name']) !!}
						<span class="help-block">{!! $errors->first('title') !!}</span>
					</div>
					<div class="form-group  {!! hasError($errors,'description') !!}">
						<label>Description</label>
						{!! Form::textarea('description', null,['class'=>'form-control','placeholder'=>'Description Event','id'=>'description']) !!}
						<span class="help-block">{!! $errors->first('description') !!}</span>
					</div>
					<div class="form-group  {!! hasError($errors,'take_place') !!}">
						<label>Take Place</label>
						{!! Form::textarea('take_place', null,['class'=>'form-control','placeholder'=>'Place','rows'=>3]) !!}
						<span class="help-block">{!! $errors->first('take_place') !!}</span>
					</div>
					<!-- <div class="form-group  {!! hasError($errors,'start_date') !!}">
						{!! Form::label('Tanggal Mulai') !!}
						{!! Form::text('start_date', null,['class'=>'form-control','id'=>'datepicker']) !!}
						<span class="help-block">{!! $errors->first('start_date') !!}</span>
					</div>
					<div class="form-group  {!! hasError($errors,'end_date') !!}">
						{!! Form::label('Tanggal Berakhir') !!}
						{!! Form::text('start_date', null,['class'=>'form-control','id'=>'datepicker2']) !!}
						<span class="help-block">{!! $errors->first('end_date') !!}</span>
					</div>
					<div class="bootstrap-timepicker">
						<div class="form-group  {!! hasError($errors,'start_time') !!}">
							{!! Form::label('Mulai pada') !!}
							{!! Form::text('start_time', null,['class'=>'form-control timepicker']) !!}
							<span class="help-block">{!! $errors->first('start_time') !!}</span>
						</div>
					</div>
					<div class="bootstrap-timepicker">
						<div class="form-group  {!! hasError($errors,'end_time') !!}">
							{!! Form::label('Berakhir pada') !!}
							{!! Form::text('end_time', null,['class'=>'form-control timepicker']) !!}
							<span class="help-block">{!! $errors->first('end_time') !!}</span>
						</div>
					</div> -->
					<div class="form-group">
						{!! Form::label('Waktu Pelaksanaan') !!}
						<div class="input-group">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							{!! Form::text('daterange',null, ['class'=>'form-control range']) !!}
						</div>
						<!-- Hidden -->
						{!! Form::hidden('start_date') !!}
						{!! Form::hidden('end_date') !!}
						{!! Form::hidden('start_time') !!}
						{!! Form::hidden('end_time') !!}
					</div>
					<div class="form-group">
						<div class="form-group {!! hasError($errors,'files') !!}">
							{!! Form::label('Image') !!}
							{!! Form::file('files[]', null,['class'=>'form-controll']) !!}
						</div>
						<div class="repeat-form-event">
							<div class="wrapper-increment">
								<div class="container">
									<div class="template row">
										<div class="col-md-5">
											<div class="form-group">
												<div class="input-group">
													{!! Form::file('files[]', null,['class'=>'form-controll']) !!}
													<span class="input-group-btn">
														<a href="javascript:void(0);" class="btn btn-danger remove">Remove</a>
													</span>
													<span class="help-block">{!! $errors->first('files') !!}</span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<a class="btn btn-default add" href="javascript:void(0)"><i class="fa fa-plus">Add Upload File</i></a>
							</div>
						</div>	
					</div>
					<div class="form-group">
						<div class="form-group">
							{!! Form::label('Link Url') !!}
							{!! Form::text('urls[]', null,['class'=>'form-control']) !!}
						</div>
						<div class="repeat-form-url">
							<div class="wrapper-increment">
								<div class="container">
									<div class="template row">
										<div class="col-md-5">
											<div class="form-group {!! hasError($errors,'files') !!}">
												<div class="input-group">
													{!! Form::text('urls[]', null,['class'=>'form-control']) !!}
													<span class="input-group-btn">
														<a href="javascript:void(0);" class="btn btn-danger remove">Remove</a>
													</span>
													<span class="help-block">{!! $errors->first('files') !!}</span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<a class="btn btn-default add" href="javascript:void(0)"><i class="fa fa-plus">Add Link url</i></a>
							</div>
						</div>
					</div>
					<div class="form-group {!! hasError($errors,'tags') !!}">
						{!! Form::label('Tags') !!}
						{!! Form::select('tags[]', $tags, null,['multiple'=>'multiple','class'=>'form-control select2','data-placeholder'=>'Pilih Tag']) !!}
						<span class="help-block">{!! $errors->first('tags') !!}</span>
					</div>
					{!! Form::submit('Create new', ['class'=>'btn btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
@endsection
@section('scripts')
{!! Html::script('admin/bower_components/select2/dist/js/select2.full.min.js') !!}
{!! Html::script('admin/bower_components/ckeditor/ckeditor.js') !!}
{!! Html::script('admin/bower_components/timepicker/bootstrap-timepicker.min.js') !!}
{!! Html::script('admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') !!}
{!! Html::script('admin/bower_components/daterangepicker/moment.min.js') !!}
{!! Html::script('admin/bower_components/daterangepicker/daterangepicker.js') !!}
{!! Html::script('admin/dist/js/repeatable-fields.js') !!}
<script type="text/javascript">
	$('.select2').select2();

	$('[name=user_id]').change(function(){
		var selected = $('[name=user_id] :selected').text();
		$('[name=ceo]').val(selected);
	});

	$('#datepicker').datepicker({
		autoclose: true
	});

	$('#datepicker2').datepicker({
		autoclose: true
	});

	//timepicker
	$('.timepicker').timepicker({
		showInputs: false
	});

	$(function () {
		CKEDITOR.replace('description');
		$('.repeat-form-event').each(function() {
			$(this).repeatable_fields();
		});

		$('.repeat-form-url').each(function() {
			$(this).repeatable_fields();
		});

		//daterange
		$('.range').daterangepicker({
			timePicker: true,
		    startDate: moment().startOf('hour'),
		    endDate: moment().startOf('hour').add(32, 'hour'),
		    locale: {
		      format: 'Y/M/DD hh:mm A'
		    }
		}, function(start, end, label){
			$('[name=start_date]').val( start.format('Y-M-D') );
			$('[name=end_date]').val( end.format('Y-M-D') );
			$('[name=start_time]').val( start.format('hh:mm:ss') );
			$('[name=end_time]').val( end.format('hh:mm:ss') );
		});
	})

	$('.select2').select2();
</script>
@endsection