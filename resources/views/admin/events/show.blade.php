@extends('admin.layouts.app')
@section('page-header','Event Detail')
@section('content')
<div class="row">
	<div class="col-md-3">
		<div class="box box-default box-solid">
			<div class="box-header with-border">
				<h4 class="box-title">Event</h4>
			</div>
			<div class="box-body">
				<span><i class="fa fa-edit"></i></span>{{$list['event_name']}}
			</div>
		</div>
	</div>
	<div class="col-md-9">
		<div class="box box-default box-solid">
			<div class="box-header with-border">
				<h3 class="box-title">Detail Information</h3>
			</div>
			<div class="box-body">
				<div class="col-md-5">
					<div class="box box-default box-solid">
						<div class="box-header with-border">
							<h3 class="box-title">Description</h3>
						</div>
						<div class="box-body">
							{{ $list['desc'] }}
						</div>
					</div>
				</div>
				<div class="col-md-5">
					<div class="box box-default box-solid">
						<div class="box-header with-border">
							<h3 class="box-title">Date</h3>
						</div>
						<div class="box-body">
							<table class="table table-conseded">
								<tr>
									<td><i class="fa fa-calendar-check-o"></i>&nbsp;{{ $list['date'] }}</td>
								</tr>
								<tr>
									<td><i class="fa fa-location-arrow"></i>{{ $list['place'] }}</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<div class="col-md-10">
					<div class="box box-default box-solid">
						<div class="box-header with-border">
							<h3 class="box-title">Tags</h3>
						</div>
						<div class="box-body">
							@foreach($list['tags'] as $tag)
								<div class="label label-info">{{ $tag['name'] }}</div>
							@endforeach
						</div>
					</div>
					<div class="box box-default box-solid">
						<div class="box-header with-border">
							<h3 class="box-title">Images</h3>
						</div>
						<div class="box-body">
							@foreach($list['images'] as $img)
								<div class="col-md-3">
									{!! Html::image($img['path'],null,['width'=>'150']) !!}
								</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection