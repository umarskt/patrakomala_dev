@extends('admin.layouts.app')
@section('page-header','Form Content')
@section('styles')
{!! Html::style('admin/bower_components/timepicker/bootstrap-timepicker.min.css') !!}
{!! Html::style('admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') !!}
{!! Html::style('admin/bower_components/select2/dist/css/select2.min.css') !!}
<!-- {!! Html::style('admin/plugins/uploadjs/css/style.css') !!} -->
{!! Html::style('https://blueimp.github.io/Gallery/css/blueimp-gallery.min.css') !!}
{!! Html::style('admin/plugins/uploadjs/css/jquery.fileupload.css') !!}
{!! Html::style('admin/plugins/uploadjs/css/jquery.fileupload-ui.css') !!}
{!! Html::style('admin/bower_components/select2/dist/css/select2.min.css') !!}
{!! Html::style('admin/bower_components/daterangepicker/daterangepicker.css') !!}
@endsection
@section('content')
<div class="box box-default">
	<div class="box-header with border">
		<span><i class="fa fa-edit"></i></span>
		<h3 class="box-title">Edit Content</h3>
		<div class="box-body">
			@include('admin.main.flash-message')
			{!! Form::open(['route'=>['admin.event.updated', $data['id']],'method'=>'put','files'=>'true']) !!}
			<div class="row">
				<div class="col-md-8">
					<div class="form-group {!! hasError($errors,'event_content_type') !!}">
						{!! Form::label('Tipe Kontent') !!}
						{!!  Form::select('event_content_type', ['event'=>'Event','news'=>'News'], $data['event_content_type'], ['class'=>'form-control','data-placeholder'=>'Pilih Type']) !!}
					</div>
					<div class="form-group  {!! hasError($errors,'title') !!}">
						{!! Form::label('Judul') !!}
						{!! Form::text('title', $data['title'],['class'=>'form-control','placeholder'=>'input name']) !!}
						<span class="help-block">{!! $errors->first('title') !!}</span>
					</div>
					<div class="form-group  {!! hasError($errors,'description') !!}">
						<label>Description</label>
						{!! Form::textarea('description', $data['description'],['class'=>'form-control','placeholder'=>'Description Event','id'=>'description']) !!}
						<span class="help-block">{!! $errors->first('description') !!}</span>
					</div>
					<div class="form-group  {!! hasError($errors,'take_place') !!}">
						<label>Take Place</label>
						{!! Form::textarea('take_place', $data['take_place'],['class'=>'form-control','placeholder'=>'Place','rows'=>3]) !!}
						<span class="help-block">{!! $errors->first('take_place') !!}</span>
					</div>
					
					<div class="form-group">
						{!! Form::label('Waktu Pelaksanaan') !!}
						<div class="input-group">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							{!! Form::text('daterange', null, ['class'=>'form-control range']) !!}
						</div>
						<!-- Hidden -->
						{!! Form::hidden('start_date',$data['start_date']) !!}
						{!! Form::hidden('end_date',$data['end_date']) !!}
						{!! Form::hidden('start_time',$data['start_time']) !!}
						{!! Form::hidden('end_time',$data['end_time']) !!}
					</div>
					<div class="form-group">
						{!! Form::label('Event Galleri') !!}
						<table id="datatables-images" class="table table-bordered">
							<thead>
								<tr>
									<th>Images</th>
									<th>Action</th>
								</tr>
							</thead>
						</table>
					</div>
					<div class="form-group">
						<div class="repeat-form-image">
							<div class="wrapper-increment">
								<div class="container">
									<div class="template row">
										<div class="col-md-5">
											<div class="form-group">
												<div class="input-group">
													{!! Form::file('files[]', null,['class'=>'form-controll']) !!}
													<span class="input-group-btn">
														<a href="javascript:void(0);" class="btn btn-danger remove">Remove</a>
													</span>
													<span class="help-block">{!! $errors->first('files') !!}</span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<a class="btn btn-default add" href="javascript:void(0)"><i class="fa fa-plus">Add Upload File</i></a>
							</div>
						</div>	
					</div>

					<div class="form-group">
						{!! Form::label('List Link Url') !!}
						<div class="form-group">
							<table id="datatables-url" class="table table-bordered">
								<thead>
									<tr>
										<th>Link</th>
										<th>Action</th>
									</tr>
								</thead>
							</table>
						</div>
						<div class="repeat-form-url">
							<div class="wrapper-increment">
								<div class="container">
									<div class="template row">
										<div class="col-md-5">
											<div class="form-group {!! hasError($errors,'files') !!}">
												<div class="input-group">
													{!! Form::text('urls[]', null,['class'=>'form-control']) !!}
													<span class="input-group-btn">
														<a href="javascript:void(0);" class="btn btn-danger remove">Remove</a>
													</span>
													<span class="help-block">{!! $errors->first('files') !!}</span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<a class="btn btn-default add" href="javascript:void(0)"><i class="fa fa-plus">Add Link url</i></a>
							</div>
						</div>
					</div>
					
					<div class="form-group {!! hasError($errors,'tags') !!}">
						{!! Form::label('Tags') !!}
						{!! Form::select('tags[]', $tags, $data['tags'],['multiple'=>'multiple','class'=>'form-control select2','data-placeholder'=>'Pilih Tag']) !!}
						<span class="help-block">{!! $errors->first('tags') !!}</span>
					</div>
					{!! Form::submit('Update', ['class'=>'btn btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection
@section('scripts')
{!! Html::script('admin/bower_components/select2/dist/js/select2.full.min.js') !!}
{!! Html::script('admin/bower_components/ckeditor/ckeditor.js') !!}
{!! Html::script('admin/bower_components/timepicker/bootstrap-timepicker.min.js') !!}
{!! Html::script('admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') !!}
{!! Html::script('admin/bower_components/daterangepicker/moment.min.js') !!}
{!! Html::script('admin/bower_components/daterangepicker/daterangepicker.js') !!}
{!! Html::script('admin/bower_components/datatables.net/js/jquery.dataTables.min.js') !!}
{!! Html::script('admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}
{!! Html::script('admin/dist/js/repeatable-fields.js') !!}

<script type="text/javascript">
	$('.select2').select2();
	$('[name=user_id]').change(function(){
		var selected = $('[name=user_id] :selected').text();
		$('[name=ceo]').val(selected);
	});
	$('#datepicker').datepicker({
		autoclose: true
	});

	$('#datepicker2').datepicker({
		autoclose: true
	});
	//timepicker
	$('.timepicker').timepicker({
		showInputs: false
	});
$(function () {
	CKEDITOR.replace('description')
	$('.range').daterangepicker({
			timePicker: true,
		    startDate: moment("{{ $data['start_date'] }}").add( "{{ $data['start_time_hours'] }}",'hours' ).add( "{{ $data['start_time_minute'] }}",'minutes' ),
		    endDate:  moment("{{ $data['end_date'] }}").add(" {{  $data['end_time_hours'] }} ",'hours').add( "{{ $data['end_time_hours'] }}",'minutes' ),
		    locale: {
		      format: 'Y/MM/DD hh:mm A'
		    }
		}, function(start, end, label){
			$('[name=start_date]').val( start.format('Y-MM-D') );
			$('[name=end_date]').val( end.format('Y-MM-D') );
			$('[name=start_time]').val( start.format('hh:mm:ss') );
			$('[name=end_time]').val( end.format('hh:mm:ss') );
		});

	$('#datatables-images').DataTable({
		processing : true,
		serverSide : true,
		ajax : "{!! route('admin.event.datatables.image', $data['id']) !!}",
		columns : [
			{data : 'images', name : 'images'},
			{data : 'action', name : 'action'}
		]
	});

	$('#datatables-url').DataTable({
		processing : true,
		serverSide : true,
		ajax : "{!! route('admin.event.datatables.url', $data['id']) !!}",
		columns : [
			{data : 'url', name : 'url'},
			{data : 'action', name : 'action'}
		]
	});

	$('.repeat-form-url').each(function() {
		$(this).repeatable_fields();
	});

	$('.repeat-form-image').each(function() {
		$(this).repeatable_fields();
	});
});
$('.select2').select2();

</script>
@endsection