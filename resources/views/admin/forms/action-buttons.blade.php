@if( isset( $show_url ) )
<a href="{!! empty( $show_url ) ? 'javascript:void(0)' : $show_url !!}" class="btn btn-primary {!! empty( $show_url ) ? 'disabled' : '' !!}" title="Show" data-button="edit">
    <i class="fa fa-search fa-fw"></i>
</a>
@endif
@if( isset( $edit_url ) )
<a href="{!! empty( $edit_url ) ? 'javascript:void(0)' : $edit_url !!}" class="btn btn-success {!! empty( $edit_url ) ? 'disabled' : '' !!}" title="Edit" data-button="edit">
    <i class="fa fa-pencil-square-o fa-fw"></i>
</a>
@endif
@if( isset( $user_login ) )
<a href="{!! empty( $user_login ) ? 'javascript:void(0)' : $user_login !!}" class="btn btn-info {!! empty( $user_login ) ? 'disabled' : '' !!}" title="log in" data-button="edit">
    <i class="fa fa-download"></i>
</a>
@endif
@if( isset( $delete_url ) )
    <a href="{!! empty( $delete_url ) ? 'javascript:void(0)' : $delete_url !!}" class="btn btn-danger {!! empty( $delete_url ) ? 'disabled' : '' !!}" title="Delete" data-button="delete">
        <i class="fa fa-trash-o fa-fw"></i>
    </a>
@endif