@component('mail::message')
# Dear Admin

Berikut approval data tenant

@component('mail::table')
<table>
	<tbody>
		<tr>
			<th>Nama Tenant</th>
			<td>{!! $data_tenant['tenant_name'] !!}</td>
		</tr>
		<tr>
			<th>Email</th>
			<td>{!! $data_tenant['tenant_email'] !!}</td>
		</tr>
		<tr>
			<th>No Telp</th>
			<td>{!! $data_tenant['tenant_phone'] !!}</td>
		</tr>
	</tbody>
</table>
@endcomponent

@component('mail::button', ['url' => route('admin.tenant.show', $data_tenant['id'])])
Cek selengkapnya
@endcomponent

Thanks,<br>
Admin Patrakomala
@endcomponent
