@component('mail::message')
# Dear Admin

Berikut approval data Tenant 

@component('mail::table')
<table>
	<tbody>
		<tr>
			<th>Nama Tenant</th>
			<td>{!! $data_tenant->tenant_name !!}</td>
		</tr>
		<tr>
			<th>Tahun Berdiri</th>
			<td>{!! $data_tenant->founding_year !!}</td>
		</tr>
	</tbody>
</table>
@endcomponent

@component('mail::button', ['url' => route('admin.tenant.show', $data_tenant->id)])
Cek selengkapnya
@endcomponent

Thanks,<br>
Admin Patrakomala
@endcomponent
