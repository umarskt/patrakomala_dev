@component('mail::message')
Selamat akun tenant anda telah di acc, berikut data akunnya

@component('mail::table')
<table>
	<tbody>
		<tr>
			<th>Username</th>
			<td>{{ $params['app_username'] }}</td>
		</tr>
		<tr>
			<th>Password</th>
			<td>{{ $params['app_password'] }}</td>
		</tr>
	</tbody>
</table>
@endcomponent

@component('mail::button', ['url' => '','color'=>'blue'])
Login
@endcomponent

Thanks,<br>
Admin Patrakomala
@endcomponent
