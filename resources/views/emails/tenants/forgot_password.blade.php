@component('mail::message')
# Reset Password

Password anda berhasil di reset, silahkan klik button di bawah untuk mengganti password.

@component('mail::button', ['url' => env('FRONTEND_URL').'users/change-password?code='.$token])
Ganti Password
@endcomponent

Thanks,<br>
Admin Patrakomala
@endcomponent
