@component('mail::message')
# Dear Hackathon Team

Kini Team Anda dapat mengakses API patrakomala!

@component('mail::table')
<table>
	<tbody>
		<tr>
			<th>Nama Team</th>
			<td>{!! $data['team_name'] !!}</td>
		</tr>
		<tr>
			<th>Email Team</th>
			<td>{!! $data['contact_email'] !!}</td>
		</tr>
		<tr>
			<th>Access Key</th>
			<td>{!! $data['access_key'] !!}</td>
		</tr>
	</tbody>
</table>
@endcomponent

Untuk pengaksesan dokumentasi API dapat di akses di bawah
@component('mail::button', ['url' => 'https://patrakomaladev.docs.apiary.io/#'])
DOC API
@endcomponent

Thanks,<br>
Admin {{ config('app.name') }}
@endcomponent
