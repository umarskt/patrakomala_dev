@component('mail::message')
# Introduction

Available job for you

@component('mail::button', ['url' => ''])
Take It
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
