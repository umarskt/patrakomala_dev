@component('mail::message')

Selamat akun anda telah aktif

@component('mail::table')
<table>
	<tbody>
		<tr>
			<th>Username</th>
			<td>{{ $params['app_username'] }}</td>
		</tr>
		<tr>
			<th>Password</th>
			<td>{{ $params['client_password'] }}</td>
		</tr>
		<tr>
			<th>email</th>
			<td>{{ $params['app_email'] }}</td>
		</tr>
	</tbody>
</table>
@endcomponent


Thanks,<br>
{{ config('app.name') }}
@endcomponent
