<?php

return[
	/*General Language*/

	//for sidebar
	'dashboard'=>'Dashboard',
	//users
	'user' => 'Users',
	'user_management' => 'User Management',
	'group_management' => 'Group Management',

	//tenant
	'tenant' => 'Tenant',

	//Event
	'event' => 'Event',
	'event_management' => 'Event Management',
	'set_tags_event' => 'Setting Tags Event',

	//Tourism Package
	'tourism' => 'Tourism Package',
	'belts'=> 'Tourism Belt Management',
	'tourism_management' => 'Tourism Package Management',

	//Itineraries
	'itinerary_management' => 'Itinerary Management',

	//hackathon
	'hackathon' => 'List Team Hackathon'
];