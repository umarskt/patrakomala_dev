<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['namespace'=>'Admin','middleware'=>'web'], function(){
    Route::get('/', ['as'=>'admin.login','uses'=>'AuthencticationController@index']);
    Route::post('login/auth',['as'=>'admin.post.login','uses'=>'AuthencticationController@store']);
});

Route::group(['namespace'=>'Admin','prefix'=>'admin','middleware'=>['web','access_admin']], function(){
    Route::get('main',['as'=>'admin.dahsboard','uses'=>'AdminController@index']);
    Route::get('tenant-unregistered',['as'=>'tenant.unregistered.datatables','uses'=>'AdminController@getData']);
    Route::get('tenant-claimed',['as'=>'tenant.claimed.datatables', 'uses'=>'ClaimTenantController@getDataList']);
    Route::post('analyze-chart',['as'=>'tenant.analyze.chart','uses'=>'AdminController@analyzeChart']);
    //group user
    Route::group(['prefix'=>'user'], function(){
        Route::get('index', ['as'=>'admin.user.index','uses'=>'UserController@index']);
        Route::get('create',['as'=>'admin.user.create','uses'=>'UserController@create']);
        Route::get('edit/{id}',['as'=>'admin.user.edit','uses'=>'UserController@edit']);
        Route::put('update/{id}',['as'=>'admin.user.update','uses'=>'UserController@update']);
        Route::post('create',['as'=>'admin.user.create','uses'=>'UserController@store']);
        Route::get('{id}/delete',['as'=>'admin.user.delete','uses'=>'UserController@destroy']);

        //user management
        Route::get('data-table',['as'=>'admin.user.datatables','uses'=>'UserController@get_data']);
        Route::get('data-table-list-tenant',['as'=>'admin.user.tenant.datatables','uses'=>'TenantController@get_data_user']);
    });
    //group tenant
    Route::group(['prefix'=>'tenant'], function(){
        Route::post('store',['as'=>'admin.tenant.store','uses'=>'TenantController@store']);
        Route::post('approve-tenant',['as'=>'admin.tenant.approve','uses'=>'TenantController@approvalTenant']);
        Route::get('index', ['as'=>'admin.tenant.index','uses'=>'TenantController@index']);
        Route::get('data-table',['as'=>'admin.tenant.datatables','uses'=>'TenantController@get_data']);
        Route::get('data-table-galleries/{id}',['as'=>'admin.tenant.datatables.galleries','uses'=>'TenantController@get_data_images_gallery']);
        Route::get('data-table-clients/{id}',['as'=>'admin.tenant.datatables.clients','uses'=>'TenantController@get_data_clients']);
        Route::get('data-table-legalities/{id}',['as'=>'admin.tenant.datatables.legalities','uses'=>'TenantLegalityController@get_data_legality']);
        Route::get('create',['as'=>'admin.tenant.create','uses'=>'TenantController@create']);
        Route::get('edit/{id}',['as'=>'admin.tenant.edit','uses'=>'TenantController@edit']);
        Route::put('update/{id}',['as'=>'admin.tenant.update','uses'=>'TenantController@update']);
        Route::get('delete/{id}',['as'=>'admin.tenant.delete','uses'=>'TenantController@destroy']);
        Route::get('delete-gallery/{id}',['as'=>'admin.gallery.delete','uses'=>'TenantController@destroyImageGallery']);
        Route::get('show/{id}',['as'=>'admin.tenant.show','uses'=>'TenantController@show']);
    });

    //Event     
    Route::group(['prefix'=>'event'], function(){
        Route::get('index',['as'=>'admin.event.index','uses'=>'EventController@index']);
        Route::get('create',['as'=>'admin.event.create','uses'=>'EventController@create']);
        Route::get('data-table',['as'=>'admin.event.datatables','uses'=>'EventController@get_data']);
        Route::post('store',['as'=>'admin.event.store','uses'=>'EventController@store']);
        Route::get('delete/{id}',['as'=>'admin.event.delete','uses'=>'EventController@destroy']);
        Route::get('show/{id}',['as'=>'admin.event.show','uses'=>'EventController@show']);
        Route::get('edit/{id}',['as'=>'admin.event.edit','uses'=>'EventController@edit']);
        Route::put('updated/{id}',['as'=>'admin.event.updated','uses'=>'EventController@update']);
        Route::get('datatables-image/{id}',['as'=>'admin.event.datatables.image','uses'=>'EventController@getDataImages']);
        Route::get('datatables-url/{id}',['as'=>'admin.event.datatables.url','uses'=>'EventController@getDataUrls']);
        Route::get('delete-image/{id}',['as'=>'admin.event.images.delete','uses'=>'EventController@deleteImg']);
        Route::get('delete-url/{id}',['as'=>'admin.event.url.delete','uses'=>'EventController@deleteUrl']);

        //tag event
        Route::group(['prefix'=>'tag'], function(){
            Route::get('index',['as'=>'admin.tag.index','uses'=>'TagEventController@index']);
            Route::get('data-table',['as'=>'admin.tag.datatables','uses'=>'TagEventController@get_data']);
            Route::get('create',['as'=>'admin.event.tag.create','uses'=>'TagEventController@create']);
            Route::post('store',['as'=>'admin.event.tag.store','uses'=>'TagEventController@store']);
            Route::get('edit/{id}',['as'=>'admin.event.tag.edit','uses'=>'TagEventController@edit']);
            Route::get('delete/{id}',['as'=>'admin.event.tag.delete','uses'=>'TagEventController@destroy']);
        });

        //approval
        Route::resource('approval', 'EventApprovalManagementController');
        
    });
    
    Route::group(['prefix'=>'belts'], function(){
        Route::get('index',['as'=>'admin.belts.index','uses'=>'TourismBeltController@index']);
        Route::get('data-table',['as'=>'admin.belts.datatables','uses'=>'TourismBeltController@get_data']);
        Route::get('create',['as'=>'admin.belts.create','uses'=>'TourismBeltController@create']);
        Route::post('store',['as'=>'admin.belts.store','uses'=>'TourismBeltController@store']);
        Route::get('edit/{id}',['as'=>'admin.belts.edit','uses'=>'TourismBeltController@edit']);
        Route::put('update/{id}',['as'=>'admin.belts.update','uses'=>'TourismBeltController@update']);
        Route::get('delete/{id}',['as'=>'admin.belts.delete','uses'=>'TourismBeltController@destroy']);
    });

    Route::group(['prefix'=>'packages'], function(){
        Route::get('index',['as'=>'admin.packages.index','uses'=>'TourismPackageController@index']);
        Route::get('data-table',['as'=>'admin.packages.datatables','uses'=>'TourismPackageController@get_data']);
        Route::get('create',['as'=>'admin.packages.create','uses'=>'TourismPackageController@create']);
        Route::post('store',['as'=>'admin.packages.store','uses'=>'TourismPackageController@store']);
        Route::get('delete/{id}',['as'=>'admin.packages.delete','uses'=>'TourismPackageController@destroy']);
    });

    Route::group(['prefix'=>'itineraries'], function(){
        Route::get('index',['as'=>'admin.itineraries.index','uses'=>'ItineraryController@index']);
        Route::get('data-table',['as'=>'admin.itineraries.datatables','uses'=>'ItineraryController@get_data']); 
        Route::get('create',['as'=>'admin.itineraries.create','uses'=>'ItineraryController@create']);
        Route::post('store',['as'=>'admin.itineraries.store','uses'=>'ItineraryController@store']);
    });

    Route::group(['prefix'=>'hackathons'], function(){
        Route::get('index',['as'=>'admin.hackathons.index','uses'=>'HackathonController@index']);
        Route::get('data-table',['as'=>'admin.hackathons.datatables','uses'=>'HackathonController@get_data']);
        Route::get('detail/{id}',['as'=>'admin.hackathons.detail','uses'=>'HackathonController@detail']);
    });

    Route::get('logout',['as'=>'admin.post.logout','uses'=>'AuthencticationController@destroy']);
});


