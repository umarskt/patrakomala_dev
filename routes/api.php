<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace'=>'Api', 'prefix'=>'v1', 'middleware'=> ['api','log_api'] ], function(){
	Route::group(['prefix'=>'user'], function(){
		Route::post('auth',['uses'=>'AuthenticationController@login']);
		Route::post('verification',['uses'=>'AuthenticationController@verification']);
		Route::post('renewal-verification',['uses'=>'AuthenticationController@reSendVerification']);
		Route::post('reset-password',['uses'=>'AuthenticationController@resetPassword']);
		Route::post('change-password',['uses'=>'AuthenticationController@changePassword']);
		Route::group(['prefix'=>'client'], function(){
			Route::post('register', ['uses'=>'ClientController@register']);
			Route::post('post-project',['uses'=>'ProjectController@postProject']);
			Route::post('list-project',['uses'=>'ProjectController@listProject']);
		});
	});

	Route::group(['prefix'=>'tenant'], function(){
		Route::get('list-subsector',['uses'=>'SubSectorController@getAllSubSector']);
		Route::get('list-subdistrict',['uses'=>'TenantController@getAllSubDistrict']);
		Route::post('list-tenant',['uses'=>'TenantController@filteringTenant']);
		Route::get('profile-tenant',['uses'=>'TenantController@tenantDetail']);
		Route::post('register-tenant',['uses'=>'TenantController@storeTenant']);
		Route::post('edit-profile',['uses'=>'TenantController@editTenant']);
		Route::post('store-gallery',['uses'=>'TenantController@StoreGalleries']);
		Route::post('list-villages',['as'=>'api.list-villages','uses'=>'TenantController@getVillages']);
		Route::post('claim',['uses'=>'ClaimTenantController@claimTenant']);
		Route::group(['prefix'=>'portofolio'], function(){
			Route::post('delete', ['uses'=>'PortofolioController@removePortofolio']);
		});
	});

	Route::group(['prefix'=>'tour-package'], function(){
		Route::get('list-belt',['uses'=>'TourismPackageController@getAllBelt']);
		Route::get('list-package',['uses'=>'TourismPackageController@getAllPackage']);
		Route::post('filter-package',['uses'=>'TourismPackageController@filterPackage']);
		Route::get('list-tenant',['uses'=>'TourismPackageController@listTenant']);
		Route::get('detail-tenant',[ 'uses'=> 'TourismPackageController@detailTenant']);
	});

	Route::group(['prefix'=>'content'], function(){
		Route::post('filter-content',['uses'=>'ContentController@getAllContent']);
		Route::post('post-content',['uses'=>'EventController@store']);
		Route::put('{id}/post-content',['uses'=>'EventController@editContent']);
		Route::get('detail-content',['uses'=>'ContentController@detailContent']);
		Route::post('post-image',['uses'=>'EventController@uploadEventImage']);
		Route::post('list-content',['uses'=>'EventController@listEventUser']);
		Route::post('user-content',['uses'=>'EventController@detailEventUser']);
	});

	Route::group(['prefix'=>'hackathon'], function(){
		Route::post('register-hackathon', ['uses'=>'HackathonController@RegisHackathon']);
		Route::post('generate-key', ['as'=>'api.generate.key','uses'=>'HackathonController@generate']);
	});

	Route::group(['prefix'=>'public','namespace'=>'Publics'], function(){
		Route::post('generate-key', ['uses'=>'KeyController@generateAccessToken']);
		Route::group(['middleware'=>'access_key'], function(){
			//API public tenant
			Route::group(['prefix'=>'tenant'], function(){
					Route::post('list-tenant',['uses'=>'PublicTenantController@listTenant']);
					Route::get('detail-tenant',['uses'=>'PublicTenantController@tenantDetail']);
					Route::get('list-subsector',['uses'=>'PublicListSubsector@getAllSubSector']);
					Route::get('list-subdistrict',['uses'=>'PublicListSubDistrict@getAllSubDistrict']);
			});

			//detail event
			Route::group(['prefix'=>'event'], function(){
				Route::post('list-event',['uses'=>'PublicEventController@listEvent']);
				Route::get('detail-event',['uses'=>'PublicEventController@detailContent']);
			});

			Route::group(['prefix'=>'project'], function(){
				Route::post('post-project', ['uses'=>'PublicProjectController@postProject']);
			});

			Route::group(['prefix'=>'beat'], function(){
				Route::get('list-belt',['uses'=>'PublicTourismPackageController@getAllBelt']);
				Route::get('list-package',['uses'=>'PublicTourismPackageController@getAllPackage']);
				Route::post('filter-package',['uses'=>'PublicTourismPackageController@filterPackage']);
				Route::get('list-tenant',['uses'=>'PublicTourismPackageController@listTenant']);
				Route::get('detail-tenant',[ 'uses'=> 'PublicTourismPackageController@detailTenant']);
			});

			Route::post('client/register',['uses'=>'RegisterClientPublicController@register']);
			Route::post('client/login',['uses'=>'LoginClientPublicController@login']);
		});
	});

	Route::group(['prefix'=>'analytic','middleware'=>'api_auth'], function(){
		Route::post('counter',['uses'=>'AnalyticController@counterVisitPage']);
	});
});
